import argparse
import pdb
from mage_webapp import *
import warnings

warnings.filterwarnings("ignore", module='openpyxl')

default_upsfname = 'example_spreadsheets/marketgen/default_unit_profile_set.xlsx'


def bootstrap_admin_user(username, password):
    sa_engine = create_engine('sqlite:///'+database_file)
    Base.metadata.create_all(sa_engine)
    sa_session = sessionmaker(bind=sa_engine)()
    u = User(username, 'admin', password)
    ulist = [u for u in sa_session.query(User).filter_by(username=u.username)]
    if not ulist:
        sa_session.add(u)
        print('Adding user %s with credentials %s, password %s to db %s' % (u.username,
                                                                            u.credentials,
                                                                            password,
                                                                            database_file))
    else:
        print('User %s already in database, not adding' % u.username)

    # add some default configurations to database
    upslist = [u for u in sa_session.query(UnitProfileSet).filter_by(profilesetname='default')]
    if not upslist:
        ups = UnitProfileSet(profilesetname='default')
        wb = load_workbook(default_upsfname)
        ups.initialize_from_workbook(wb)
        sa_session.add(ups)
        print('Adding default unit profile set, loaded from file %s' % default_upsfname)
    else:
        print('Default unit profile set already in database, not adding')

    # add default daily time segments to database
    dts5 = DailyTimeSegments(marketsimulationname='')
    e = dts5.initialize_from_spreadsheet_file('example_spreadsheets/set3/daily_time_segments.xlsx')
    if e:
        print(e)
        raise Exception()
    dts24 = DailyTimeSegments(marketsimulationname='')
    e = dts24.initialize_from_spreadsheet_file('example_spreadsheets/set3/daily_time_segments_24hour.xlsx')
    if e:
        print(e)
        raise Exception()

    d_dts5 = DefaultDailyTimeSegments(dts=dts5, dailytimesegmentsname='default_5_time_segments')
    d_dts24 = DefaultDailyTimeSegments(dts=dts24, dailytimesegmentsname='default_24_time_segments')

    for d_dts in [d_dts5, d_dts24]:
        tmp = sa_session.query(DefaultDailyTimeSegments).filter_by(dailytimesegmentsname=d_dts.dailytimesegmentsname)
        if [o for o in tmp]:
            print('Default daily time segments named %s already in database, not adding' % d_dts.dailytimesegmentsname)
        else:
            sa_session.add(d_dts)
            print('Adding daily time segments named %s' % d_dts.dailytimesegmentsname)
    sa_session.commit()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Add a single admin account to emm-app database')
    parser.add_argument('username', help='username of admin to add to database')
    parser.add_argument('password', help='password of admin')

    n = parser.parse_args()
    bootstrap_admin_user(n.username, n.password)
