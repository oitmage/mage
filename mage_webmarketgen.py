# market generation tools
import sys
from mage_model import *
import random
import json
import pdb
import os

assert sys.version_info >= (3,)


# automatically generate market simulation based on given # of users, desired energy mix
class DiscreteDensity:
    """Represents discrete random variable

    x is list of possible values
    p is corresponding probabilities

    This will be inefficient for large lists, probably fine for small lists
    """

    def __init__(self, x, p):
        tol = 1e-4
        if any(pp < 0 for pp in p) or abs(sum(p)-1.0) > tol:
            raise MageException('probabilities must be positive and sum to 1.0')
        if len(x) != len(p):
            raise MageException('x and p must have same length')
        self.x = x
        self.p = [pp/sum(p) for pp in p]  # renormalize, to be robust to rounding errors
        self.pcumsum = cumsum(self.p)
        self.pcumsum[-1] = 1.0  # ensure last value exactly 1.0; even in presence of rounding errors

    def mean(self):
        return sum(xx*pp for (xx, pp) in zip(self.x, self.p))

    def sample(self):
        # generate random value
        r = random.random()
        # find first value where r<pcumsum[n]
        for xx, pcum in zip(self.x, self.pcumsum):
            if r < pcum:
                return xx


class UniformDensity:
    """Represents continuous random variable uniformly distributed on [a,b)
     """

    def __init__(self, a, b):
        if not (a < b):
            raise MageException('a must be smaller than b')
        self.a = a
        self.b = b

    def mean(self):
        return (self.a+self.b)/2

    def sample(self):
        return random.random()*(self.b-self.a)+self.a


class GeneratorUnitGenerator:

    def __init__(self):
        self.generator_type = ''
        self.fuel = ''
        self.capacity_dist = None
        self.segment_capacity_fraction = None
        self.fixed_costs = None
        self.unitstubname = ''
        self.groupsizes = None
        self.constraints = None

    def expected_capacity(self):
        return self.capacity_dist.mean()

    def generate_nameless_unit(self):
        segment_heatrates = [self.heatrate1_dist.sample()]
        for diffgen in [self.heatrate_d12_dist, self.heatrate_d23_dist]:
            if diffgen:
                segment_heatrates.append(segment_heatrates[-1] - diffgen.sample())

        segment_heatrates = [round(hr, 0) for hr in segment_heatrates]
        owner = ''
        unit = ''
        capacity = self.capacity_dist.sample()
        forced_outage_rate = round(self.for_dist.sample(), 3)
        variable_o_and_m = round(self.variable_o_and_m_dist.sample(), 2)
        segment_capacities = [capacity * scf for scf in self.segment_capacity_fraction]
        segment_numbers = [1, 2, 3]
        snchr_triples = zip(segment_numbers, segment_capacities, segment_heatrates)
        marketsimulationname = ''
        u = MasterGeneratorUnit(marketsimulationname, owner, self.generator_type, unit, capacity, forced_outage_rate,
                                self.fuel, variable_o_and_m, self.fixed_costs, snchr_triples, self.constraints)
        return u

    def initialize_from_spreadsheet(self, sheet):
        # first parse spreadsheet lines into dictionary
        d = {}
        for row in sheet.rows:
            try:
                key = str(row[0].value)
                # filter out empty cells values
                # cell value is empty if it is None type, or a string that, when stripped, is empty
                # Directly using truthines of v.value is a bug as v.value may be float(0.0), which is false-ey
                val = [v for v in row[1:] if (str(v.value).strip() and v.value is not None)]
                d[key] = val
                # print('%s : %s' % (key, [v.value for v in val]))
                # pdb.set_trace()
            except IndexError:
                pass
        self.generator_type = str(d['generator_type'][0].value).strip()
        self.fuel = str(d['fuel'][0].value).strip()
        self.capacity_dist = DiscreteDensity([float(xx.value) for xx in d['capacity_dist_x']],
                                             [float(pp.value) for pp in d['capacity_dist_p']])
        for name in ['for', 'variable_o_and_m', 'heatrate1', 'heatrate_d12', 'heatrate_d23']:
            try:
                dist = UniformDensity(a=float(d[name+'_dist_limits'][0].value),
                                      b=float(d[name+'_dist_limits'][1].value))
                setattr(self, name+'_dist', dist)
            except (IndexError, ValueError):
                setattr(self, name+'_dist', None)
        #pdb.set_trace()
        self.segment_capacity_fraction = json.loads(d['segment_capacity_fraction'][0].value)
        self.fixed_costs = float(d['fixed_costs'][0].value)
        if d['constraints']:
            self.constraints = GeneratorConstraints.from_string(str(d['constraints'][0].value))
        else:
            self.constraints = GeneratorConstraints()
        self.unitstubname = str(d['unitstubname'][0].value)
        self.groupsizes = json.loads(d['groupsizes'][0].value)

    def spreadsheet_repr(self):
        wb = Workbook()
        self.spreadsheet_sheet_repr(wb.active)
        return wb

    def spreadsheet_sheet_repr(self, sheet):
        textlists = [['generator_type', self.generator_type],
                     ['fuel', self.fuel],
                     ['capacity_dist_x'] + self.capacity_dist.x,
                     ['capacity_dist_p'] + self.capacity_dist.p]

        for name in ['for', 'variable_o_and_m', 'heatrate1', 'heatrate_d12', 'heatrate_d23']:
            dist = getattr(self, name+'_dist')
            if dist:
                textlists.append([name+'_dist_limits', dist.a, dist.b])
            else:
                textlists.append([name+'_dist_limits'])

        for name in ['fixed_costs', 'constraints', 'unitstubname']:
            textlists.append([name, str(getattr(self, name))])
        for name in ['segment_capacity_fraction', 'groupsizes']:
            textlists.append([name, json.dumps(getattr(self, name))])

        write_list_to_sheet(sheet, WSpoint(row=1, col=1), textlists)


class CoalUnitGenerator(GeneratorUnitGenerator):
    def __init__(self):
        super().__init__()
        self.generator_type = 'Coal'
        self.fuel = 'Coal'
        self.capacity_dist = DiscreteDensity([300, 400], [.66, .34])
        self.for_dist = UniformDensity(.106, .136)
        self.variable_o_and_m_dist = UniformDensity(3.0, 5.0)
        # sample heatrate1 and then set heatrate2=heatrate1-heatrate_d12 and heatrate3=heatrate2-heatrated23
        self.heatrate1_dist = UniformDensity(11000, 13400)
        self.heatrate_d12_dist = UniformDensity(0, 2800)
        self.heatrate_d23_dist = UniformDensity(0, 1600)
        self.segment_capacity_fraction = [.5, .25, .25]
        self.fixed_costs = 5.0
        self.constraints = GeneratorConstraints(['slow_ramp'])
        self.unitstubname = 'CL'
        self.groupsizes = (2, 3)


class CombinedCycleUnitGenerator(GeneratorUnitGenerator):
    def __init__(self):
        super().__init__()
        self.capacity_dist = DiscreteDensity([100, 200], [.35, .65])
        self.for_dist = UniformDensity(.045, .063)
        self.variable_o_and_m_dist = UniformDensity(2.25, 2.75)
        self.heatrate1_dist = UniformDensity(8100, 9900)
        self.heatrate_d12_dist = UniformDensity(0, 1710)
        self.heatrate_d23_dist = UniformDensity(400, 3190)
        self.segment_capacity_fraction = [.25, .25, .5]
        self.generator_type = 'Combined Cycle'
        self.fuel = 'Natural Gas'
        self.fixed_costs = 5.0
        self.constraints = GeneratorConstraints()
        self.unitstubname = 'CC'
        self.groupsizes = (3, 4, 5)


class CombustionTurbineUnitGenerator(GeneratorUnitGenerator):
    def __init__(self):
        super().__init__()
        self.capacity_dist = DiscreteDensity([50], [1.0])
        self.for_dist = UniformDensity(.09, .125)
        self.variable_o_and_m_dist = UniformDensity(4.5, 5.5)
        self.heatrate1_dist = UniformDensity(9300, 10700)
        self.heatrate_d12_dist = UniformDensity(1160, 3000)
        self.heatrate_d23_dist = None
        self.segment_capacity_fraction = [.5, .5]
        self.generator_type = 'Combustion Turbine'
        self.fuel = 'Natural Gas'
        self.fixed_costs = 5.0
        self.constraints = GeneratorConstraints()
        self.unitstubname = 'CT'
        self.groupsizes = (3, 4, 5)


class NuclearUnitGenerator(GeneratorUnitGenerator):
    def __init__(self):
        super().__init__()
        self.capacity_dist = DiscreteDensity([600], [1.0])
        self.for_dist = UniformDensity(.048, .053)
        self.variable_o_and_m_dist = UniformDensity(3.8, 4.4)
        self.heatrate1_dist = UniformDensity(8245, 8755)
        self.heatrate_d12_dist = UniformDensity(240, 430)
        self.heatrate_d23_dist = UniformDensity(80, 350)
        self.segment_capacity_fraction = [5/12, 4/12, 3/12]
        self.generator_type = 'Nuclear'
        self.fuel = 'Nuclear'
        self.fixed_costs = 5.0
        self.constraints = GeneratorConstraints(['must_run'])
        self.unitstubname = 'NUC'
        self.groupsizes = (2, 3)


class UnitProfileSet(Base):
    __tablename__ = "unitprofilesets"
    id = Column(Integer, primary_key=True)
    profilesetname = Column(String)
    gug_d = Column(PickleType)  # dictionary containing generator unit generators, by name

    def __init__(self, profilesetname):
        self.gug_d = {}
        self.profilesetname = profilesetname

    # noinspection PyUnusedLocal,PyBroadException
    def initialize_from_workbook(self, wb, **kwargs):
        e = []
        try:
            for sheet in wb:
                x = GeneratorUnitGenerator()
                x.initialize_from_spreadsheet(sheet)
                self.gug_d[x.generator_type] = x
        except Exception as exc:
            pdb.set_trace()
            e = ['Could not initialize unit profile set from spreadsheet']
        return e

    def spreadsheet_repr(self):
        wb = Workbook()
        sheet = wb.active
        first = True
        for names in self.gug_d:
            if not first:
                sheet = wb.create_sheet()
            first = False
            self.gug_d[names].spreadsheet_sheet_repr(sheet)
        return wb

    @classmethod
    def get_from_db(cls, sa_session, **kwargs):
        return [o for o in sa_session.query(cls).filter_by(**kwargs)]


class UsernameSet(Base):
    __tablename__ = "usernamesets"
    id = Column(Integer, primary_key=True)
    usernamesetname = Column(String)
    usernames = Column(PickleType)
    marketsimulationname = Column(String, ForeignKey('marketsimulationinstances.name'))

    def __init__(self, usernamesetname, marketsimulationname):
        self.usernamesetname = usernamesetname
        self.marketsimulationname = marketsimulationname
        self.usernames = []

    def initialize_from_spreadsheet(self, sheet):
        def is_valid_username(usr):
            return all([c in string.ascii_lowercase for c in usr])
        e = []
        # verify first row
        if not (is_string_list([sheet.cell(row=1, column=1 + n).value for n in range(2)]) and
                sheet.cell(row=1, column=1).value.lower() == 'username'):
            e.append('first row not as expected : should be username in A1')
        else:
            for row in sheet.rows[1:]:  # skip first row
                u = str(row[0].value).strip()
                if u:  # silently ignore any rows where username or password are empty
                    if not is_valid_username(u):
                        e.append('Username %s invalid, must be all lowercase letters' % u)
                    else:
                        self.usernames.append(u)
        return e

    @classmethod
    def get_from_db(cls, sa_session, **kwargs):
        return [o for o in sa_session.query(cls).filter_by(**kwargs)]


def assign_names_to_units(unit_list, ups):
    """ Generate names for all units, place in unit field.

    This should be called after owners are assigned to units

    :param unit_list: list of MasterGeneratorUnit's
    :param ups: unit profile set
    :return: None
    """
    # find all owners
    owners = list(set([u.owner for u in unit_list]))
    generator_types = list(set([u.generator_type for u in unit_list]))
    # build "pool" of names to draw from
    basenames_by_gtype_d = {gtype.lower(): generate_name_stubs(ups[gtype].unitstubname) for gtype in generator_types}

    groupsizes_by_gtype_d = {'nuclear': (2, 3),
                             'coal': (2, 3),
                             'combined cycle': (3, 4, 5),
                             'combustion turbine': (3, 4, 5)
                             }
    default_groupsizes = (3, 4, 5)

    # loop over each owner
    for owner in owners:
        for gtype in generator_types:
            curr_units = [u for u in unit_list if u.owner == owner and u.generator_type == gtype]
            # loop over each type
            curr_unit_groups = grouplist(curr_units, groupsizes_by_gtype_d.get(gtype.lower(), default_groupsizes))
            for group in curr_unit_groups:
                basename = next(basenames_by_gtype_d[gtype.lower()])
                for n, u in enumerate(group):
                    u.unit = '%s Unit %g' % (basename, n+1)


def assign_units_to_owners(unit_list, owner_list):
    # do greedy algorithm for partition problem for list of capacities
    owner_sum_names = list(set(owner_list))  # ensure unique owner names
    owner_sum_values = [0 for _ in owner_sum_names]

    # sort in decreasing capacity
    sorted_units = sorted(unit_list, key=lambda x: x.capacity, reverse=True)
    sorted_units_nuclear = []
    sorted_units_other = []
    for u in sorted_units:
        if u.fuel.lower() == 'nuclear':
            sorted_units_nuclear.append(u)
        else:
            sorted_units_other.append(u)

    def first_argmin(x):
        """ return first index where x equals its minimum value """
        minx = min(x)
        for (k, v) in enumerate(x):
            if v == minx:
                return k

    # assign nuclear units 2 or 3 units at a time, if possible
    for ulist in grouplist(sorted_units_nuclear, (2, 3)):
        n = first_argmin(owner_sum_values)
        for u in ulist:
            u.owner = owner_sum_names[n]
            owner_sum_values[n] += u.capacity

    for u in sorted_units_other:
        n = first_argmin(owner_sum_values)
        u.owner = owner_sum_names[n]
        owner_sum_values[n] += u.capacity

    # for owner,ownersum in zip(owner_sum_names,owner_sum_values):
    #     print('%s has sum %g'%(owner,ownersum))


def find_partition(n, allowed_sizes):
    """ Find (if possible) a partition of the number n into pieces that all have sizes in allowed_sizes

    Works by depth first search, but randomizing order of out-edges at each vertex

    Not recursive, so can be applied for larger numbers without exceeding stack limit
    :param n:
    :param allowed_sizes:
    :return: list of integers, all elements of allowed_sizes, that sums to n, or empty list if no partition is possible
    """
    def shuffled(x):
        r = list(x).copy()
        random.shuffle(r)
        return r

    stack = [{'n': n, 'k': 0, 'steps': shuffled(allowed_sizes)}]
    while stack:
        if stack[-1]['n'] < 0:
            del stack[-1]
        elif stack[-1]['n'] in stack[-1]['steps']:
            # found solution, back out path that arrived here
            return [sf['steps'][sf['k']-1] for sf in stack[0:-1]] + [stack[-1]['n']]
        else:
            if stack[-1]['k'] == len(stack[-1]['steps']):
                del stack[-1]
            else:
                new_frame = {'n': stack[-1]['n']-stack[-1]['steps'][stack[-1]['k']],
                             'k': 0,
                             'steps': shuffled(allowed_sizes)}
                stack[-1]['k'] += 1
                stack.append(new_frame)
    return []


def generate_mgf(ups, owner_list, capacity_fraction_d, mw_per_owner):
    if not set(capacity_fraction_d.keys()) <= set(ups.keys()):
        raise MageException('capacity fraction keys must all be present in unit profile set')

    generator_capacity_d = {}
    for gtype in capacity_fraction_d:
        generator_capacity_d[ups[gtype]] = capacity_fraction_d[gtype]

    unit_list = generate_nameless_ownerless_unit_set(mw_per_owner * len(owner_list), generator_capacity_d)
    assign_units_to_owners(unit_list, owner_list)
    assign_names_to_units(unit_list, ups)
    marketsimulationname = ''
    mgf = MasterGeneratorFleet(marketsimulationname)
    mgf.generatorunits = unit_list
    return mgf


def generate_nameless_ownerless_unit_set(total_capacity, generator_capacity_d):
    """ Create list of units with empty owner, name fields that correspond to specified fractions for capacity

    :param total_capacity: total desired capacity, in MW
    :param generator_capacity_d: dictionary w/ generator unit generator instances as keys, capacity fractions as values
    :return: list of generator units
    """
    unit_list = []
    for unit_gen, cap_frac in generator_capacity_d.items():
        curr_target_capacity = total_capacity*cap_frac
        curr_units = []
        curr_units_capacity = 0
        while curr_units_capacity < curr_target_capacity:
            u = unit_gen.generate_nameless_unit()
            curr_units_capacity += u.capacity
            curr_units.append(u)
        # now decide whether to remove last unit or not, to be as close as possible to target capacity
        # do nothing if curr_units is empty
        if curr_units:
            if abs(curr_target_capacity-(curr_units_capacity - curr_units[-1].capacity)) < \
               abs(curr_target_capacity-curr_units_capacity):
                del curr_units[-1]
        unit_list += curr_units
    return unit_list


def generate_name_stubs(stub):
    """ return (python) generator corresponding to infinite stream of unique name stubs for given
        (electricity model) generator type stub
    """
    curr_range_block = 0
    range_block_size = 1000
    while True:
        rng = list(range(curr_range_block*range_block_size+1, (curr_range_block+1)*range_block_size))
        random.shuffle(rng)
        for n in rng:
            yield '%s%03g' % (stub, n)
        curr_range_block += 1


def grouplist(x, allowed_sizes):
    """ partition list into list of lists where each element has sizes in allowed_sizes

    Algorithm is based on partitioning len(x) into sum of integers that are all in allowed_sizes, using
    depth first graph search, with order of partition sizes randomized at each vertex of search tree

    Algorithm may fail if no such partition is possible, in this case just return [x]

    :param x: list to be regrouped
    :param allowed_sizes:
    :return:
    """
    part = find_partition(len(x), allowed_sizes)
    if not part:
        r = [x]
    else:
        r = []
        c = 0
        for p in part:
            r.append(x[c:c+p])
            c += p
    return r


def load_ups_from_spreadsheet(wb):
    """ Load unit profile set from spreadsheet and return dictionary of GeneratorUnitGenerator's by generator type

    :param wb:
    :return:
    """
    d = {}
    for sheet in wb:
        x = GeneratorUnitGenerator()
        x.initialize_from_spreadsheet(sheet)
        d[x.generator_type] = x
    return d


def load_usernames_from_spreadsheet(wb):
    return load_usernames_from_spreadsheet_sheet(wb.active)


def load_usernames_from_spreadsheet_sheet(sheet):
    def is_valid_username(usr):
        return all([c in string.ascii_lowercase for c in usr])
    usernames = []
    e = []
    # verify first row
    if not (is_string_list([sheet.cell(row=1, column=1 + n).value for n in range(2)]) and
            sheet.cell(row=1, column=1).value.lower() == 'username'):
        e.append('first row not as expected : should be username in A1')
        return usernames, e
    else:
        for row in sheet.rows[1:]:  # skip first row
            u = str(row[0].value).strip()
            if u:  # silently ignore any rows where username or password are empty
                if not is_valid_username(u):
                    e.append('Username %s invalid, must be all lowercase letters' % u)
                else:
                    usernames.append(u)

    return usernames, e


if __name__ == '__main__':

    def clearscreen():
        os.system('cls' if os.name == 'nt' else 'clear')


    unit_profile_set_fname_default = os.sep.join(['example_spreadsheets', 'marketgen', 'default_unit_profile_set.xlsx'])
    import warnings
    warnings.filterwarnings('ignore', 'Unknown extension')

    import tkinter as tk
    from tkinter import filedialog

    root = tk.Tk()
    root.withdraw()
    clearscreen()
    print('Welcome to the MaGE market configuration generator')
    print('Select unit profile set, or cancel to use default unit profile set')
    unit_profile_set_fname = filedialog.askopenfilename()
    if not unit_profile_set_fname:
        unit_profile_set_fname = unit_profile_set_fname_default

    myups = UnitProfileSet(profilesetname='')
    myups.initialize_from_workbook(load_workbook(unit_profile_set_fname))

    print('Loaded unit profile set from file %s' % unit_profile_set_fname)

    print('Select username file')
    usernames_fname = filedialog.askopenfilename()
    my_usernames, errlist = load_usernames_from_spreadsheet(load_workbook(usernames_fname))
    print('loaded %g users :' % len(my_usernames) + ','.join(my_usernames) + ' from file ' + usernames_fname)

    my_capacity_fraction_d = {}
    sorted_gt = sorted([gt for gt in myups.gug_d])
    print('\nGenerator types are ' + ','.join(sorted_gt))
    gtwidth = max([len(x) for x in sorted_gt])

    my_cf_valid = False
    while not my_cf_valid:
        for gt in sorted([x for x in myups.gug_d]):
            mystg = input(('   Capacity percent for generator type %'+str(gtwidth)+'s ?') % gt)
            my_capacity_fraction_d[gt] = float(mystg) / 100.0
        mythresh = 1e-4
        my_cf = [my_capacity_fraction_d[gt] for gt in myups.gug_d]
        if any([x < 0.0 for x in my_cf]) or \
                abs(sum([my_capacity_fraction_d[gt] for gt in myups.gug_d]) - 1.0) > mythresh:
            print('Given capacities must be positive and sum to 100 percent, please try again')
        else:
            my_cf_valid = True

    my_mw_per_owner = float(input('MW per owner?'))

    mymgf = generate_mgf(myups.gug_d, my_usernames, my_capacity_fraction_d, my_mw_per_owner)

    print('Master generator fleet created. Where do you want to save it?')
    fname = filedialog.asksaveasfilename(defaultextension='.xlsx')
    mywb = mymgf.spreadsheet_repr()
    mywb.save(fname)
    print('Master generator fleet wrote to file %s' % fname)
