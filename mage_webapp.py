# MAGE (Market Analysis Game for Electricity)
#
#  Author : David Hammond, Oregon Tech Portland-Metro
#  2015-2018
import io
import sys
import datetime
import hashlib
import json
from sqlalchemy.orm import scoped_session
import cherrypy
from mage_model import *
from mage_webmarketgen import *
from sqlalchemy import Table
from cherrypy.lib import file_generator

# import html


########################
# Global variables
########################
allowed_credentials = ['admin', 'user']
basedir = os.path.dirname(os.path.abspath(__file__))
database_file = os.sep.join([basedir, 'mage_webapp.sqlite'])
global_css_file = '/mage_webapp.css'
SESSION_KEY = '_cp_username'

########################
# Global methods :
# variables used for HTML formatting, utility methods
########################


def assert_currentsimulation_valid():
    currentsimulation = cherrypy.session.get('_cp_currentsimulation')
    if currentsimulation is None:
        raise cherrypy.HTTPRedirect("/userselectsimulation/")
    else:
        return True


def dict_to_post_args(d):
    return '&'.join(['%s=%s' % (k, d[k]) for k in d])


def form_button(method, action, buttonstg, **kwargs):
    """
    Returns html for form button

    :param method: method of button (get or post)
    :param action: action url
    :param buttonstg: string to display for button
    :param kwargs: for each keyword,value in argument list, a hidden input type will be added to form
    :return: string with html code for form
    :rtype: str
    """
    txt = '<form method="%s" action="%s" style="display:inline">\n' % (method, action)
    for name, value in kwargs.items():
        txt += '<input type="hidden" name="%s" value="%s" />\n' % (name, value)
    txt += '<input type="submit" value="%s" />\n</form>\n' % buttonstg
    return txt


def get_current_marketsimulationname():
    """ return currently selecte market simulation name. Raise exception if it isn't selected"""
    currentsimulation = cherrypy.session.get('_cp_currentsimulation')
    if not currentsimulation:
        raise MageException('no current market simulation selected')
    return currentsimulation


def get_matching_userlist(username):
    """
    Returns list of User objects in database matching supplied username.

    Length of this list should be 0 or 1, an exception will be raised if there is more than 1 user entry
    with matching username. This shouldn't happen.

    :param username: username to search for
    :returns: list of User objects or empty list
    :rtype list[User]
    """
    ulist = [u for u in cherrypy.request.sa_session.query(User).filter_by(username=username)]
    if len(ulist) > 1:
        raise MageDatabaseException('Multiple users in db with username %s' % username)
    return ulist


def get_misc_settings():
    ms = get_object_from_db(MageMiscSettings)
    if not ms:  # create default settings if not in db
        ms = MageMiscSettings(marketsimulationname=get_current_marketsimulationname())
    return ms


def get_object_from_db(cls, **kwargs):
    return get_object_from_db_for_current_simulation(cls, **kwargs)


def get_object_from_db_for_current_simulation(cls, **kwargs):
    currentsimulation = cherrypy.session.get('_cp_currentsimulation')
    return cls.get_from_db(cherrypy.request.sa_session, marketsimulationname=currentsimulation, **kwargs)


def gt_name_format(x):
    return x.lower().strip().replace(' ', '_')


def html_format_messages_and_errors(m, e):
    """
    Takes list of non-eror messages and error messages, returns html

    :param m: list of strings for non-error messages (e.g. indicating success of some action)
    :param e: list of strings for error messages
    :returns: formatted html string
    :rtype: str
    """
    msgtxt = ''
    msgtxt += '<div class=non_error_message_set>'
    for cm in m:
        msgtxt += '<div class="non_error_message"> %s </div>\n' % cm
    msgtxt += '</div>'

    msgtxt += '<div class=error_message_set>'
    for ce in e:
        msgtxt += '<div class="error_message"> %s </div>\n' % ce
    msgtxt += '</div>'

    return msgtxt


def is_admin():
    try:
        curr_user = cherrypy.request.sa_session.query(User).filter_by(username=cherrypy.request.login)[0]
        return curr_user.credentials == 'admin'
    except IndexError:
        pass
    return False


def is_admin_or_user():
    try:
        curr_user = cherrypy.request.sa_session.query(User).filter_by(username=cherrypy.request.login)[0]
        return curr_user.credentials == 'admin' or curr_user.credentials == 'user'
    except IndexError:
        pass
    return False


def mage_app_header():
    """
    Returns formatted header appropriate for top of each page
    :return:

    """
    username = cherrypy.session.get(SESSION_KEY)
    currentsimulation = cherrypy.session.get('_cp_currentsimulation')
    timestr = time_format(datetime.datetime.now())
    if username:
        if currentsimulation:
            txt = """<div class=mage_app_header> MAGE. Logged in as: <span class=hdr_name>%s</span>.
            Market simulation: <span class=hdr_marketsim>%s</span>. Current time is %s </div>
    """ % (username, currentsimulation, timestr)
        else:
            txt = """<div class=mage_app_header> MAGE. Logged in as:  <span class=hdr_name>%s</span>.
            No market simulation selected. Time: %s </div>
    """ % (username, timestr)
    else:
        txt = """<div class=mage_app_header> MAGE. You are not logged in. Time: %s </div>""" % timestr

    return txt


def mage_html_wrap(titlebar=True, pagename='', body='', do_nav_footer=True, ml=None, headstg='',logo=True):
    txt = ''
    if logo:
        txt += '<div class=mage_logo> %s </div>' % mage_logo_txt()
    if titlebar:
        txt += '<div class=mage_titlebar> %s </div>' % mage_titlebar_txt()
    if pagename:
        txt += '<div class=mage_pagename> %s </div>' % pagename
    if body:
        txt += '<div class=mage_body> %s </div>' % body
    if do_nav_footer:
        txt += '<div class=mage_nav_footer> %s </div>' % nav_footer()
    else:
        txt += '<div class=mage_nav_footer> &nbsp; </div>'
    if ml:
        txt += '<div></div>'
        txt += ml.html_format()

#    txt = '<div class=mage_page_wrapper>%s</div>' % txt
    txt = '<div class=mage_background><div class=mage_page_wrapper>%s</div></div>' % txt
    return """<html><head>{headstg}<link rel="stylesheet" type="text/css" href="{globalcss}"></head>
<body> {bodystg} </body></html>""".format(headstg=headstg, globalcss=global_css_file, bodystg=txt)

def mage_logo_txt():
    txt =''
#    txt += '<img src = "/images/mage-logo.png" width=306 height=208>'
    txt += '<img src = "/images/mage-logo-gradmask.png" width=800 height=210>'

    return txt

def mage_titlebar_txt():
    """
    Returns formatted header appropriate for top of each page
    :return:

    """
    username = cherrypy.session.get(SESSION_KEY)
    currentsimulation = cherrypy.session.get('_cp_currentsimulation')
    timestr = time_format(datetime.datetime.now())
    if username:
        if currentsimulation:
            txt = """Logged in as: <span class=hdr_name>%s</span>.
            Market simulation: <span class=hdr_marketsim>%s</span>. Current time is %s
            """ % (username, currentsimulation, timestr)
        else:
            txt = """Logged in as:  <span class=hdr_name>%s</span>.
            No market simulation selected. Time: %s """ % (username, timestr)
    else:
        txt = """You are not logged in. Time: %s""" % timestr

    return txt


def nav_footer():
    """
    Returns navigation footer, depending on user credentials

    :returns: string of navigation footer
    :rtype: str
    """
    if is_admin():
        txt = """
<a href="/"> Main Page </a> |
<a href="/auth/logout"> Logout </a>  |
<a href="/admin"> Admin Market Configuration </a> |
<a href="/admin/view_market_console"> Admin Market Console </a> |
<a href="/adminsimulations/"> Admin Market Simulation List </a> 
"""
    else:
        txt = """
<a href="/"> Main Page </a>  | 
<a href="/user/change_user_password"> Change User Password </a> |
<a href="/auth/logout"> Logout </a>"""
    return txt

def stairstep_interpolate(x, y):
    """ x,y are numpy arrays, same size
    returns new x,y arrays that have stairstep interpolation points added
    """
    num_pts = len(x)
    xnew = np.zeros(2 * num_pts - 1)
    ynew = np.zeros(2 * num_pts - 1)
    xnew[0] = x[0]
    ynew[0] = y[0]
    for n in range(1, num_pts):
        xnew[2*n - 1] = x[n]
        xnew[2*n] = x[n]
        ynew[2*n - 1] = y[n-1]
        ynew[2*n] = y[n]
    return xnew,ynew


def string_to_bool(confirm):
    """
    Converts string to python bool.

    Needed as string 'False' is true-ey

    :param confirm: typically string "True" or "False"
    :returns : boolean True or False
    :rtype: bool
    """
    return {'True': True, 'true': True}.get(confirm, False)


def market_configuration_complete():
    """
    Determines if market configuration is complete, so that bids may be accepted

    :return: bool
    :rtype: bool
    """
    return bool(get_object_from_db(MasterGeneratorFleet) and
                get_object_from_db(BidTimeSchedule) and
                get_object_from_db(DailyTimeSegments))


class MessageLog(GenericMessageLogger):
    allowed_keys = ['e', 'm']

    ########################
    # magic methods
    ########################

    def __init__(self, m=None, e=None):
        super().__init__()
        if m:
            self.log('m', m)
        else:
            self.log('m', [])
        if e:
            self.log('e', e)
        else:
            self.log('e', [])

    def __repr__(self):
        return 'e:'+repr(self.e)+'\n'+'m:'+repr(self.m)

    ########################
    # regular  methods
    ########################

    def add_error(self, msg):
        self.log('e', msg)

    def add_message(self, msg):
        self.log('m', msg)

    @classmethod
    def from_json(cls, stg):
        d = json.loads(stg)
        return cls(m=d['m'], e=d['e'])

    def html_format(self):
        """
    Returns html representation of error and non-error messages

    :returns: formatted html string
    :rtype: str
    """
        msgtxt = ''
        msgtxt += '<div class=non_error_message_set>'
        for cm in self.m:
            msgtxt += '<div class="non_error_message"> %s </div>\n' % cm
        msgtxt += '</div>'
        msgtxt += '<div class=error_message_set>'
        for ce in self.e:
            msgtxt += '<div class="error_message"> %s </div>\n' % ce
        msgtxt += '</div>'
        return msgtxt

    def log(self, key, msg):
        if key in self.allowed_keys:
            super().log(key, msg)
        else:
            raise MageException('Invalid key '+str(key))

    def to_json(self):
        return json.dumps({'m': self.m, 'e': self.e})


class User(Base):
    """
User class : represents users, names, credentials, password hashes
"""
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    username = Column(String)
    salt = Column(String)
    passwordhash = Column(String)
    credentials = Column(String)

    ########################
    # magic methods
    ########################

    def __init__(self, username, credentials, newpassword):
        self.username = username
        self.credentials = credentials
        self.update_password(newpassword)

    def __repr__(self):
        return ("User with fields -\n\t username={username} \n \t salt={salt}\n "
                "\t credentials={credentials}\n \t passwordhash={passwordhash}"
                ).format(username=self.username, salt=self.salt,
                         credentials=self.credentials, passwordhash=self.passwordhash)

    ########################
    # regular methods
    ########################

    def is_valid_password(self, password):
        """Returns true if provided password is this users password"""
        return self.pwhash(password, self.salt) == self.passwordhash

    def update_password(self, newpassword):
        self.salt = self.generate_salt()  # generate new random salt
        self.passwordhash = self.pwhash(newpassword, self.salt)

    ########################
    # static methods
    ########################
    @staticmethod
    def generate_salt():
        """ 32 bytes of salt, hex encoded """
        # line below requires python3.5
        # return os.urandom(32).hex()
        # line below equivalent
        saltstr = "".join(["%02x" % b for b in os.urandom(32)])
        return saltstr

    @staticmethod
    def problems_with_username_password_credentials(username, password, credentials):
        """Return None if username, password and credentials are allowed, otherwise return string
        indicating the problem(s)."""
        un_min = 3
        pw_min = 4

        def made_of_allowed_chars(stg, allowed_chars):
            return all([c in allowed_chars for c in stg])

        un_ok = (isinstance(username, str) and
                 len(username) >= un_min and
                 made_of_allowed_chars(username, string.ascii_letters + '.'))
        pw_ok = (isinstance(password, str) and
                 len(password) >= pw_min and
                 made_of_allowed_chars(password, string.digits + string.punctuation + string.ascii_letters))
        credentials_ok = credentials in allowed_credentials
        err = []
        if not un_ok:
            err.append('username must be at least %d alphabetical characters and contain no spaces' % un_min)
        if not pw_ok:
            err.append('password must be at least %d characters and contain no spaces' % pw_min)
        if not credentials_ok:
            err.append('credential must be one of [ %s ]' % ','.join(allowed_credentials))
        if not err:
            return None
        else:
            return ' '.join(err)

    @staticmethod
    def pwhash(pw, salt):
        m = hashlib.sha256()
        m.update((pw + salt).encode('utf-8'))
        return m.hexdigest()


msi_user_association_table = Table('association', Base.metadata,
                                   Column('marketsimulationinstances_name', String,
                                          ForeignKey('marketsimulationinstances.name')),
                                   Column('users_id', Integer, ForeignKey('users.id')))


class MarketSimulationInstance(Base, StoredSingleInDB):
    """Class representing seperate market simulations. Ultimately, every database entry that
    is specific to a single market simulation will have a MaketSimulationInstance field, so that the system
    can keep track of which objects belong to which market simulation"""
    __tablename__ = 'marketsimulationinstances'
    name = Column(String, primary_key=True)
    users = relationship('User', secondary=msi_user_association_table)

    def __init__(self, name, users):
        self.name = name
        self.users = users


class AdminAccess(object):
    """Class containing all methods corresponding to and used by url endpoints that should only be accessible to admin
"""
    _cp_config = {'auth.require': [is_admin, assert_currentsimulation_valid]}

    ########################
    # exposed regular methods (defining url endpoints)
    ########################

    @cherrypy.expose
    def adduser(self, username=None, password=None, credentials=None, parent_url='/admin/'):
        """URL endpoint for adding user"""
        ml = MessageLog()
        if username is None or password is None or credentials not in allowed_credentials:
            pass
        else:
            ulist = get_matching_userlist(username)
            if len(ulist) == 1:
                ml.add_error('user %s already exists, cannot add' % username)
            else:
                ml += self._adduser(username, credentials, password)
        raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json='+ml.to_json())

    @cherrypy.expose
    def add_user_to_ms(self, username, msi_name, parent_url='/admin/listusers/'):
        ml = MessageLog()

        msi_list = list(cherrypy.request.sa_session.query(MarketSimulationInstance).filter_by(name=msi_name))
        ulist = get_matching_userlist(username)
        if not ulist:
            ml.add_error('User %s not present' % username)
        else:
            if len(msi_list) == 0:
                ml.add_error('Market simulation %s not present' % msi_name)
            elif len(msi_list) == 1:
                user = ulist[0]
                msi = msi_list[0]
                if user in msi.users:
                    ml.add_error('User %s already in market simulation %s' % (username, msi_name))
                else:
                    msi.users.append(user)
                    cherrypy.request.sa_session.commit()
                    ml.add_message('User %s added to market simulation %s' % (username, msi_name))
            else:
                raise MageException('more than one msi found with name %s' % msi_name)
        raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json='+ml.to_json())

    @cherrypy.expose
    def admin_delete_bid(self, username, bidround, confirm=False, parent_url='/admin/view_all_bids'):
        bidround = int(bidround)
        confirm = string_to_bool(confirm)
        bid = get_object_from_db(Bid, username=username, bidround=int(bidround))
        ml = MessageLog()
        if not bid:
            ml.add_error('No bid present to delete')
        else:
            if confirm:
                ml += self._admin_delete_bid(username, bidround)
            else:
                ml.add_message('Really delete bid for user %s, bidround %d?' % (username, bidround) +
                               self.admin_delete_bid_button_code(username=username, bidround=bidround, confirm=True))
        qs = dict_to_post_args({'message_log_json': ml.to_json, 'bidround': str(bidround)})
        raise cherrypy.InternalRedirect(parent_url, query_string=qs)

    @cherrypy.expose
    def admin_view_bid(self, username, bidround):
        bidround = int(bidround)
        bid = get_object_from_db(Bid, username=username, bidround=bidround)
        if bid:
            txt = '<div> Bid for user %s, bidround %d</div>' % (username, bidround)
            txt += '<div> %s </div>' % bid.html_repr()
        else:
            txt = '<div> Bid for user %s, bidround %d not present </div>' % (username, bidround)
        return mage_html_wrap(pagename='View Bid', body=txt)

    @cherrypy.expose
    def changeuser(self, username='', password=None, credentials=None, confirm=False, parent_url='/admin/listusers/'):
        """ url endpoint for changing user password and/or credentials """
        ulist = get_matching_userlist(username)
        ml = MessageLog()
        if len(ulist) == 1:
            if confirm:
                if credentials is not None:
                    ml += self._changecredentials(username, credentials)
                if password is not None:
                    ml += self._changepassword(username, password)
            else:
                cptxt = """<div> Change password for user {username} :
                <form method="post" action="/admin/changeuser" style="display:inline">
                <input type="password" name="password" value="" />
                <input type="hidden" name="confirm" value="true"/>
                <input type="hidden" name="username" value="{username}"/>
                <input type="submit" value="change password"/> </form> </div>""".format(username=username)

                opttxt = ''
                if ulist[0].credentials == 'admin':
                    opttxt += """<option value="user" > user </option>
                    <option value="admin" selected="selected" > admin </option>"""
                if ulist[0].credentials == 'user':
                    opttxt += """<option value="user" selected="selected" > user </option>
                    <option value="admin"> admin </option> """

                cctxt = """<div> Change credentials for user {username} :
                <form method="post" action="/admin/changeuser" style="display:inline">
                <input type="hidden" name="username" value="{username}"/>
                <input type="hidden" name="confirm" value="true"/>
                <select name="credentials"> {opttxt} </select>
                <input type="submit" value="change credentials"/> </form> </div>
                """.format(username=username, opttxt=opttxt)

                # build list of available market simulations
                msi_list = cherrypy.request.sa_session.query(MarketSimulationInstance).filter_by()
                available_msi_txt = ''
                for msi in msi_list:
                    available_msi_txt += '<option value=%s> %s </option>' % (msi.name, msi.name)
                add_to_ms_txt = """<div> Add user {username} to market simulation
                <form method="post" action="/admin/add_user_to_ms" style="display:inline">
                <input type="hidden" name="username" value="{username}"/>
                <select name="msi_name"> {available_msi_txt} </select>
                <input type="submit" value="add to market simulation"/> </form> </div>
                """.format(username=username, available_msi_txt=available_msi_txt)

                # build list of market simulations user is currently associated with
                msi_names_by_user = self.get_msi_names_by_user()
                accessible_msi_names = msi_names_by_user.get(username, [])
                accessible_msi_names_txt = ''
                for msi_name in accessible_msi_names:
                    accessible_msi_names_txt += '<option value=%s> %s </option>' % (msi_name, msi_name)
                remove_from_ms_txt = """<div> Remove user {username} from market simulation
                <form method="post" action="/admin/remove_user_from_ms" style="display:inline">
                <input type="hidden" name="username" value="{username}"/>
                <select name="msi_name"> {accessible_msi_names_txt} </select>
                <input type="submit" value="remove from market simulation"/> </form> </div>
                """.format(username=username, accessible_msi_names_txt=accessible_msi_names_txt)

                txt = cptxt + cctxt + add_to_ms_txt + remove_from_ms_txt
                ml.add_message(txt)
        else:
            ml.add_error('user %s does not exist, cannot change' % username)
        raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json='+ml.to_json())

    @cherrypy.expose
    def create_bts(self, **kwargs):
        """ url endpoint for editing bid time schedule
        By passing parameters into kwargs, this represents the state of a bid time schedule spreadsheet
        """
        parent_url = '/admin/create_bts/'

        if 'message_log_json' in kwargs:
            ml = MessageLog.from_json(kwargs['message_log_json'])
        else:
            ml = MessageLog()

        if kwargs.get('update', '') not in ['create', 'update']:
            # check if bid time schedule in database
            oldbts = get_object_from_db(BidTimeSchedule)
            if oldbts:
                d_bts = {}
                num_bidrounds = len(oldbts.bidtimes)
                d_bts['num_bidrounds'] = str(num_bidrounds)
                for (r, st, et) in oldbts.bidtimes:
                    st_txt = 'start_time%03d' % r
                    st_val = time_format(st)
                    et_txt = 'end_time%03d' % r
                    et_val = time_format(et)
                    d_bts[st_txt] = st_val
                    d_bts[et_txt] = et_val

                qs = dict_to_post_args(d_bts)
                qs += '&update=update'
                raise cherrypy.InternalRedirect(parent_url, query_string=qs)

        if 'num_bidrounds' in kwargs:
            num_bidrounds = int(kwargs['num_bidrounds'])
        else:
            num_bidrounds = 0

        if kwargs.get('update', '') == 'create':
            # attempt to build spreadsheet from kwargs
            wb = Workbook()
            sheet = wb.active
            # working here
            sheet.cell(row=1, column=1).value = 'Round'
            sheet.cell(row=1, column=2).value = 'Bid open time'
            sheet.cell(row=1, column=3).value = 'Bid close time'
            for br in range(1, num_bidrounds+1):
                sheet.cell(row=1+br, column=1).value = br
                st_txt = "start_time%03d" % br
                st_val = kwargs.get(st_txt, '')
                et_txt = "end_time%03d" % br
                et_val = kwargs.get(et_txt, '')
                try:
                    st_dt = datetime.datetime.strptime(st_val, '%m/%d/%Y %H:%M')
                    sheet.cell(row=1 + br, column=2).value = st_dt
                    if not st_dt:
                        raise ValueError
                except ValueError:
                    ml.add_error('Incorrect start date format for bidround %d' % br)
                try:
                    et_dt = datetime.datetime.strptime(et_val, '%m/%d/%Y %H:%M')
                    sheet.cell(row=1 + br, column=3).value = et_dt
                    if not st_dt:
                        raise ValueError
                except ValueError:
                    ml.add_error('Incorrect end date format for bidround %d' % br)
            if not ml.e:
                newbts = BidTimeSchedule(marketsimulationname=get_current_marketsimulationname())
                e = newbts.initialize_from_spreadsheet(sheet)
                if e:
                    ml.add_error(e)
                else:
                    # newbts is valid. Overwrite current bts if it exists.
                    oldbts = get_object_from_db(BidTimeSchedule)
                    if oldbts:
                        ml.add_message('Overwriting current bid time schedule')
                        cherrypy.request.sa_session.delete(oldbts)
                        cherrypy.request.sa_session.add(newbts)
                    else:
                        cherrypy.request.sa_session.add(newbts)
                    cherrypy.request.sa_session.commit()
                    ml.add_message('Added new bid time schedule')

            kwargs['update'] = 'update'
            qs = dict_to_post_args(kwargs)
            qs += '&message_log_json='+ml.to_json()
            raise cherrypy.InternalRedirect(parent_url, query_string=qs)

        # code below builds form from kwargs, and displays

        txt = 'Enter start/end times in format mm/dd/yyyy hh:mm, using 24 hour time <br> <br>'
        txt += """<form method="post" action="/admin/create_bts" enctype="multipart/form-data">
        Number of Bid Rounds <input type="text" name="num_bidrounds" value = "{str_num_bidrounds}"/>
        <input type="submit" name="update" value = "update"/>
        """.format(str_num_bidrounds=str(num_bidrounds))
        # working here
        txt += '<table class="create_bts">'
        txt += '<tr><td> Bid round</td> <td> Start time</td> <td> End time </td> </tr>'
        for br in range(1, num_bidrounds+1):
            st_txt = "start_time%03d" % br
            st_val = kwargs.get(st_txt, '')
            et_txt = "end_time%03d" % br
            et_val = kwargs.get(et_txt, '')
            txt += """<tr><td> {br} </td> 
            <td> <input type="text" name="{st_txt}" value="{st_val}"/></td> 
            <td> <input type="text" name="{et_txt}" value="{et_val}"/> </td>
            </tr>""".format(br=str(br), st_txt=st_txt, st_val=st_val, et_txt=et_txt, et_val=et_val)
        txt += '</table>'
        txt += '<br> <input type="submit" name="update" value="create" /> </form>'

        return mage_html_wrap(pagename='Bid time schedule tool', body=txt, ml=ml)

    @cherrypy.expose
    def create_profitability_report(self, bidround, update, parent_url='/admin/view_market_console'):
        ml = MessageLog()
        bidround = int(bidround)
        mgf = get_object_from_db(MasterGeneratorFleet)
        omr = get_object_from_db(OverallMarketReport, bidround=bidround)
        if mgf and omr:
            opr = OverallProfitabilityReport(bidround=bidround, mgf=mgf, omr=omr)
            opr.compute()
            wb = opr.spreadsheet_repr()
            if update == 'view':
                txt = ''
                for sheet in wb:
                    txt += excel_worksheet_to_html_table(sheet, tableclass="market_conditions")
                    txt += '<br><br>'
                return mage_html_wrap(pagename='View Profitability report', body=txt)
            if update == 'download':
                buff = io.BytesIO()
                wb.save(buff)
                buff.seek(0)
                filename = 'overall_profitability_round_%d.xlsx' % bidround
                return cherrypy.lib.static.serve_fileobj(buff, 'application/x-download', 'attachment', filename)

        else:
            ml.add_error('Need overall market report to generate profitability report')
            raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json=' + ml.to_json())

    @cherrypy.expose
    def create_cumulative_profitability_report(self, update):
        mgf = get_object_from_db(MasterGeneratorFleet)
        bts = get_object_from_db(BidTimeSchedule)
        bidroundlist = [r for (r, _, _) in bts]

        wb = Workbook()
        sheet = wb.active

        opr_by_bidround = {}
        for r in bidroundlist:
            omr = get_object_from_db(OverallMarketReport, bidround=r)
            if omr:
                opr_by_bidround[r] = OverallProfitabilityReport(bidround=r, mgf=mgf, omr=omr)
                opr_by_bidround[r].compute()

        currentsimulation = get_current_marketsimulationname()
        msi = cherrypy.request.sa_session.query(MarketSimulationInstance).filter_by(name=currentsimulation)[0]
        usernames = [u.username for u in msi.users]

        bidround_list = sorted(opr_by_bidround.keys())
        column_headings = ['user \ round'] + bidround_list + ['Total profit']
        pt = WSpoint(row=1, col=1)
        pt = write_list_to_sheet(sheet, pt, textlists=[column_headings])
        #pt = WSpoint(row=pt.row + 1, col=1)
        pt = WSpoint(row=pt.row + 1, col=1)

        for u in usernames:
            profit_list = [opr_by_bidround[r].total_profit_by_owner[u] for r in bidround_list]
            tl = [[u] + profit_list + ['%.2f' % sum(profit_list)]]
            pt = write_list_to_sheet(sheet, pt, textlists=tl)
            pt = WSpoint(row=pt.row + 1, col=1)

        if update == 'view':
            txt = excel_worksheet_to_html_table(sheet, tableclass='cumulative_profitability')
            return mage_html_wrap(pagename='Cumulative profitability for all users', body=txt)
        if update == 'download':
            buff = io.BytesIO()
            wb.save(buff)
            buff.seek(0)
            filename = 'cumulative_profitability.xlsx'
            return cherrypy.lib.static.serve_fileobj(buff, 'application/x-download', 'attachment', filename)
        return ''  # this shouldn't be reached

    @cherrypy.expose
    def delete_bts(self, confirm=False, parent_url='/admin/'):
        """url endpoint for deleting bid time schedule"""
        # confirm = {'True': True}.get(confirm, False)  # convert from string to python bool
        ml = MessageLog()
        if not get_object_from_db(BidTimeSchedule):
            ml.add_error('Bid time schedule not loaded, cannot delete')
        else:
            if confirm == 'True':  # note confirm is a string not a python bool
                ml += self._delete_bts()
            else:
                ml.add_message('Really delete bid time schedule?' + self.delete_bts_button_code(confirm=True))
        raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json='+ml.to_json())

    @cherrypy.expose
    def delete_mgf(self, confirm=False, parent_url='/admin/', **kwargs):
        """url endpoint for deleting master generator fleet"""
        ml = MessageLog()
        if not get_object_from_db(MasterGeneratorFleet):
            ml.add_error('Master generator fleet description not loaded, cannot delete')
        else:
            if confirm:
                ml += self._delete_mgf()
            else:
                ml.add_message('Really delete master generator fleet?' +
                               self.delete_mgf_button_code(confirm=True, parent_url=parent_url, hidden_params=kwargs))
        qs = dict_to_post_args({**{'message_log_json': ml.to_json()}, **kwargs})

        raise cherrypy.InternalRedirect(parent_url, query_string=qs)

    @cherrypy.expose
    def download_bts(self, parent_url='/admin/'):
        ml = MessageLog()
        bts = get_object_from_db(BidTimeSchedule)
        if bts:
            wb = bts.spreadsheet_repr()
            buff = io.BytesIO()
            wb.save(buff)
            buff.seek(0)
            filename = 'bid_time_schedule.xlsx'
            return cherrypy.lib.static.serve_fileobj(buff, 'application/x-download', 'attachment', filename)
        else:
            ml.add_error('No bid time schedule present, cannot download')
            raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json='+ml.to_json())

    @cherrypy.expose
    def delete_amc(self, bidround, confirm=False, parent_url='/admin/view_market_console/'):
        """url endpoint for deleting actual market conditions"""
        bidround = int(bidround)
        ml = MessageLog()
        if not get_object_from_db(ActualMarketConditions, bidround=bidround):
            ml.add_error('Actual market conditions for bidround %d not loaded, cannot delete' % bidround)
        else:
            if confirm == 'True':
                ml += self._delete_amc(bidround)
            else:
                ml.add_message(('Really delete actual market conditions for bidround '
                                '%d?') % bidround + self.delete_amc_button_code(bidround, confirm=True))
        raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json='+ml.to_json())

    @cherrypy.expose
    def delete_dtseg(self, confirm=False, parent_url='/admin/'):
        """url endpoint for deleting daily time segments time schedule"""
        # confirm = {'True': True}.get(confirm, False)  # convert from string to python bool
        ml = MessageLog()
        dtseg = get_object_from_db(DailyTimeSegments)
        if not dtseg:
            ml.add_error('Daily time segments not loaded, cannot delete')
        else:
            if confirm == 'True':  # note confirm is a string not a python bool
                ml += self._delete_dtseg()
            else:
                # find stored objects that depend on current dtseg
                pdict = {'Bid': Bid,
                         'Forecast Market Conditions': ForecastMarketConditions,
                         'Actual Market Conditions': ActualMarketConditions}
                for k, v in pdict.items():
                    stg = '%d dependent %s object(s) will also be deleted' % (
                          cherrypy.request.sa_session.query(v).filter_by(daily_time_segments=dtseg).count(), k)
                    ml.add_message(stg)
                ml.add_message('Really delete daily time segments?' + self.delete_dtseg_button_code(confirm=True))
        raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json='+ml.to_json())

    @cherrypy.expose
    def delete_fmc(self, bidround, confirm=False, parent_url='/admin/view_market_console/'):
        """url endpoint for deleting forecast market conditions"""
        bidround = int(bidround)
        ml = MessageLog()
        if not get_object_from_db(ForecastMarketConditions, bidround=bidround):
            ml.add_message('Forecast market conditions for bidround %d not loaded, cannot delete' % bidround)
        else:
            if confirm == 'True':
                ml += self._delete_fmc(bidround)
            else:
                ml.add_message(('Really delete forecast market conditions for bidround '
                                '%d?') % bidround + self.delete_fmc_button_code(bidround, confirm=True))
        raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json='+ml.to_json())

    @cherrypy.expose
    def delete_omr(self, bidround, confirm=False, parent_url='/admin/view_market_console/'):
        """url endpoint for deleting overall market report"""
        bidround = int(bidround)
        ml = MessageLog()
        if not get_object_from_db(OverallMarketReport, bidround=bidround):
            ml.add_error('Overall market report for bidround %d not present, cannot delete' % bidround)
        else:
            if confirm == 'True':
                ml += self._delete_omr(bidround)
            else:
                ml.add_message(('Really delete overall market report for bidround '
                                '%d ?') % bidround + self.delete_omr_button_code(bidround, confirm=True))
        raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json='+ml.to_json())

    @cherrypy.expose
    def deleteuser(self, username=None, confirm=False, parent_url='/admin/listusers/'):
        """ url endpoint for deleting user """
        # check if user exists
        # if so, ask for confirmation
        # if user exists and confirmation has been given, delete user and display message
        ulist = get_matching_userlist(username)
        ml = MessageLog()
        if len(ulist) == 1:
            if confirm == 'True':
                # delete and generate user deleted message
                ml += self._deleteuser(username)
            else:
                ml.add_message('Really delete user %s ? ' % username + self.delete_user_button_code(username,
                                                                                                    confirm=True))
        else:
            ml.add_error('User %s does not exist, cannot delete' % username)
        raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json='+ml.to_json())

    @cherrypy.expose
    def download_dtseg(self, parent_url='/admin/'):
        dts = get_object_from_db(DailyTimeSegments)
        if dts:
            wb = dts.spreadsheet_repr()
            buff = io.BytesIO()
            wb.save(buff)
            buff.seek(0)
            filename = 'daily_time_segments.xlsx'
            return cherrypy.lib.static.serve_fileobj(buff, 'application/x-download', 'attachment', filename)
        else:
            ml = MessageLog()
            ml.add_error('Daily time segments not present, cannot download')
            raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json='+ml.to_json())

    @cherrypy.expose
    def download_mgf(self, parent_url='/admin/'):
        mgf = get_object_from_db(MasterGeneratorFleet)
        if mgf:
            wb = mgf.spreadsheet_repr()
            buff = io.BytesIO()
            wb.save(buff)
            buff.seek(0)
            filename = 'master_generator_fleet.xlsx'
            return cherrypy.lib.static.serve_fileobj(buff, 'application/x-download', 'attachment', filename)

        else:
            ml = MessageLog(e='Master generator fleet not present, cannot download')
            raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json='+ml.to_json())

    @cherrypy.expose
    def download_omr(self, bidround):
        omr = get_object_from_db(OverallMarketReport, bidround=bidround)
        mgf = get_object_from_db(MasterGeneratorFleet)

        wb = omr.spreadsheet_repr(mgf)
        # serve the file
        buff = io.BytesIO()
        wb.save(buff)
        buff.seek(0)
        filename = 'overall_market_report_bidround%d.xlsx' % int(bidround)
        return cherrypy.lib.static.serve_fileobj(buff, 'application/x-download', 'attachment', filename)

    @cherrypy.expose
    def index(self, message_log_json=MessageLog().to_json()):
        """
:param:str message_log_json:  json-encoded MessageLog
:return:
"""
        def marketconf_div_wrap(intxt, doneflag):
            if doneflag:
                return "<div class = marketconfsep_done> %s </div>" % intxt
            else:
                return "<div class = marketconfsep_notdone> %s </div>" % intxt

        currentsimulation = get_current_marketsimulationname()
        try:
            msi = [u for u in
                   cherrypy.request.sa_session.query(MarketSimulationInstance).filter_by(name=currentsimulation)][0]
            numusers = len(msi.users)
        except IndexError:
            numusers = 0

        admin_area_divs = []

        users_div = """
        <div class="marketconfsep_hdr"> Configure Users (current simulation has {numusers} users) :</div> <br>
        
Create multiple users by uploading username/passwords 
<form method="post" action="/admin/upload_username_password_file" style="display:inline" enctype="multipart/form-data">
<input type="file" name = "username_password_file" />
<input type="submit" value="upload"/>
</form>

<br> <br>

List users <form action="/admin/listusers" style="display:inline"> <input type="submit" value = "list"/> </form>

<br> <br>

Add user <form method="post" action="/admin/adduser" style="display:inline">
          Username : <input type="text" name="username" value = "">
          Password : <input type="password" name="password" value = "">
          Credentials : <select name="credentials">
          <option value="user"> user </option>
          <option value="admin"> admin </option>
          </select>
          <input type="submit" value ="add" /> </form>
""".format(numusers=numusers)

        users_div = marketconf_div_wrap(users_div, numusers > 0)
        # if numusers > 0 :
        #     users_div = "<div class = marketconfsep_done> %s </div>" % users_div
        # else:
        #     users_div = "<div class = marketconfsep_notdone> %s </div>" % users_div

        admin_area_divs.append(users_div)

# <form method="get" action="/admin/upload_username_password_file_help" style="display:inline">
# <input type="submit" value="help"/></form>
        mgf = get_object_from_db(MasterGeneratorFleet)
        mgf_loaded = not not mgf
        mgf_div = """ <div class="marketconfsep_hdr"> Configure Master Generator Fleet Description """
        if mgf_loaded:
            mgf_div += "(current simulation has a master generator fleet) </div> <br>"
        else:
            mgf_div += "(current simulation has no master generator fleet) </div> <br>"

        # determine if master generator fleet is loaded
        if mgf_loaded:
            #             mgf_div += """
            # View, download or delete current Master Generator Fleet Description :
            # {view_mgf_button} {download_mgf_button} {delete_mgf_button}
            #  """.format(view_mgf_button=form_button('get', '/admin/view_mgf', 'view'),
            #             download_mgf_button=form_button('get', '/admin/download_mgf', 'download'),
            #             delete_mgf_button=self.delete_mgf_button_code())
            mgf_div += 'View, download or delete current Master Generator Fleet Description : '
            mgf_div += form_button('get', '/admin/view_mgf', 'view')
            mgf_div += form_button('get', '/admin/download_mgf', 'download')
            mgf_div += self.delete_mgf_button_code()
        else:
            mgf_div += """
Upload new Master Generator Fleet Description:
<form method="post" action="/admin/upload_mgf" style="display:inline"  enctype="multipart/form-data">
<input type="file" name = "mgf_spreadsheet_file" />
<input type="submit" value = "upload"/></form>
"""

        mgf_div += """ <br> <br>
Create a new Master Generator Fleet Description using the 
<a href="/admin/marketgen_profile"> Master Generator Fleet Description Creation tool </a>
"""

        # <form method="get" action="/admin/upload_mgf_help" style="display:inline">
        # <input type="submit" value="help"/></form>

        mgf_div = marketconf_div_wrap(mgf_div, mgf_loaded)
        # if mgf_loaded:
        #     mgf_div = "<div class = marketconfsep_done> %s </div>" % mgf_div
        # else:
        #     mgf_div = "<div class = marketconfsep_notdone> %s </div>" % mgf_div

        admin_area_divs.append(mgf_div)

        bts_done = not not get_object_from_db(BidTimeSchedule)
        bts_div = """ <div class="marketconfsep_hdr"> Configure Bid Time Schedule """
        if bts_done:
            bts_div += "(current simulation has a bid time schedule) </div> <br>"
        else:
            bts_div += "(current simulation has no bid time schedule) </div><br>"

        if bts_done:
            #             bts_div += """
            # View, download or delete current Bid Time Schedule: {view_bts_button} {dowload_bts_button}
            #  {delete_bts_button}
            # """.format(delete_bts_button=self.delete_bts_button_code(),
            #          view_bts_button=form_button('get', '/user/view_bts', 'view'),
            #          dowload_bts_button=form_button('get', '/admin/download_bts/', 'download', parent_url='/admin/'))
            bts_div += 'View, download or delete current Bid Time Schedule: '
            bts_div += form_button('get', '/user/view_bts', 'view')
            bts_div += form_button('get', '/admin/download_bts/', 'download', parent_url='/admin/')
            bts_div += self.delete_bts_button_code()
        else:
            #             bts_div += """
            # Upload new Bid Time Schedule : {upload_bts_button} {bts_help_button}
            # """.format(upload_bts_button=self.upload_bts_button_code(),
            #            bts_help_button='')
            bts_div += 'Upload new Bid Time Schedule : '
            bts_div += self.upload_bts_button_code()

        # self.bts_help_button_code()
        bts_div += "<br> <br> Create new or edit current Bid Time Schedule using the "
        bts_div += "<a href=/admin/create_bts> Bid Time Schedule tool</a>"
        bts_div = marketconf_div_wrap(bts_div, bts_done)
        admin_area_divs.append(bts_div)

        dts_done = not not get_object_from_db(DailyTimeSegments)
        dts_div = """<div class="marketconfsep_hdr"> Configure daily time segments """
        if dts_done:
            dts_div += "(current simulation has daily time segments) </div> <br>"
        else:
            dts_div += "(current simulation has no daily time segments) </div> <br>"

        if dts_done:
            dts_div += """
View, download or delete current Daily Time Segments : {view_dtseg_button} {download_dtseg_button} {delete_dtseg_button}
""".format(delete_dtseg_button=self.delete_dtseg_button_code(),
                download_dtseg_button=form_button('get', '/admin/download_dtseg', 'download', parent_url='/admin/'),
                view_dtseg_button=self.view_dtseg_button_code(),
                dtseg_help_button='')
        else:
            dts_div += """
Upload new Daily Time Segments : {upload_dtseg_button} {dtseg_help_button}
""".format(upload_dtseg_button=self.upload_dtseg_button_code(),
                dtseg_help_button='')

        # select one of default daily time segments
        defaults_dtslist = [o for o in cherrypy.request.sa_session.query(DefaultDailyTimeSegments)]
        default_dts_names = [dts.dailytimesegmentsname for dts in defaults_dtslist]
        # get
        dts_div += """ 
        <br><br> Select Daily Time Segments from available defaults: 
        <form method="post" action="/admin/select_dts" enctype="multipart/form-data" style="display:inline">
        <select name="dts_name" style="width: 150px;">"""
        for name in default_dts_names:
            dts_div += '<option value="%s"> %s </option>' % (name, name)
        dts_div += '</select>'
        dts_div += '<input type="submit" value="select" /> </form>'
        dts_div = marketconf_div_wrap(dts_div, dts_done)
        admin_area_divs.append(dts_div)

        # market conditions
        mc_done = False

        if not market_configuration_complete():
            mc_div = '<div class="marketconfsep_hdr"> Forecast / Actual market conditions </div>'
            mc_div += 'Forecast / Actual market conditions cannot be configured until rest of market is configured'
        else:
            bts = get_object_from_db(BidTimeSchedule)
            fmc_by_round = {r: get_object_from_db(ForecastMarketConditions, bidround=r) for (r, _, _) in bts}
            amc_by_round = {r: get_object_from_db(ActualMarketConditions, bidround=r) for (r, _, _) in bts}
            num_bidrounds = len(bts.bidtimes)
            num_fmc_present = sum([x is not None for x in fmc_by_round.values()])
            num_amc_present = sum([x is not None for x in amc_by_round.values()])
            if num_amc_present == num_bidrounds and num_fmc_present == num_bidrounds:
                mc_done = True
            mc_div = '<div class="marketconfsep_hdr"> Forecast / Actual market conditions ' \
                     '(current simulation has %d/%d forecast, %d/%d actual)</div> <br>' % (
                         num_fmc_present, num_bidrounds, num_amc_present, num_bidrounds)
            mc_div += 'View, download, delete or upload market conditions with the '
            mc_div += '<a href="/admin/view_market_console"> Admin Market Console</a> <br><br>'
            mc_div += 'Edit market conditions with the '
            mc_div += '<a href="/admin/marketconditions_edit"> Edit Market Conditions tool</a>'

        mc_div = marketconf_div_wrap(mc_div, mc_done)
        admin_area_divs.append(mc_div)

        miscsettings = get_object_from_db(MageMiscSettings)
        if not miscsettings:
            miscsettings = MageMiscSettings(marketsimulationname=get_current_marketsimulationname())

        allow_user_profitability_computation = miscsettings.settings.get('allow_user_profitability_computation', True)

        if allow_user_profitability_computation:
            yeschecked = 'checked'
            nochecked = ''
        else:
            yeschecked = ''
            nochecked = 'checked'

        misc_settings_div = ''
        misc_settings_div += '<div class="marketconfsep_hdr"> Miscellaneous Settings </div> <br>'
        misc_settings_div += '<form method="post" action="/admin/update_misc_settings" enctype="multipart/form-data" >'
        misc_settings_div += 'Allow users to download profitability report?'
        misc_settings_div += '<input type="radio" name="profitabilityenable" value="yes" %s> yes ' % yeschecked
        misc_settings_div += '<input type="radio" name="profitabilityenable" value="no" %s> no ' % nochecked
        misc_settings_div += '<input type="submit" name="update" value="update"/>'
        misc_settings_div += '</form>'

        misc_settings_div = '<div class="marketconfsep_neutral"> %s </div>' % misc_settings_div
        admin_area_divs.append(misc_settings_div)

        if not market_configuration_complete():
            mconf_complete_stg = """
Core market configuration is not complete. To complete core market configuration, upload users, master generator fleet,
daily time segments, and bid time schedule.
"""
        else:
            if mc_done:
                mconf_complete_stg = 'Core market configuration is complete, ' \
                                     'and all forecast and actual market conditions are present'
            else:
                mconf_complete_stg = 'Core market configuration is complete, ' \
                                     'but not all forecast and actual market conditions are present'

        txt = '<div> %s </div>' % mconf_complete_stg
        txt += '<br>'
        txt += '<br> \n'.join(admin_area_divs)
        txt += '<br>'
        return mage_html_wrap(pagename='Market Configuration', body=txt, ml=MessageLog.from_json(message_log_json))

    @cherrypy.expose
    def listusers(self, message_log_json=MessageLog().to_json()):
        """URL endpoint for listing users"""
        # get users
        ulist = cherrypy.request.sa_session.query(User)
        trows = []
        # get list of market simulations for each user
        msi_names_by_user = self.get_msi_names_by_user()

        for u in ulist:
            username = u.username
            credentials = u.credentials
            delete_button = self.delete_user_button_code(u.username)
            modify_button = self.modify_button_code(u.username)

            marketsimulations_stg = ' , '.join([msi_name for msi_name in msi_names_by_user.get(username, [])])

            trows.append(('<tr> <td> {username} </td> '
                          '<td> {credentials} </td>'
                          '<td> {marketsimulations_stg} </td>'
                          '<td> {delete_button} </td>'
                          '<td> {modify_button} </td> '
                          '</tr>').format(username=username,
                                          credentials=credentials,
                                          marketsimulations_stg=marketsimulations_stg,
                                          delete_button=delete_button,
                                          modify_button=modify_button)
                         )
        tablestg = ''
        tablestg += '<table class=userlist>'
        tablestg += """<tr> <th> username </th> <th> credentials </th> <th> market simulation(s) </th> 
        <th> Delete </th> <th> Modify </th> </tr>"""
        tablestg += '\n'.join(trows)
        tablestg += '</table>'
        ml = MessageLog.from_json(message_log_json)
        return mage_html_wrap(pagename='User List', body=tablestg, ml=ml)

    @cherrypy.expose
    def marketconditions_edit(self, **kwargs):

        def conditions_price_name(inft, inctype='forecast'):
            return '{ftl}_{ctype}_price'.format(ftl=gt_name_format(inft), ctype=inctype)

        def conditions_demand_name(ints, inctype='forecast'):
            return '{tsl}_{ctype}_demand'.format(tsl=gt_name_format(ints), ctype=inctype)

        def conditions_resample_pct_name(inft, category=''):
            # category should be 'fuel' or 'ts'
            # needed to avoid collision if fuel and timesegment had same name (unlikely, but ...)
            return '{ftl}_{category}_resample_pct'.format(ftl=gt_name_format(inft), category=category)

        if 'message_log_json' in kwargs:
            ml = MessageLog.from_json(kwargs['message_log_json'])
        else:
            ml = MessageLog()

        bts = get_object_from_db(BidTimeSchedule)
        mgf = get_object_from_db(MasterGeneratorFleet)
        dts = get_object_from_db(DailyTimeSegments)
        txt = ''
        if not (bts and mgf and dts):
            txt += 'Core market configuration not complete, cannot edit market conditions'
        else:
            fueltypes = sorted(list(set([u.fuel for u in mgf])))
            dtsnames = dts.names()
            if kwargs.get('update', '') == 'save':
                save_to_bidround = int(kwargs.get('bidroundsaveselect', '1'))

                mc_class_d = {'forecast': ForecastMarketConditions, 'actual': ActualMarketConditions}
                for ctype in ['forecast', 'actual']:
                    # build fmc
                    wb = Workbook()
                    sheet = wb.active
                    point = WSpoint(row=1, col=1)
                    endpt = write_list_to_sheet(sheet, origin=point, textlists=[['Fuel', 'Forecast Price', 'Units']])
                    endpt = WSpoint(row=endpt.row+1, col=1)
                    for ft in fueltypes:
                        price_name = conditions_price_name(ft, ctype)
                        price_val = kwargs.get(price_name, '')
                        endpt = write_list_to_sheet(sheet, origin=endpt, textlists=[[ft, float(price_val), '$/MMBTU']])
                        endpt = WSpoint(row=endpt.row + 1, col=1)
                    endpt = WSpoint(row=endpt.row + 1, col=1)
                    endpt = write_list_to_sheet(sheet, origin=endpt,
                                                textlists=[['Time segment', 'Forecast Demand', 'Units']])
                    endpt = WSpoint(row=endpt.row + 1, col=1)
                    for ts in dtsnames:
                        demand_name = conditions_demand_name(ts, ctype)
                        demand_val = kwargs.get(demand_name, '')
                        endpt = write_list_to_sheet(sheet, origin=endpt,
                                                    textlists=[[ts, float(demand_val), 'MWh/h']])
                        endpt = WSpoint(row=endpt.row + 1, col=1)

                    mc = mc_class_d[ctype](marketsimulationname=get_current_marketsimulationname())
                    e = mc.initialize_from_spreadsheet(sheet, bidround=save_to_bidround, daily_time_segments=dts)
                    if e:
                        ml.add_error(e)
                    else:

                        oldmc = get_object_from_db(mc_class_d[ctype], bidround=save_to_bidround)
                        if oldmc:
                            cherrypy.request.sa_session.delete(oldmc)
                        cherrypy.request.sa_session.add(mc)
                        ml.add_message('Saved %s market conditions for bidround %d' % (ctype, save_to_bidround))

            if kwargs.get('update', '') == 'load':
                load_from_bidround = int(kwargs.get('bidroundloadselect', '1'))
                fmc = get_object_from_db(ForecastMarketConditions, bidround=load_from_bidround)
                amc = get_object_from_db(ActualMarketConditions, bidround=load_from_bidround)

                mc_d = {'forecast': fmc, 'actual': amc}
                for ctype in ['forecast', 'actual']:
                    for ft in fueltypes:
                        price_name = conditions_price_name(ft, inctype=ctype)
                        if mc_d[ctype]:
                            price_val = str(mc_d[ctype].fuelprices[ft][0])
                        else:
                            price_val = ''
                        kwargs[price_name] = price_val

                    for ts in dtsnames:
                        demand_name = conditions_demand_name(ts, inctype=ctype)
                        if mc_d[ctype]:
                            demand_val = str(mc_d[ctype].demand_by_ts[ts][0])
                        else:
                            demand_val = ''
                        kwargs[demand_name] = demand_val

                if fmc:
                    ml.add_message('loaded forecast market conditions from bidround %d' % load_from_bidround)
                if amc:
                    ml.add_message('loaded actual market conditions from bidround %d' % load_from_bidround)

            num_bidrounds = len(bts.bidtimes)
            txt += '<form method="post" action="/admin/marketconditions_edit" enctype="multipart/form-data">'
            txt += 'Load from bidround : <select name="bidroundloadselect">'
            for n in range(1, num_bidrounds+1):
                if kwargs.get('bidroundloadselect', '') == str(n):
                    txt += '<option value="{br}" selected> {br} </option>'.format(br=str(n))
                else:
                    txt += '<option value="{br}"> {br} </option>'.format(br=str(n))
            txt += '</select>'
            txt += '<input type="submit" name="update" value="load"> </input>'

            txt += '<br>'
            forecast_pct_range_name = 'forecast_pct_all'
            default_resample_pct_stg = '10'
            forecast_pct_range_val = kwargs.get(forecast_pct_range_name, default_resample_pct_stg)
            txt += 'Resample actual values from forecast : '
            txt += '<input type="submit" name="update" value="resample"/>'
            txt += '<br>'
            txt += ('Set all resample percentage ranges to'
                    '<input type="text" name="{forecast_pct_range_name}" value="{forecast_pct_range_val}"/> % :'
                    '<input type="submit" name="update" value="set">'
                    ).format(forecast_pct_range_name=forecast_pct_range_name,
                             forecast_pct_range_val=forecast_pct_range_val)
            txt += '<br>'

            txt += '<table class="editmarketconditions">'
            txt += '<tr> <th> </th> <th> Forecast </th> <th></th>'
            txt += '<th> </th> <th> Actual</th> <th></th>'
            txt += '<th> Resample percentage range </th> </tr>'
            txt += '<tr> </tr>'
            for ft in fueltypes:
                forecast_price_name = conditions_price_name(ft, 'forecast')
                forecast_price_val = kwargs.get(forecast_price_name, '')
                txt += '<tr>    '
                txt += '<td> {ft} </td> '.format(ft=ft)
                txt += '<td> <input type="text"  name="{forecast_price_name}" value="{forecast_price_val}" /> ' \
                       '</td>'.format(forecast_price_name=forecast_price_name, forecast_price_val=forecast_price_val)
                txt += '<td> $/MMBTU </td>'
                actual_price_name = conditions_price_name(ft, 'actual')
                actual_price_val = kwargs.get(actual_price_name, '')

                resample_pct_name = conditions_resample_pct_name(ft, 'fuel')

                if kwargs.get('update', '') == 'resample':
                    try:

                        # round price to 2 dp
                        z = random.random() * 2 - 1
                        fpr = float(kwargs.get(resample_pct_name, 0))
                        actual_price_val = float(forecast_price_val)*(1.0+z*fpr/100.0)
                        actual_price_val = str(int(actual_price_val*100)/100)
                    except ValueError:
                        ml.add_error('Unable to resample actual price for fuel type %s' % ft)

                txt += '<td> {ft} </td> '.format(ft=ft)
                txt += '<td> <input type="text" name="{actual_price_name}" value="{actual_price_val}" " /> ' \
                       '</td> '.format(actual_price_name=actual_price_name, actual_price_val=actual_price_val)
                txt += '<td> $/MMBTU </td>'

                resample_pct_val = kwargs.get(resample_pct_name, default_resample_pct_stg)
                if kwargs.get('update','') == 'set':
                    resample_pct_val = kwargs.get('forecast_pct_all', default_resample_pct_stg)

                txt += ('<td> <input type="text" name="{resample_pct_name}" value="{resample_pct_val}" /> ' 
                        '</td>'
                        ).format(resample_pct_name=resample_pct_name, resample_pct_val=resample_pct_val)
                txt += '</tr>'

            txt += '</table> <br>'
            txt += ('Maximum total generation capacity for all generators in market is %d MW '
                    '(not considering forced outages) ' 
                    '<br>') % mgf.maximum_total_generation_capacity()
            txt += ('Average total generation capacity for all generators in market is %d '
                    '(considering forced outages) ' 
                    '<br>') % mgf.expected_total_generation_capacity()


            txt += ' <br><br><table class="editmarketconditions">'
            txt += '<tr> <th> </th> <th> Forecast </th> <th> </th>'
            txt += '<th> </th> <th>  Actual </th> <th> </th>'
            txt += '<th> Resample percentage range </th> </tr>'
            for ts in dtsnames:
                forecast_demand_name = conditions_demand_name(ts, 'forecast')
                forecast_demand_val = kwargs.get(forecast_demand_name, '')

                txt += '<tr> <td> {ts} </td> <td> '.format(ts=ts)
                txt += ('<input type="text" name="{forecast_demand_name}" value="{forecast_demand_val}"/> '
                        '</td>').format(forecast_demand_name=forecast_demand_name,
                                        forecast_demand_val=forecast_demand_val)
                txt += '<td> MW </td>'
                actual_demand_name = conditions_demand_name(ts, 'actual')
                actual_demand_val = kwargs.get(actual_demand_name, '')

                resample_pct_name = conditions_resample_pct_name(ts, 'timesegment')

                if kwargs.get('update', '') == 'resample':
                    try:

                        # round demand to integer
                        z = random.random() * 2 - 1
                        fpr = float(kwargs.get(resample_pct_name, 0))
                        actual_demand_val = float(forecast_demand_val)*(1.0+z*fpr/100)
                        actual_demand_val = str(int(actual_demand_val))
                    except ValueError:
                        ml.add_error('Unable to resample actual demand for time segment type %s' % ts)

                txt += '<td> {ts} </td> <td> '.format(ts=ts)
                txt += '<input type="text" name="{actual_demand_name}" value="{actual_demand_val}"/> ' \
                       '</td>'.format(actual_demand_name=actual_demand_name, actual_demand_val=actual_demand_val)
                txt += '<td> MW </td>'

                resample_pct_val = kwargs.get(resample_pct_name, default_resample_pct_stg)
                if kwargs.get('update','') == 'set':
                    resample_pct_val = kwargs.get('forecast_pct_all', default_resample_pct_stg)

                txt += ('<td> <input type="text" name="{resample_pct_name}" value="{resample_pct_val}" /> ' 
                        '</td>'
                        ).format(resample_pct_name=resample_pct_name, resample_pct_val=resample_pct_val)

                txt += '</tr>'

            txt += '</table>'
            txt += '<br>'
            txt += 'Save as bidround : <select name="bidroundsaveselect">'
            for n in range(1, num_bidrounds+1):
                if kwargs.get('bidroundsaveselect', '') == str(n):
                    txt += '<option value="{br}" selected> {br} </option>'.format(br=str(n))
                else:
                    txt += '<option value="{br}"> {br} </option>'.format(br=str(n))
            txt += '</select>'
            txt += '<input type="submit" name="update" value="save"/>'
            txt += '</form>'

        return mage_html_wrap(pagename='Market conditions edit tool', body=txt, ml=ml)

    @cherrypy.expose
    def marketgen_generate(self, profileset, usernameset, message_log_json=MessageLog().to_json()):
        sa_session = cherrypy.request.sa_session
        csim = get_current_marketsimulationname()
        marketgen_mgflist = sa_session.query(MarketgenMasterGeneratorFleet).filter_by(marketsimulationname=csim)
        marketgen_mgflist = [o for o in marketgen_mgflist]

        # determine if master generator fleet is loaded
        mgf = get_object_from_db(MasterGeneratorFleet)
        if mgf:
            mgf_div = """
        Master generator fleet description is loaded.
        {delete_mgf_button}
        <form method="post" action="/admin/view_mgf" style="display:inline" >
        <input type="submit" value = "view"/></form>
        <form method="get" action="/admin/download_mgf" style="display:inline">
        <input type="submit" value="download"/></form>
        """.format(delete_mgf_button=self.delete_mgf_button_code(parent_url='/admin/marketgen_generate',
                                                                 hidden_params={'profileset': profileset,
                                                                                'usernameset': usernameset}))
        else:
            mgf_div = """
        Master generator fleet description is not loaded.
        <form method="post" action="/admin/upload_mgf" style="display:inline"  enctype="multipart/form-data">
        <input type="file" name = "mgf_spreadsheet_file" />
        <input type="submit" value = "upload"/>"""
            mgf_div += '<input type="hidden" name="%s" value="%s"/>' % ('profileset', profileset)
            mgf_div += '<input type="hidden" name="%s" value="%s"/>' % ('usernameset', usernameset)
            mgf_div += '<input type="hidden" name="%s" value="%s"/>' % ('parent_url', '/admin/marketgen_generate')
            mgf_div += '</form>'
            mgf_div += '</form>'

        pslist = [o for o in cherrypy.request.sa_session.query(UnitProfileSet).filter_by(profilesetname=profileset)]
        if not pslist:
            raise MageException('UnitProfileSet with profilesetname %s not in database' % profileset)
        ups = pslist[0]

        txt = ''
        txt += 'Current simulation : '
        txt += mgf_div
        txt += '<form method="post" action="/admin/marketgen_generate_formhandler">'
        txt += "Available Master Generator Fleets : "
        txt += '<select name = "marketgen_mgf" style="width: 150px;">'
        for mg_mgf in marketgen_mgflist:
            txt += '<option value = "%s"> %s </option> ' % (mg_mgf.mastergeneratorfleetname,
                                                            mg_mgf.mastergeneratorfleetname)
        txt += '</select>'
        txt += '<input type="submit" name="mgfselect" value="View" />'
        txt += '<input type="submit" name="mgfselect" value="Delete" />'
        txt += '<input type="submit" name="mgfselect" value="Download" />'
        txt += '<input type="submit" name="mgfselect" value="Select for current simulation"/>'

        txt += '<br> <br> Using generator types from profileset <span class=hdr_name> %s</span>' % profileset
        txt += ', and users from usernameset <span class=hdr_name> %s </span> <br><br>' % usernameset
        txt += 'Specify fraction of total generation capacity for each generator type, and MW per user <br><br>'

        txt += '<input type="hidden" name="profileset" value="%s"/>' % profileset
        txt += '<input type="hidden" name="usernameset" value="%s"/>' % usernameset
        txt += '<table>'
        generatortypes = sorted(ups.gug_d.keys())
        for gt in generatortypes:
            txt += ('<tr><td> %s percentage </td> <td> '
                    '<input type="text" name="%s" /> </td></tr>\n') % (gt, gt_name_format(gt)+'_pct')

        txt += '<tr><td> Megawatts per user : </td> <td> <input type="text" name="mw_per_user" /> </td> </tr>\n'
        txt += '</table>'
        txt += '<br> Create new Master Generator Fleet <input type="submit" name="createmgf" value="generate"/>'

        txt += '</form>'
        return mage_html_wrap(body=txt, pagename='Master Generator Fleet Description Creation Tool (page 2 of 2)',
                              ml=MessageLog.from_json(message_log_json))

    @cherrypy.expose
    def marketgen_generate_formhandler(self, usernameset, profileset, mw_per_user,
                                       createmgf=None, mgfselect=None, marketgen_mgf=None, **kwargs):
        ml = MessageLog()
        parent_url = '/admin/marketgen_generate'
        sa_session = cherrypy.request.sa_session
        marketsimulationname = get_current_marketsimulationname()

        try:
            ups = [o for o in sa_session.query(UnitProfileSet).filter_by(profilesetname=profileset)][0]
            uns = [o for o in sa_session.query(UsernameSet).filter_by(usernamesetname=usernameset)][0]
        except IndexError:
            raise MageException('Could not load UnitProfileSet or UsernameSet')

        if createmgf:
            thresh = 1.0e-5
            capacity_fraction_d = {}
            cf_valid = True
            for gt in ups.gug_d:
                try:
                    capacity_fraction_d[gt] = float(kwargs[gt_name_format(gt)+'_pct'])/100
                except ValueError:
                    ml.add_error('Invalid entry for %s percentage' % gt)
                    cf_valid = False
            if cf_valid:
                cf = [capacity_fraction_d[gt] for gt in ups.gug_d]
                if any([x < 0.0 for x in cf]) or abs(sum([capacity_fraction_d[gt] for gt in ups.gug_d]) - 1.0) > thresh:
                    ml.add_error('Given capacities must be positive and sum to 100, please try again')
                    cf_valid = False
            try:
                f_mw_per_user = float(mw_per_user)
            except ValueError:
                ml.add_error('Invalid value %s for Megawatts per user' % mw_per_user)
                cf_valid = False
                f_mw_per_user = -1  # invalid value
            if cf_valid:
                mgf = generate_mgf(ups.gug_d, uns.usernames, capacity_fraction_d, f_mw_per_user)
                mastergeneratorfleetname = '%s_%s_mgf' % (profileset, usernameset)
                my_marketgen_mgf = MarketgenMasterGeneratorFleet(mgf=mgf, marketsimulationname=marketsimulationname,
                                                                 mastergeneratorfleetname=mastergeneratorfleetname)
                # add to db if name doesn't already exist
                tmp = sa_session.query(MarketgenMasterGeneratorFleet)
                marketgen_mgflist = tmp.filter_by(marketsimulationname=marketsimulationname,
                                                  mastergeneratorfleetname=mastergeneratorfleetname)
                marketgen_mgflist = [o for o in marketgen_mgflist]
                if marketgen_mgflist:
                    ml.add_error(('master generator fleet with name %s already exists, '
                                  'not overwriting') % mastergeneratorfleetname)
                else:
                    sa_session.add(my_marketgen_mgf)
                    sa_session.commit()
                    ml.add_message('Created new master generator fleet with name %s' % mastergeneratorfleetname)

        if mgfselect:
            tmp = sa_session.query(MarketgenMasterGeneratorFleet)
            mgflist = [o for o in tmp.filter_by(marketsimulationname=marketsimulationname,
                                                mastergeneratorfleetname=marketgen_mgf)]
            if mgflist:
                mgf = mgflist[0]
            else:
                mgf = None

            if mgfselect == 'View':
                txt = '<div> Master Generator Fleet </div>' + '<div> %s </div>' % mgf.mgf.html_repr()
                return mage_html_wrap(pagename='View Master Generator Fleet', body=txt)
            elif mgfselect == 'Delete':
                if not mgf:
                    ml.add_error('No selected master generator fleet to delete')
                else:
                    sa_session.delete(mgf)
                    sa_session.commit()
                    ml.add_message('Deleted master generator fleet with name %s' % marketgen_mgf)

            elif mgfselect == 'Download':
                if not mgf:
                    ml.add_error('No selected master generator fleet to download')
                else:
                    buff = io.BytesIO()
                    wb = mgf.mgf.spreadsheet_repr()
                    wb.save(buff)
                    buff.seek(0)
                    filename = '%s.xlsx' % marketgen_mgf
                    return cherrypy.lib.static.serve_fileobj(buff, 'application/x-download', 'attachment', filename)

            elif mgfselect == 'Select for current simulation':
                cs_mgf = get_object_from_db(MasterGeneratorFleet)
                if cs_mgf:
                    ml.add_error(('A master generator fleet is already loaded into the current simulation, '
                                  'will not overwrite. Delete it first.'))
                else:
                    cs_mgf = mgf.mgf
                    cs_mgf.marketsimulationname = marketsimulationname
                    sa_session.add(cs_mgf)
                    sa_session.commit()
                    ml.add_message("Loaded master generator fleet with name %s into current simulation" % marketgen_mgf)
            else:
                raise MageException('Unexpected value of mgfselect')
        qs = dict_to_post_args({'usernameset': usernameset, 'profileset': profileset, 'message_log_json': ml.to_json()})
        raise cherrypy.InternalRedirect(parent_url, query_string=qs)

    @cherrypy.expose
    def marketgen_profile(self, message_log_json=MessageLog().to_json()):
        txt = ''
        # load profile sets from db
        upslist = [u for u in cherrypy.request.sa_session.query(UnitProfileSet)]
        # upslist = get_object_from_db(UnitProfileSet)
        # upslist = [u for u in upslist]

        # load username sets from db
        usernamesetlist = get_object_from_db(UsernameSet)
        usernamesetlist = [u for u in usernamesetlist]
        txt += '<form method="post" action="/admin/marketgen_profile_formhandler" enctype="multipart/form-data">'
        txt += '<div> <table> <tr> <td> Profile set '
        txt += '<select name = "profileset" style="width: 150px;">'
        for u in upslist:
            txt += '<option value = "%s"> %s </option> ' % (u.profilesetname, u.profilesetname)
        txt += '</select> </td>'
        txt += '<td> <input type="submit" name="profileset_action" value="Delete"/></td>'
        txt += '<td> <input type="submit" name="profileset_action" value="View"/></td>'
        txt += '<td> <input type="submit" name="profileset_action" value="Download"/></td></tr>'
        txt += '<tr> <td> </td>'
        txt += '<td> <input type="submit" name="profileset_action" value="Upload"/> </td>'
        txt += '<td> <input type="file" name="newprofilesetfile"/> </td>'
        txt += ' <td> with name  <input type="text" name="newprofilesetname"/> </td> </tr> </table> </div>'
        txt += '<br/>'
        txt += '<div> Username set '
        txt += '<select name = "usernameset" style="width: 150px;">'
        for u in usernamesetlist:
            txt += '<option value = "%s"> %s </option> ' % (u.usernamesetname, u.usernamesetname)
        txt += '</select>'
        txt += '<input type="submit" name="usernameset_action" value="Delete"/>'
        txt += '<input type="submit" name="usernameset_action" value="View"/>'
        txt += '<input type="submit" name="usernameset_action" value="Import from simulation"/>'
        txt += '</div>'
        txt += '<br/>'
        txt += '<div>'
        txt += 'Use selected profile set and username set for master generator fleet generation '
        txt += '<input type="submit" name="submit" value="go" />'
        txt += '</div>'
        txt += '</form>'

        return mage_html_wrap(pagename='Master Generator Fleet Description Creation Tool (page 1 of 2)', body=txt,
                              ml=MessageLog.from_json(message_log_json))

    @cherrypy.expose
    def marketgen_profile_formhandler(self, profileset=None, profileset_action=None, newprofilesetname=None,
                                      usernameset='', usernameset_action=None, submit=None, newprofilesetfile=None):
        ml = MessageLog()
        parent_url = '/admin/marketgen_profile'
        sa_session = cherrypy.request.sa_session
        try:
            ups = [o for o in sa_session.query(UnitProfileSet).filter_by(profilesetname=profileset)][0]
        except IndexError:
            ups = None
        txt = ''
        if profileset_action:
            if profileset_action == 'Delete':
                if ups:
                    if profileset == 'default':
                        ml.add_error('Cannot delete default profile set, not deleting')
                    else:
                        sa_session.delete(ups)
                        sa_session.commit()
                        ml.add_message('Deleted profile set with name %s' % profileset)
                else:
                    ml.add_error('Profile set with name %s not found, cannot delete' % profileset)

            elif profileset_action == 'View':
                if ups:
                    wb = ups.spreadsheet_repr()
                    profileset_txt = ('Generator unit profile set named '
                                      '<span class=hdr_name> %s </span> <br><br>') % profileset
                    profileset_txt += '<br><br>'.join([excel_worksheet_to_html_table(sheet, tableclass='mgf')
                                                       for sheet in wb])
                    ml.add_message(profileset_txt)
                    raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json=' + ml.to_json())
                else:
                    ml.add_error('Profile set with name %s not found, cannot view' % profileset)
            elif profileset_action == 'Download':
                if ups:
                    buff = io.BytesIO()
                    wb = ups.spreadsheet_repr()
                    wb.save(buff)
                    buff.seek(0)
                    filename = profileset+'_ups.xlsx'
                    return cherrypy.lib.static.serve_fileobj(buff, 'application/x-download', 'attachment', filename)
                else:
                    ml.add_error('Profile set with name %s not found, cannot download' % profileset)

            elif profileset_action == 'Upload':
                if newprofilesetname:
                    ups = UnitProfileSet(profilesetname=newprofilesetname)

                    upslist = [o for o in sa_session.query(UnitProfileSet).filter_by(profilesetname=newprofilesetname)]
                    if upslist:
                        ml.add_error(('Database already contains profileset with name %s, '
                                      'not overwriting') % newprofilesetname)
                    else:
                        if newprofilesetfile.file:
                            wb = load_workbook(newprofilesetfile.file)
                            #pdb.set_trace()
                            e = ups.initialize_from_workbook(wb)
                            if e:
                                for err in e:
                                    ml.add_error(err)
                            else:
                                sa_session.add(ups)
                                ml.add_message('Uploaded profileset with name %s' % newprofilesetname)
                        else:
                            ml.add_error('No file specified, cannot upload')
                else:
                    ml.add_error('No profile set name given, cannot upload')

            else:
                raise MageException('Unexpected  value %s for profileset_action' % profileset_action)

            raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json=' + ml.to_json())

        if usernameset_action:
            if usernameset_action == 'Delete':
                if usernameset:
                    uns = get_object_from_db(UsernameSet, usernamesetname=usernameset)
                    if uns:
                        sa_session.delete(uns[0])
                        ml.add_message('Deleted usernameset %s' % usernameset)
                    else:
                        ml.add_error('Did not find usernameset %s in database, not deleting' % usernameset)
                else:
                    ml.add_error('No specified usernameset to delete')

            elif usernameset_action == 'View':
                if usernameset:
                    uns = get_object_from_db(UsernameSet, usernamesetname=usernameset)
                    if uns:
                        txt = 'Users contained in usernameset %s <br> <br>' % usernameset
                        for name in uns[0].usernames:
                            txt += '%s <br>' % name
                        ml.add_message(txt)
                    else:
                        ml.add_error('no usernameset %s in database' % usernameset)
                else:
                    ml.add_error('no usernameset specified, nothing to view')

            elif usernameset_action == 'Import from simulation':
                # get usernames from current simulation
                # create appropriate usernamesetname
                # add it to db
                currentsimulation = get_current_marketsimulationname()
                msi = MarketSimulationInstance.get_from_db(sa_session, name=currentsimulation)
                if msi:
                    usernames = sorted([u.username for u in msi.users])
                    if usernames:
                        usernamesetname = currentsimulation+'_users'
                        uns = UsernameSet(usernamesetname=usernamesetname, marketsimulationname=currentsimulation)
                        uns.usernames = usernames
                        # add usernameset to database
                        if get_object_from_db(UsernameSet, usernamesetname=usernamesetname):
                            ml.add_error(('usernameset with name %s already in database, '
                                          'will not overwrite') % usernamesetname)
                        else:
                            sa_session.add(uns)
                            sa_session.commit()
                            ml.add_message('Added usernameset %s to database' % usernamesetname)

                    else:
                        ml.add_error('No users in current simulation. Add them at the Admin Market Configuration page')
                else:
                    raise MageException('No market simulation instance found')

            elif usernameset_action == 'Upload':
                ml.add_error('Upload not currently implemented')
            else:
                raise MageException('Unexpected value %s for usernameset_action' % usernameset_action)

            raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json=' + ml.to_json())

        if submit:
            qs = dict_to_post_args({'profileset': profileset, 'usernameset': usernameset})
            raise cherrypy.InternalRedirect('/admin/marketgen_generate', query_string=qs)
        return mage_html_wrap(body=txt)


    @cherrypy.expose
    def render_supply_curve(self,bidround, timesegment,parent_url = '/admin/view_market_console/'):
        """ returns png image of supply curve, for specified bidround and timesegment
        """
        def effective_bidprice(bidprice,forced_on):
            if not forced_on:
                return bidprice
            else:
                return 0.0

        bidround = int(bidround)
        omr = get_object_from_db(OverallMarketReport,bidround=bidround)
        mgf = get_object_from_db(MasterGeneratorFleet)

        if omr and mgf:
            # create list of effective bidprice vs cumulative bid capacity
            market_unit_segment_list = omr.market_unit_segment_list_by_ts[timesegment]
            effective_bidprice_list = [effective_bidprice(mus.bidprice,mus.forced_on) for
                                       mus in market_unit_segment_list]
            cumulative_effective_capacity = cumsum([mus.effective_capacity() for
                                             mus in market_unit_segment_list])

            price = np.array(effective_bidprice_list)
            supply = np.array(cumulative_effective_capacity)
            plt.clf()
            price, supply = stairstep_interpolate(price, supply)
            plt.plot(price, supply)
            plt.xlabel('price ($)')
            plt.ylabel('supply (MW)')
            #  set zoom ...
            mcp = omr.clearing_price_by_ts[timesegment]
            demand = omr.actual_market_conditions.demand_by_ts[timesegment][0]

            # have to be careful if magic expensive energy was ever dispatched
            num_idx_valid = sum(supply < MAGIC_EXPENSIVE_ENERGY_CAPACITY)
            axislimit_pricemax = max(price[num_idx_valid - 1], mcp) * 1.2
            axislimit_supplymax = max(supply[num_idx_valid-1], demand) * 1.2
            plt.xlim([0, axislimit_pricemax])
            plt.ylim([0, axislimit_supplymax])

            plt.title('Supply curve for bidround %d, time segment "%s" ' % (bidround, timesegment) +
                      '\n' +
                      ' (demand %s MWh/h, mcp $%2.2f/MWh) ' % (demand, mcp))
            y = omr.actual_market_conditions.demand_by_ts[timesegment][0]
            plt.plot(plt.xlim(), [y, y])
            plt.legend(('supply', 'demand'))
            buff = io.BytesIO()
            plt.savefig(buff, format='png')
            buff.seek(0)
            cherrypy.response.headers['Content-Type'] = "image/png"
            return file_generator(buff)
        else:
            pdb.set_trace()
            return None

    @cherrypy.expose
    def remove_user_from_ms(self, username, msi_name, parent_url='/admin/listusers/'):
        ml = MessageLog()
        msi_list = list(cherrypy.request.sa_session.query(MarketSimulationInstance).filter_by(name=msi_name))
        ulist = get_matching_userlist(username)
        if not ulist:
            ml.add_error('User %s not present' % username)
        else:
            if len(msi_list) == 0:
                ml.add_error('Market simulation %s not present' % msi_name)
            elif len(msi_list) == 1:
                user = ulist[0]
                msi = msi_list[0]
                if user in msi.users:
                    msi.users.remove(user)
                    ml.add_message('User %s removed from market simulation %s' % (username, msi_name))
                    # recursively remove all of users bids ...
                    bid_list = cherrypy.request.sa_session.query(Bid).filter_by(username=username,
                                                                                marketsimulationname=msi_name)
                    for bid in bid_list:
                        cherrypy.request.sa_session.delete(bid)
                        ml.add_message('Deleted bid for user %s,market simulation %s,bidround %s'
                                       % (username, bid.marketsimulationname, bid.bidround))
                    cherrypy.request.sa_session.commit()
                else:
                    ml.add_error('User %s not in market simulation %s' % (username, msi_name))
            else:
                raise MageException('more than one msi found with name %s' % msi_name)
        raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json='+ml.to_json())

    @cherrypy.expose
    def run_market_model(self, bidround, parent_url='/admin/view_market_console/'):
        """url endpoint for running market model. Bids should be loaded when this is run"""
        ml = self._run_market_model(int(bidround))
        raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json='+ml.to_json())

    @cherrypy.expose
    def select_dts(self, parent_url='/admin/', dts_name=None):
        ml = MessageLog()
        if dts_name:
            tmp = cherrypy.request.sa_session.query(DefaultDailyTimeSegments).filter_by(dailytimesegmentsname=dts_name)
            dtslist = [o for o in tmp]
            if dtslist:
                newdts = dtslist[0].dts
                newdts.marketsimulationname = get_current_marketsimulationname()
                if not get_object_from_db(DailyTimeSegments):
                    cherrypy.request.sa_session.add(newdts)
                    cherrypy.request.sa_session.commit()
                    ml.add_message('Added daily time segments named %s to current simulation' % dts_name)
                else:
                    ml.add_error('Daily time segments already selected for current simulation, not overwriting')
            else:
                ml.add_error('No defaut daily time segments with name %s found' % dts_name)
        else:
            ml.add_error('No default daily time segments specified')
        raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json=' + ml.to_json())

    @cherrypy.expose
    def update_misc_settings(self, parent_url='/admin/', **kwargs):
        ml = MessageLog()
        miscsettings = MageMiscSettings(marketsimulationname=get_current_marketsimulationname())

        if kwargs['profitabilityenable'] == 'yes':
            miscsettings.settings['allow_user_profitability_computation'] = True
        if kwargs['profitabilityenable'] == 'no':
            miscsettings.settings['allow_user_profitability_computation'] = False
        oldmiscsettings = get_object_from_db(MageMiscSettings)
        if oldmiscsettings:
            cherrypy.request.sa_session.delete(oldmiscsettings)
        cherrypy.request.sa_session.add(miscsettings)
        ml.add_message('Updated settings')
        raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json=' + ml.to_json())

    @cherrypy.expose
    def upload_amc(self, spreadsheet_file, bidround, parent_url='/admin/view_market_console/'):
        """url endpoint for uploading actual market conditions"""
        ml = MessageLog()
        if not hasattr(spreadsheet_file, 'file'):
            ml.add_error('Cannot read file')
        else:
            dtseg = get_object_from_db(DailyTimeSegments)
            if not dtseg:
                ml.add_error('Cannot accept actual market conditions as daily time segments not yet loaded')
            else:
                amc = ActualMarketConditions(marketsimulationname=get_current_marketsimulationname())
                ml.add_error(amc.initialize_from_spreadsheet_file(spreadsheet_file.file, bidround=bidround,
                                                                  daily_time_segments=dtseg))
                if not ml.e:
                    ml.add_message(amc.add_to_db(cherrypy.request.sa_session))
                else:
                    ml.add_error('Could not load %s' % 'actual market conditions')
        raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json='+ml.to_json())

    @cherrypy.expose
    def upload_bts(self, bts_spreadsheet_file=None, parent_url='/admin/'):
        if not bts_spreadsheet_file:
            return
        bts = BidTimeSchedule(marketsimulationname=get_current_marketsimulationname())
        e = bts.initialize_from_spreadsheet_file(bts_spreadsheet_file.file)
        ml = MessageLog(e=e)
        if not ml.e:
            ml += self._addbts(bts)
        else:
            ml.add_error('Could not successfuly load bid time schedule')
        raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json='+ml.to_json())

    @cherrypy.expose
    def upload_dtseg(self, dtseg_spreadsheet_file=None, parent_url='/admin/'):
        if not dtseg_spreadsheet_file:
            return
        dtseg = DailyTimeSegments(marketsimulationname=get_current_marketsimulationname())
        e = dtseg.initialize_from_spreadsheet_file(dtseg_spreadsheet_file.file)
        ml = MessageLog(e=e)
        if not ml.e:
            ml += self._add_dtseg(dtseg)
        else:
            ml.add_error('Could not successfuly load daily time segments')

        raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json='+ml.to_json())

    # @cherrypy.expose
    # def upload_mcp(self,mcp_spreadsheet_file, parent_url='/admin/'):
    #     ml = MessageLog()
    #     if not hasattr(mcp_spreadsheet_file, 'file'):
    #         return
    #
    #     # attempt to parse spreadsheet file
    #     wb = load_workbook(mcp_spreadsheet_file.file)
    #     ml = MessageLog()
    #     d = load_mcp_from_spreadsheet_file(mcp_spreadsheet_file.file, ml,
    #                                        marketsimulationname=get_current_marketsimulationname())
    #
    #     if ml.e:
    #         raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json=' + ml.to_json())
    #     else:
    #         # usernames, passwords
    #         for username, password in zip(d['usernames'], d['passwords']):
    #             ml += self._adduser(username, 'user', password)
    #         # mgf
    #         ml += self._addmgf(d['mgf'])
    #         # bts
    #         ml += self._addbts(d['bts'])
    #         # dtseg
    #         ml += self._add_dtseg(d['dtseg'])
    #         # fmc
    #         for fmc in d['fmc_list']:
    #             if fmc:
    #                 ml.add_message(fmc.add_to_db(cherrypy.request.sa_session))
    #         # amc
    #         for amc in d['amc_list']:
    #             if amc:
    #                 ml.add_message(amc.add_to_db(cherrypy.request.sa_session))
    #
    #     raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json=' + ml.to_json())

    @cherrypy.expose
    def upload_fmc(self, spreadsheet_file, bidround, parent_url='/admin/view_market_console/'):
        ml = MessageLog()
        if not hasattr(spreadsheet_file, 'file'):
            return

        dtseg = get_object_from_db(DailyTimeSegments)
        if not dtseg:
            ml.add_error('Cannot accept forecast market conditions as daily time segments not yet loaded')
        else:
            fmc = ForecastMarketConditions(marketsimulationname=get_current_marketsimulationname())
            ml.add_error(fmc.initialize_from_spreadsheet_file(spreadsheet_file.file,
                                                              bidround=bidround,
                                                              daily_time_segments=dtseg))
            if not ml.e:
                ml.add_message(fmc.add_to_db(cherrypy.request.sa_session))
            else:
                ml.add_error('Could not load %s' % 'forecast market conditions')

        raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json='+ml.to_json())

    @cherrypy.expose
    def upload_mgf(self, mgf_spreadsheet_file=None, parent_url='/admin/', **kwargs):
        if not hasattr(mgf_spreadsheet_file, 'file'):
            return
        mgf = MasterGeneratorFleet(marketsimulationname=get_current_marketsimulationname())
        e = mgf.initialize_from_spreadsheet_file(mgf_spreadsheet_file.file)
        ml = MessageLog(e=e)
        if not ml.e:
            ml += self._addmgf(mgf)
        else:
            ml.add_error('Could not successfuly load master generator fleet')
        qs = dict_to_post_args({**{'message_log_json': ml.to_json()}, **kwargs})
        raise cherrypy.InternalRedirect(parent_url, query_string=qs)

    @cherrypy.expose
    def upload_username_password_file(self, username_password_file=None, parent_url='/admin/'):
        (usernames, passwords, e) = parse_username_password_spreadsheet(username_password_file.file)
        ml = MessageLog(e=e)
        for username, password in zip(usernames, passwords):
            ml += self._adduser(username, 'user', password)
        raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json='+ml.to_json())

    @cherrypy.expose
    def view_all_bids(self, bidround, message_log_json=MessageLog().to_json()):
        bidround = int(bidround)
        txt = '<div> All bids for bidround %s </div>' % bidround

        txt += '<div> %s </div>' % self.all_bids_table(bidround)

        return mage_html_wrap(pagename='View All Bids', body=txt, ml=MessageLog.from_json(message_log_json))

    @cherrypy.expose
    def view_market_console(self, message_log_json=MessageLog().to_json()):
        txt = ''
        if market_configuration_complete():

            txt += self.market_console_table()

            txt += '<br> <div> View or download cumulative profitability report: '
            txt += '<form method="post" action="/admin/create_cumulative_profitability_report" '
            txt += 'style="display:inline" enctype="multipart/form-data">'
            txt += '<input type="submit" name="update" value="view"/>'
            txt += '<input type="submit" name="update" value="download"/>'
            txt += '</form> </div>'
            txt += '<br>'
            txt += self.visualize_supply_curve_options_div()

        else:
            txt += '<div> Electricity Market is not configured </div>'

        return mage_html_wrap(pagename='Admin Market Console', body=txt, ml=MessageLog.from_json(message_log_json))


    @cherrypy.expose
    def view_mgf(self):
        """url endpoint for viewing master generator fleet"""
        mgf = get_object_from_db(MasterGeneratorFleet)
        if mgf:
            txt = '<div> Master Generator Fleet </div>' + '<div> %s </div>' % mgf.html_repr()
        else:
            txt = '<div> %s </div>' % 'Master generator fleet not loaded'
        return mage_html_wrap(pagename='View Master Generator Fleet', body=txt)

    @cherrypy.expose
    def view_omr(self, bidround):
        omr = get_object_from_db(OverallMarketReport, bidround=bidround)
        mgf = get_object_from_db(MasterGeneratorFleet)

        if omr and mgf:
            txt = '<div> Overall market report for bidround %d </div>' % int(
                bidround) + '<div> %s </div>' % omr.html_repr(
                mgf)
        else:
            txt = '<div> Overall market report not loaded </div>'
        return mage_html_wrap(pagename='View Overall Market Report', body=txt)



    @cherrypy.expose
    def visualize_supply_curve(self, bidround, timesegment):


        txt = '<img src="/admin/render_supply_curve?bidround={bidround}&timesegment={timesegment}">'.format(
                timesegment=timesegment,
                bidround=bidround)

        txt += self.visualize_supply_curve_options_div(bidround, timesegment)

        return mage_html_wrap(body=txt)





    ########################
    # non-exposed regular methods
    ########################

    def all_bids_table(self, bidround):
        # get all regular users
        msi_name = get_current_marketsimulationname()
        msi_list = list(cherrypy.request.sa_session.query(MarketSimulationInstance).filter_by(name=msi_name))
        ulist = msi_list[0].users
        allbids = [b for b in cherrypy.request.sa_session.query(Bid).filter_by(bidround=bidround,
                                                                               marketsimulationname=msi_name)]
        bids_by_user = {bid.username: bid for bid in allbids}
        txt = '<table class="all_bids">'
        txt += '<tr> <th> User </th> <th> Bid </th> </tr>'
        ulist.sort(key=lambda us: us.username)
        for u in ulist:
            txt += '<tr>'
            txt += '<td> %s </td>' % u.username
            if u.username in bids_by_user:
                txt += '<td> %s </td>' % ('Bid present.' +
                                          self.admin_view_bid_button_code(u.username, bidround) +
                                          self.admin_delete_bid_button_code(u.username, bidround))
            else:
                txt += '<td> %s </td>' % 'Bid not present.'
            txt += '</tr>'
        txt += '</table>'
        return txt

    def market_console_table(self):
        bts = get_object_from_db(BidTimeSchedule)
        if not bts:  # convert from None to [] to allow subsequent dictionary comprehension if there is no bts
            bts = []

        fmc_by_round = {r: get_object_from_db(ForecastMarketConditions, bidround=r) for (r, _, _) in bts}
        amc_by_round = {r: get_object_from_db(ActualMarketConditions, bidround=r) for (r, _, _) in bts}
        omr_by_round = {r: get_object_from_db(OverallMarketReport, bidround=r) for (r, _, _) in bts}

        txt = '<table class="market_console">'
        # header row
        txt += '<tr>'
        txt += ''.join(['<th> %s </th>' % x for x in
                        ['Round', 'Bids Open', 'Bids Closed', 'View All Bids', 'Forecast Market Conditions',
                         'Actual Market Conditions', 'Overall Market Report', 'Profitability Report']])
        txt += '</tr>'
        # table rows
        for (r, o, c) in bts:
            txt += '<tr>'
            txt += '<td> %d </td>' % r

            # bids open
            txt += '<td> %s </td>' % time_format(o)
            # bids closed
            txt += '<td> %s </td>' % time_format(c)

            txt += '<td> %s </td>' % self.view_all_bids_button(bidround=r)

            if fmc_by_round[r]:
                txt += '<td> %s </td>' % ('Forecast market conditions present.' +
                                          self.view_fmc_button_code(bidround=r) +
                                          self.delete_fmc_button_code(bidround=r))
            else:
                txt += '<td> %s </td>' % ('Forecast market conditions not present.' +
                                          self.upload_fmc_button_code(bidround=r))

            if amc_by_round[r]:
                txt += '<td> %s </td>' % ('Actual market conditions present.' +
                                          self.view_amc_button_code(bidround=r) +
                                          self.delete_amc_button_code(bidround=r))
            else:
                txt += '<td> %s </td>' % ('Actual market conditions not present.' +
                                          self.upload_amc_button_code(bidround=r))
            if omr_by_round[r]:
                txt += '<td> %s </td>' % ('Overall market report present.' +
                                          self.view_omr_button_code(bidround=r) +
                                          self.delete_omr_button_code(bidround=r) +
                                          self.download_omr_button_code(bidround=r))
            else:
                txt += '<td> %s </td>' % ('Overall market report not present. Run model?' +
                                          self.run_model_button_code(bidround=r))

            txt += '<td> profitability report: '
            txt += '<form method="post" action="/admin/create_profitability_report" enctype="multipart/form-data">'
            txt += '<input type="hidden" name="bidround" value="%d" />' % r
            txt += '<input type="submit" name="update" value="view"/>'
            txt += '<input type="submit" name="update" value="download"/>'
            txt += '</form> </td>'
            txt += '</tr>'
        txt += '</table>'
        return txt

    def _changecredentials(self, username, credentials):
        """Actually change user credentials to database.
        Returns text explaining if credentials changed or not and if not, why"""
        ml = MessageLog()
        if credentials not in allowed_credentials:
            ml.add_error('%s is not a valid credential' % credentials)
            return ml
        ulist = get_matching_userlist(username)
        if len(ulist) == 1:
            if self.is_last_admin(ulist[0]):
                ml.add_error('%s is last admin, not changing credentials' % username)
            elif ulist[0].credentials != credentials:
                ulist[0].credentials = credentials
                cherrypy.request.sa_session.commit()
                ml.add_message('credentials for %s updated to %s' % (username, credentials))
            else:
                ml.add_error('credentials for %s were already %s, no change made' % (username, credentials))
        if len(ulist) == 0:
            ml.add_error('%s is not a user, no change possible' % username)
        return ml

    def _deleteuser(self, username):
        """Actually delete user from database.
        Returns MessageLog indicating if user deleted or not and if not, why """
        ml = MessageLog()
        ulist = get_matching_userlist(username)
        if self.is_last_admin(ulist[0]):
            ml.add_error('%s is last admin, not deleted' % username)
            return ml
        if len(ulist) == 1:
            cherrypy.request.sa_session.delete(ulist[0])
            ml.add_message('%s has been deleted' % username)
            # delete all bids for user as well
            bid_list = cherrypy.request.sa_session.query(Bid).filter_by(username=username)
            for bid in bid_list:
                cherrypy.request.sa_session.delete(bid)
                ml.add_message('Deleted bid for user %s, market simulation %s,bidround %s'
                               % (username, bid.marketsimulationname, bid.bidround))
            cherrypy.request.sa_session.commit()
        if len(ulist) == 0:
            cherrypy.log('unexpectedly empty user list')
            ml.add_error('nobody to delete')
        return ml

    ########################
    # static methods
    ########################

    @staticmethod
    def admin_delete_bid_button_code(username, bidround, confirm=False):
        return form_button('post', '/admin/admin_delete_bid', 'delete', username=username, bidround=bidround,
                           confirm=confirm)

    @staticmethod
    def admin_view_bid_button_code(username, bidround):
        return form_button('get', '/admin/admin_view_bid', 'view', username=username, bidround=bidround)

    @staticmethod
    def bts_help_button_code():
        return form_button('get', '/admin/bts_help', 'help')

    @staticmethod
    def delete_amc_button_code(bidround, confirm=False):
        return form_button('post', '/admin/delete_amc', 'delete', bidround=bidround, confirm=confirm)

    @staticmethod
    def delete_bts_button_code(confirm=False):
        return form_button('post', '/admin/delete_bts', 'delete', confirm=confirm)

    @staticmethod
    def delete_dtseg_button_code(confirm=False):
        return form_button('post', '/admin/delete_dtseg', 'delete', confirm=confirm)

    @staticmethod
    def delete_fmc_button_code(bidround, confirm=False):
        return form_button('post', '/admin/delete_fmc', 'delete', bidround=bidround, confirm=confirm)

    @staticmethod
    def delete_mgf_button_code(confirm=False, parent_url='/admin', hidden_params=None):
        tmpstg = """<form method="post" action="/admin/delete_mgf" style="display:inline">
        <input type="hidden" name="parent_url" value="{parent_url}">
        <input type="submit" value="delete">""".format(parent_url=parent_url)
        if confirm:
            tmpstg += '\n <input type="hidden" name="confirm" value="true">'
        if hidden_params:
            for name in hidden_params:
                tmpstg += '<input type = "hidden" name = "%s" value = "%s">' % (name, hidden_params[name])
        tmpstg += '</form>'
        return tmpstg

    @staticmethod
    def delete_omr_button_code(bidround, confirm=False):
        return form_button('post', '/admin/delete_omr', 'delete', bidround=bidround, confirm=confirm)

    @staticmethod
    def delete_user_button_code(username, confirm=False):
        return form_button('post', '/admin/deleteuser', 'delete', username=username, confirm=confirm)

    @staticmethod
    def download_omr_button_code(bidround):
        return form_button('get', '/admin/download_omr', 'download', bidround=bidround)

    @staticmethod
    def dtseg_help_button_code():
        return form_button('get', '/admin/dtseg_help', 'help')

    @staticmethod
    def get_msi_names_by_user():
        """
           Return dictionary of lists of market simulation names, by user,
           so msi_names_by_user[uname] will be lists of market simulations that uname has access to
        """
        msi_list = list(cherrypy.request.sa_session.query(MarketSimulationInstance).filter_by())
        msi_names_by_user = {}
        for msi in msi_list:
            for user in msi.users:
                uname = user.username
                if uname in msi_names_by_user:
                    msi_names_by_user[uname].append(msi.name)
                else:
                    msi_names_by_user[uname] = [msi.name]
        return msi_names_by_user

    @staticmethod
    def is_last_admin(user):
        """Check if supplied user object corresponds to only current admin.
        Input user is a user object, not a string with username"""
        if user.credentials == 'admin':
            alist = cherrypy.request.sa_session.query(User).filter_by(credentials='admin')
            if len([a for a in alist]) == 1:
                return True
        return False

    @staticmethod
    def modify_button_code(username):
        return form_button('post', '/admin/changeuser', 'modify', username=username)

    @staticmethod
    def run_model_button_code(bidround):
        return form_button('post', '/admin/run_market_model', 'run', bidround=bidround)

    @staticmethod
    def upload_amc_button_code(bidround):
        txt = """
<form method="post" action="/admin/upload_amc" style="display:inline" enctype="multipart/form-data">
<input type="file" name = "spreadsheet_file" />
<input type="hidden" name="bidround" value="{bidround}"/>
<input type="submit" value = "upload"/></form>""".format(bidround=bidround)
        return txt

    @staticmethod
    def upload_bts_button_code():
        return """<form method="post" action="/admin/upload_bts" style="display:inline"  enctype="multipart/form-data">
<input type="file" name = "bts_spreadsheet_file" />
<input type="submit" value = "upload"/></form>"""

    @staticmethod
    def upload_dtseg_button_code():
        return """<form method="post" action="/admin/upload_dtseg" style="display:inline"  enctype="multipart/form-data">
<input type="file" name = "dtseg_spreadsheet_file" />
<input type="submit" value = "upload"/></form>"""

    @staticmethod
    def upload_fmc_button_code(bidround):
        txt = """
<form method="post" action="/admin/upload_fmc" style="display:inline" enctype="multipart/form-data">
<input type="file" name = "spreadsheet_file" />
<input type="hidden" name="bidround" value="{bidround}"/>
<input type="submit" value = "upload"/>
</form>
""".format(bidround=bidround)
        return txt

    @staticmethod
    def view_all_bids_button(bidround):
        return form_button('get', '/admin/view_all_bids', 'view', bidround=bidround)

    @staticmethod
    def view_amc_button_code(bidround):
        return form_button('get', '/user/view_amc', 'view', bidround=bidround)

    @staticmethod
    def view_bts_button_code():
        return form_button('get', '/user/view_bts', 'view')

    @staticmethod
    def view_dtseg_button_code():
        return form_button('get', '/user/view_dtseg', 'view')

    @staticmethod
    def view_fmc_button_code(bidround):
        return form_button('get', '/user/view_fmc', 'view', bidround=bidround)

    @staticmethod
    def view_omr_button_code(bidround):
        return form_button('get', '/admin/view_omr', 'view', bidround=bidround)

    @staticmethod
    def visualize_supply_curve_options_div(bidround_in='', timesegment_in=''):
        dts = get_object_from_db(DailyTimeSegments)
        bts = get_object_from_db(BidTimeSchedule)
        completed_bidrounds = [r for (r, _, _) in bts if get_object_from_db(OverallMarketReport, bidround=r)]
        txt = ''
        txt += '<div>'
        txt += '<form method="post" action="/admin/visualize_supply_curve" '
        txt += 'style="display:inline" enctype="multipart/form-data">'
        txt += 'Visualize supply curve for round'
        txt += '<select name="bidround">'
        for r in completed_bidrounds:
            if str(r) == str(bidround_in):
                txt += '<option value={r} selected> {r} </option>'.format(r=r)
            else:
                txt += '<option value={r} > {r} </option>'.format(r=r)
        txt += '</select>'
        txt += ', time segment '
        txt += '<select name="timesegment">'
        for ts in dts.names():
            if ts == timesegment_in:
                txt += '<option value="{ts}" selected> {ts} </option>'.format(ts=ts)
            else:
                txt += '<option value="{ts}" > {ts} </option>'.format(ts=ts)
        txt += '</select>'
        txt += '<input type="submit" value="go"/>'
        txt += '</form>'
        txt += '</div>'
        return txt

    @staticmethod
    def _addbts(bts):
        ml = MessageLog()
        if get_object_from_db(BidTimeSchedule):
            ml.add_error('Bid time schedule already in db, not storing')
        else:
            cherrypy.request.sa_session.add(bts)
            cherrypy.request.sa_session.commit()
            ml.add_message('New bid time schedule stored in db')
        return ml

    @staticmethod
    def _addmgf(mgf):
        ml = MessageLog()
        if get_object_from_db(MasterGeneratorFleet):
            ml.add_error('Master generator fleet already in db, not storing')
        else:
            cherrypy.request.sa_session.add(mgf)
            cherrypy.request.sa_session.commit()
            ml.add_message('New master generator fleet stored in db')
        return ml

    @staticmethod
    def _adduser(username, credentials, password):
        """Actually add user to database.
        Returns MessageLog explaining if user added or not and if not, why.

        In addition, associates user with the currently selected simulation
        """
        ml = MessageLog()
        r = User.problems_with_username_password_credentials(username, password, credentials)
        if get_matching_userlist(username):
            r = 'user %s already exists, cannot add' % username
        if r is None:
            u = User(username, credentials, password)
            cherrypy.request.sa_session.add(u)
            cherrypy.request.sa_session.commit()
            ml.add_message('%s added' % username)
        else:
            ml.add_error('%s not added : %s' % (username, r))

        for user in get_matching_userlist(username):
            msi_name = cherrypy.session.get('_cp_currentsimulation')
            msi_list = cherrypy.request.sa_session.query(MarketSimulationInstance).filter_by(name=msi_name)
            for msi in msi_list:
                if user not in msi.users:
                    msi.users.append(user)
                    ml.add_message('user %s added to simulation %s' % (user.username, msi.name))
        cherrypy.request.sa_session.commit()
        return ml

    @staticmethod
    def _add_dtseg(dtseg):
        ml = MessageLog()
        if get_object_from_db(DailyTimeSegments):
            ml.add_error('Daily time segments already in db, not storing')
        else:
            cherrypy.request.sa_session.add(dtseg)
            cherrypy.request.sa_session.commit()
            ml.add_message('New daily time segments stored in db')
        return ml

    @staticmethod
    def _admin_delete_bid(username, bidround):
        """Actually remove bid from database"""
        bid = get_object_from_db(Bid, username=username, bidround=bidround)
        ml = MessageLog()
        if bid:
            cherrypy.request.sa_session.delete(bid)
            cherrypy.request.sa_session.commit()
            ml.add_message('Bid for bidround %d deleted' % bidround)
        else:
            ml.add_error('No bid present to delete')
        return ml

    @staticmethod
    def _changepassword(username, password):
        ulist = get_matching_userlist(username)
        ml = MessageLog()
        if len(ulist) == 1:
            r = User.problems_with_username_password_credentials(ulist[0].username, password, ulist[0].credentials)
            if r is None:
                ulist[0].update_password(password)
                cherrypy.request.sa_session.commit()
                ml.add_message('Password changed for %s' % username)
            else:
                ml.add_error('%s password not changed : %s' % (username, r))
        else:
            ml.add_error('%s is not a user, cannot change password' % username)
        return ml

    @staticmethod
    def _delete_amc(bidround):
        """Actually remove actual market conditions from database"""
        ml = MessageLog()
        amc = get_object_from_db(ActualMarketConditions, bidround=bidround)
        if amc:
            cherrypy.request.sa_session.delete(amc)
            cherrypy.request.sa_session.commit()
            ml.add_message('Actual market conditions for bidround %d deleted' % bidround)
        else:
            ml.add_error('Actual market conditions for bidround %d not present, cannot delete' % bidround)
        return ml

    @staticmethod
    def _delete_bts():
        """Actually remove bid time schedule from database"""
        ml = MessageLog()
        bts = get_object_from_db(BidTimeSchedule)
        if bts:
            cherrypy.request.sa_session.delete(bts)
            cherrypy.request.sa_session.commit()
            ml.add_message('Bid time schedule deleted')
        else:
            ml.add_error('No bid time schedule present to delete')
        return ml

    @staticmethod
    def _delete_dtseg():
        """Actually remove bid time schedule from database"""
        ml = MessageLog()
        dtseg = get_object_from_db(DailyTimeSegments)
        pdict = {'Bid': Bid,
                 'Forecast Market Conditions': ForecastMarketConditions,
                 'Actual Market Conditions': ActualMarketConditions}
        if dtseg:
            for k, v in pdict.items():
                kill_list = cherrypy.request.sa_session.query(v).filter_by(daily_time_segments=dtseg)
                num_to_kill = kill_list.count()
                if num_to_kill > 0:
                    for c in kill_list:
                        cherrypy.request.sa_session.delete(c)
                    ml.add_message('Deleted %d %s object(s) ' % (num_to_kill, k))
            cherrypy.request.sa_session.delete(dtseg)
            cherrypy.request.sa_session.commit()
            ml.add_message('Daily time segments deleted')
        else:
            ml.add_error('No daily time segments present to delete')
        return ml

    @staticmethod
    def _delete_fmc(bidround):
        """Actually remove forecast market conditions from database"""
        ml = MessageLog()
        fmc = get_object_from_db(ForecastMarketConditions, bidround=bidround)
        if fmc:
            cherrypy.request.sa_session.delete(fmc)
            cherrypy.request.sa_session.commit()
            ml.add_message('Forecast market conditions for bidround %d deleted' % bidround)
        else:
            ml.add_error('Forecast market conditions for bidround %d not present, cannot delete' % bidround)
        return ml

    @staticmethod
    def _delete_mgf():
        """Actually remove mgf from database"""
        ml = MessageLog()
        mgf = get_object_from_db(MasterGeneratorFleet)
        if mgf:
            cherrypy.request.sa_session.delete(mgf)
            ml.add_message('Master generator fleet deleted')
        else:
            ml.add_error('No master generator fleet present to delete')
        return ml

    @staticmethod
    def _delete_omr(bidround):
        ml = MessageLog()
        omr = get_object_from_db(OverallMarketReport, bidround=bidround)
        if omr:
            cherrypy.request.sa_session.delete(omr)
            cherrypy.request.sa_session.commit()
            ml.add_message('Overall market report for bidround %d deleted' % bidround)
        else:
            ml.add_error('Overall market report for bidround %d not present, cannot delete' % bidround)
        return ml

    @staticmethod
    def _run_market_model(bidround):
        ml = MessageLog()
        amc = get_object_from_db(ActualMarketConditions, bidround=bidround)
        if not amc:
            ml.add_error('Actual market conditions for bidround %d not found, cannot run model' % bidround)
            return ml

        # load master generator fleet
        mgf = get_object_from_db(MasterGeneratorFleet)  # get_master_generator_fleet()
        if not mgf:
            ml.add_error('Master generator fleet not found, cannot run model')
            return ml

        dtseg = get_object_from_db(DailyTimeSegments)
        if not dtseg:
            ml.add_error('Daily time segments not found, cannot run model')
            return ml

        bts = get_object_from_db(BidTimeSchedule)
        if not bts:
            ml.add_error('Bid time schedule not found, cannot run model')
            return ml

        # check if bid round has closed
        now = datetime.datetime.now()
        oc_by_r = {r: (o, c) for (r, o, c) in bts}
        if bidround not in oc_by_r:
            ml.add_error('Bidround %d not present in bid time schedule, cannot run model' % bidround)
            return ml
        (o, c) = oc_by_r[bidround]

        if o < now < c:
            ml.add_message('Note: bidround %d is still open' % bidround)

        # get owners from mgf
        mgf_owners = set([gu.owner for gu in mgf])
        # get owners from bids
        currbids = [get_object_from_db(Bid, username=u, bidround=bidround) for u in mgf_owners]
        currbids = [b for b in currbids if b]
        bid_owners = set([b.username for b in currbids])

        if bid_owners < mgf_owners:
            omitted_bid_owners = list(mgf_owners - bid_owners)
            omitted_bid_owners.sort()
            ml.add_message('Note: not all users have submitted bids. Missing bid(s) from ' +
                           ', '.join(omitted_bid_owners))

        # filter out the silly unit string so that demand_by_ts[ts] is a float
        demand_by_ts = {ts: amc.demand_by_ts[ts][0] for ts in amc.demand_by_ts}
        (clearing_price_by_ts, market_unit_segment_list_by_ts, market_clearing_log_messages
         ) = run_market_model(mgf, currbids, demand_by_ts, daily_time_segments=dtseg)

        omr = OverallMarketReport(marketsimulationname=get_current_marketsimulationname(),
                                  bidround=bidround, clearing_price_by_ts=clearing_price_by_ts,
                                  market_unit_segment_list_by_ts=market_unit_segment_list_by_ts,
                                  actual_market_conditions=amc,
                                  daily_time_segments=dtseg)

        # save said market model report object to db
        ml.add_message('Electricity market was run successfully. Clearing prices : ' + repr(clearing_price_by_ts))
        ml.add_message([str(k)+':' + str(m) for k, v in market_clearing_log_messages.items() for m in v])
        ml.add_message(omr.add_to_db(cherrypy.request.sa_session))
        return ml


class Authenticator(object):
    err_msg_dict = {'invalid_login': "Invalid Login"}

    ########################
    # exposed regular methods (defining url endpoints)
    ########################

    @cherrypy.expose
    def denied(self):
        txt = 'You do not have access to the requested page.'
        return mage_html_wrap(pagename='Page Denied', body=txt)

    @cherrypy.expose
    def login(self, username=None, password=None, err_msg=None):
        if username is None or password is None:
            return self.get_loginform(err_msg)
        err_msg = self.check_valid_username_password(username, password)
        if err_msg:
            raise cherrypy.InternalRedirect('/auth/login', query_string='err_msg='+err_msg)
        else:
            cherrypy.session[SESSION_KEY] = username
            raise cherrypy.HTTPRedirect('/')

    @cherrypy.expose
    def logout(self):
        for key in [SESSION_KEY, '_cp_currentsimulation']:
            if key in cherrypy.session:
                del cherrypy.session[key]
        raise cherrypy.HTTPRedirect('/auth/login')

    ########################
    # non-exposed regular methods
    ########################

    def get_loginform(self, err_msg):
        stg = self.err_msg_dict.get(err_msg, '&nbsp;')
        txt = """
    <form method="post" action="/auth/login" >
    <table>
<tr>  <td> Username : </td> <td> <input type="text" name = "username" value = "" /> </td> </tr>
<tr> <td> Password : </td> <td> <input type="password" name = "password" /> </td> </tr>
</table>
    <input type="submit" value="Log in"/>
    </form>"""
        if stg:
            txt += '<div style="color:red"> {0} </div>'.format(stg)
        return mage_html_wrap(pagename='Login Page', body=txt, do_nav_footer=False)

    ########################
    # static methods
    ########################

    @staticmethod
    def check_valid_username_password(username, password):
        ulist = get_matching_userlist(username)
        if len(ulist) == 1:
            if ulist[0].is_valid_password(password):
                return None
        # fall through case 
        return "invalid_login"


class AdminAccessAdministerMarketSimulations:
    _cp_config = {'auth.require': [is_admin]}

    ########################
    # exposed regular methods (defining url endpoints)
    ########################

    @cherrypy.expose
    def add_msi(self, msi_name, parent_url='/adminsimulations/'):
        ml = MessageLog()
        # query all msi
        msi_list = list(cherrypy.request.sa_session.query(MarketSimulationInstance).filter_by(name=msi_name))
        if len(msi_list) > 0:
            ml.add_error('Market Simulation with name %s already exists' % msi_name)
        else:
            msi = MarketSimulationInstance(name=msi_name, users=[])
            cherrypy.request.sa_session.add(msi)
            cherrypy.request.sa_session.commit()
            ml.add_message('Market Simulation with name %s added' % msi_name)
        raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json='+ml.to_json())

    @cherrypy.expose
    def delete_msi(self, msi_name, parent_url='/adminsimulations/'):
        ml = MessageLog()
        msi_list = list(cherrypy.request.sa_session.query(MarketSimulationInstance).filter_by(name=msi_name))
        if len(msi_list) == 0:
            ml.add_error('No market simulation named %s found to delete' % msi_name)
        elif len(msi_list) == 1:
            msi = msi_list[0]
            cherrypy.request.sa_session.delete(msi)
            ml.add_message('Market simulation %s deleted' % msi_name)
        else:
            raise MageException('Unexpected number of msi objects returned')

        raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json='+ml.to_json())

    @cherrypy.expose
    def index(self, message_log_json=MessageLog().to_json()):
        currentmsi = cherrypy.session.get('_cp_currentsimulation')
        txt = "<div class=mage_pagename> Admin Market Simulations </div>"
        if currentmsi:
            txt += "<div> Currently selected market simulation : %s </div>" % currentmsi
        else:
            txt += "<div> No market simulation currently selected </div>"

        # list current simulations
        txt += "<div> List of market simulations </div>"
        msi_list = cherrypy.request.sa_session.query(MarketSimulationInstance).filter_by()
        txt += '<ul>'
        for msi in msi_list:
            txt += '<li> %s  %s %s  </li>' % (msi.name,
                                              self.select_msi_button_code(msi.name),
                                              self.delete_msi_button_code(msi.name))
        txt += '</ul>'

        txt += """Add market simulation <form method="post" action="/adminsimulations/add_msi" style="display:inline">
        Market simulation name: <input type="text" name="msi_name" value = "">
        <input type="submit" value ="add" /> </form>"""
        # form to add simulation
        # form to delete simulation
        ml = MessageLog.from_json(message_log_json)
        return mage_html_wrap(body=txt, ml=ml)

    @cherrypy.expose
    def select_msi(self, msi_name, parent_url='/adminsimulations/'):
        ml = MessageLog()
        msi_list = list(cherrypy.request.sa_session.query(MarketSimulationInstance).filter_by(name=msi_name))
        if not msi_list:
            ml.add_error('No market simulation named %s found, cannot select' % msi_name)
        else:
            ml.add_message('Market simulation %s selected' % msi_name)
            cherrypy.session['_cp_currentsimulation'] = msi_name
        raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json='+ml.to_json())

    ########################
    # static methods
    ########################

    @staticmethod
    def delete_msi_button_code(msi_name):
        return form_button('post', '/adminsimulations/delete_msi', 'delete', msi_name=msi_name)

    @staticmethod
    def select_msi_button_code(msi_name):
        return form_button('post', '/adminsimulations/select_msi', 'choose', msi_name=msi_name)


class UserAccessSelectSimulation(object):
    """Class containing all methods corresponding to and used by url endpoints needed to select which
    simulation to play. This is in a separate class because access should be limited to users that have logged in, but
    do not have a simulation selected. The UserAccess and AdminAcess classes will be limited to users that
    do have a simulation selected.
"""
    _cp_config = {'auth.require': [is_admin_or_user]}

    @cherrypy.expose
    def index(self, message_log_json=MessageLog().to_json()):
        """url endpoint for selecting which simulation to play"""
        username = cherrypy.session.get(SESSION_KEY)
        currentsimulation = cherrypy.session.get('_cp_currentsimulation')

        txt = '<div class=mage_pagename> Select Market Simulation </div>'
        if currentsimulation:
            txt += "<div> You have currently selected simulation %s </div>" % currentsimulation
        else:
            txt += "<div> You have have not selected a simulation </div>"

        # list simulations that this user has access to, or all simulations if the user is administrator
        if is_admin():
            msi_list = [msi for msi in cherrypy.request.sa_session.query(MarketSimulationInstance)]
        else:
            msi_list = [msi for msi in cherrypy.request.sa_session.query(MarketSimulationInstance)
                        if username in [u.username for u in msi.users]]

            if len(msi_list) == 1:
                # If user only has access to a single simulation, automatically log them in to it
                cherrypy.session['_cp_currentsimulation'] = msi_list[0].name
                raise cherrypy.InternalRedirect('/')

        if not msi_list:
            txt += "<div> There are no available simulations, most likely MAGE hasn't been configured yet. "
            txt += "Most likely you want to create a simulation on the "
            txt += "<a href=/adminsimulations/>Admin Market Simulation List<a></div>"
        else:
            txt += "<div> Available Simulations "
            txt += "<ul>"
            for msi in msi_list:
                txt += "<li> %s %s </li>" % (msi.name,
                                             form_button('post', '/userselectsimulation/select_msi', 'select',
                                                         msi_name=msi.name))

            txt += "</ul> </div>"
        return mage_html_wrap(body=txt, ml=MessageLog.from_json(message_log_json))

    @cherrypy.expose
    def select_msi(self, msi_name, parent_url='/userselectsimulation/'):
        ml = MessageLog()
        # check if user has access to this simulation
        msi_list = list(cherrypy.request.sa_session.query(MarketSimulationInstance).filter_by(name=msi_name))
        if len(msi_list) == 0:
            ml.add_error('Cannot find simulation named %s' % msi_name)
            pass
        elif len(msi_list) == 1:
            username = cherrypy.session.get(SESSION_KEY)
            if is_admin() or username in [u.username for u in msi_list[0].users]:
                cherrypy.session['_cp_currentsimulation'] = msi_name
                ml.add_message('Market simulation %s selected' % msi_name)
                raise cherrypy.InternalRedirect('/user/home/', query_string='message_log_json='+ml.to_json())
            else:
                ml.add_error('Username %s is not part of simulation %s' % (username, msi_name))
        else:
            raise MageException('Unexpected number of msi objects returned')
        raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json='+ml.to_json())


class UserAccess(object):
    _cp_config = {'auth.require': [is_admin_or_user, assert_currentsimulation_valid]}

    ########################
    # exposed regular methods (defining url endpoints)
    ########################

    @cherrypy.expose
    def change_user_password(self, message_log_json=MessageLog().to_json()):
        username = cherrypy.session.get(SESSION_KEY)
        pagename = 'Change User Password'
        txt = ''
        txt += '<div> Change password for user %s </div>' % username
        txt += """<div> <form method="post" action="/user/_change_user_password" style="display:inline">
        <table class="changeuserpassword">
        <tr> <td> Current password :  </td> <td> <input type="password" name="currentpassword" value=""> </td> </tr>
        <tr> <td> New password : </td> <td> <input type="password" name="newpassword" value=""> </td> </tr>
        <tr><td> Confirm new password : </td> <td> <input type="password" name="confirmnewpassword" value=""></td></tr>
        <tr> <td> <input type="submit" value="submit"> </td> </tr>
        </table>
        <input type="hidden" name="parent_url" value="/user/change_user_password">
        </form> 
        </div>
        """
        return mage_html_wrap(pagename=pagename, body=txt, ml=MessageLog.from_json(message_log_json))

    @cherrypy.expose
    def create_profitability_report(self, bidround, update, parent_url='/user/home'):
        """ url endpoint for viewing individual profitability report"""
        ml = MessageLog()
        bidround = int(bidround)
        ms = get_misc_settings()
        userprofitability_allowed = ms.settings['allow_user_profitability_computation']
        if not userprofitability_allowed:
            ml.add_message('Individual user profitability report disabled')
            raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json=' + ml.to_json())

        # user profitability allowed
        mgf = get_object_from_db(MasterGeneratorFleet)
        omr = get_object_from_db(OverallMarketReport, bidround=bidround)
        if not (mgf and omr):
            ml.add_message('Market report not present, cannot create profitability report')
            raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json=' + ml.to_json())
        else:
            opr = OverallProfitabilityReport(bidround=bidround, mgf=mgf, omr=omr)
            opr.compute()
            username = cherrypy.session.get(SESSION_KEY)
            wb = opr.spreadsheet_repr(owner=username)
            if update == 'view':
                txt = ''
                for sheet in wb:
                    txt += excel_worksheet_to_html_table(sheet, tableclass="market_conditions")
                    txt += '<br><br>'
                return mage_html_wrap(pagename='View Profitability report', body=txt)

            if update == 'download':
                buff = io.BytesIO()
                wb.save(buff)
                buff.seek(0)
                filename = '%s_profitability_round_%d.xlsx' % (username, bidround)
                return cherrypy.lib.static.serve_fileobj(buff, 'application/x-download', 'attachment', filename)

    @cherrypy.expose
    def create_cumulative_profitability_report(self, update, parent_url='/user/home'):
        ml = MessageLog()
        username = cherrypy.session.get(SESSION_KEY)
        ms = get_misc_settings()
        userprofitability_allowed = ms.settings['allow_user_profitability_computation']
        if not userprofitability_allowed:
            ml.add_message('Individual user profitability report disabled')
            raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json=' + ml.to_json())

        mgf = get_object_from_db(MasterGeneratorFleet)
        bts = get_object_from_db(BidTimeSchedule)
        bidroundlist = [r for (r, _, _) in bts]

        wb = Workbook()
        sheet = wb.active

        opr_by_bidround = {}
        for r in bidroundlist:
            omr = get_object_from_db(OverallMarketReport, bidround=r)
            if omr:
                opr_by_bidround[r] = OverallProfitabilityReport(bidround=r, mgf=mgf, omr=omr)
                opr_by_bidround[r].compute()

        column_headings = ['Bid round', 'Fuel costs', 'Fixed costs', 'Operating and Maintenance Costs', 'Revenue',
                           'Profit']

        pt = WSpoint(row=1, col=1)
        pt = write_list_to_sheet(sheet, pt, textlists=[column_headings])
        pt = WSpoint(row=pt.row+1, col=1)
        o = username
        for r in range(1, max(opr_by_bidround.keys())+1):
            if r in opr_by_bidround:
                rowvals = [r,
                           opr_by_bidround[r].total_fuel_cost_by_owner[o],
                           opr_by_bidround[r].total_fixed_cost_by_owner[o],
                           opr_by_bidround[r].total_o_and_m_by_owner[o],
                           opr_by_bidround[r].total_revenue_by_owner[o],
                           opr_by_bidround[r].total_profit_by_owner[o]]
                pt = write_list_to_sheet(sheet, pt, textlists=[rowvals])
            pt = WSpoint(row=pt.row+1, col=1)

        pt = WSpoint(row=pt.row + 1, col=1)
        total_total_fuel_cost = sum([opr_by_bidround[r].total_fuel_cost_by_owner[o] for r in opr_by_bidround.keys()])
        total_total_fixed_cost = sum([opr_by_bidround[r].total_fixed_cost_by_owner[o] for r in opr_by_bidround.keys()])
        total_total_o_and_m = sum([opr_by_bidround[r].total_o_and_m_by_owner[o] for r in opr_by_bidround.keys()])
        total_total_revenue = sum([opr_by_bidround[r].total_revenue_by_owner[o] for r in opr_by_bidround.keys()])
        total_total_profit = sum([opr_by_bidround[r].total_profit_by_owner[o] for r in opr_by_bidround.keys()])
        write_list_to_sheet(sheet, pt, textlists=[['Totals', total_total_fuel_cost, total_total_fixed_cost,
                                                   total_total_o_and_m, total_total_revenue, total_total_profit]])
        if update == 'view':
            txt = excel_worksheet_to_html_table(sheet, tableclass='cumulative_profitability')
            return mage_html_wrap(pagename='Cumulative profitability for user %s' % username,
                                  body=txt)
        if update == 'download':
            buff = io.BytesIO()
            wb.save(buff)
            buff.seek(0)
            filename = '%s_cumulative_profitability.xlsx' % username
            return cherrypy.lib.static.serve_fileobj(buff, 'application/x-download', 'attachment', filename)
        return ''  # this shouldn't be reached

    @cherrypy.expose
    def delete_bid(self, bidround, confirm=False, parent_url='/user/home/'):
        """url endpoint for deleting bid"""
        bidround = int(bidround)
        ml = MessageLog()
        confirm = string_to_bool(confirm)
        username = cherrypy.session.get(SESSION_KEY)
        bid = get_object_from_db(Bid, username=username, bidround=int(bidround))
        if not bid:
            ml.add_error('No bid present to delete')
        else:
            if confirm:
                ml += self._delete_bid(bidround)
            else:
                ml.add_message('Really delete bid for bidround %d'
                               % bidround + self.delete_bid_button_code(bidround=bidround, confirm=True))
        raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json='+ml.to_json())

    @cherrypy.expose
    def download_bid(self, bidround, parent_url='/user/home/'):
        bidround = int(bidround)
        username = cherrypy.session.get(SESSION_KEY)
        currbids = [b for b in cherrypy.request.sa_session.query(Bid).filter_by(username=username)]
        bid_by_round = {bid.bidround: bid for bid in currbids}
        bid = bid_by_round.get(bidround)
        if bid:
            wb = bid.spreadsheet_repr()
            buff = io.BytesIO()
            wb.save(buff)
            buff.seek(0)
            filename = '%s_bid_round%d.xlsx' % (username, bidround)
            return cherrypy.lib.static.serve_fileobj(buff, 'application/x-download', 'attachment', filename)
        else:
            ml = MessageLog(e='Bid for user %s, bidround %d not present, cannot download' % (username, bidround))
            raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json='+ml.to_json())

    @cherrypy.expose
    def download_empty_bidsheet(self, parent_url='/user/home/'):
        """url endpoint for downloading spreadsheet that is a valid bidsheet except bid values are empty
        """
        # create empty bidsheet
        username = cherrypy.session.get(SESSION_KEY)
        mgf = get_object_from_db(MasterGeneratorFleet)
        dtseg = get_object_from_db(DailyTimeSegments)

        if not (mgf and dtseg):
            ml = MessageLog(e='Market model not configured, cannot generate empty bidsheet')
            raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json='+ml.to_json())

        else:
            wb = create_empty_bidsheet(mgf, username, dtseg)
            buff = io.BytesIO()
            wb.save(buff)
            buff.seek(0)
            filename = '%s_empty_bidsheet.xlsx' % username
            return cherrypy.lib.static.serve_fileobj(buff, 'application/x-download', 'attachment', filename)

    @cherrypy.expose
    def download_imr(self, bidround, parent_url='/user/home/'):
        """url endpoint for downloading individual market report
         username is taken from session, not passed as an argument
        """
        username = cherrypy.session.get(SESSION_KEY)
        bidround = int(bidround)
        mgf = get_object_from_db(MasterGeneratorFleet)
        omr = get_object_from_db(OverallMarketReport, bidround=bidround)
        amc = get_object_from_db(ActualMarketConditions, bidround=bidround)

        if mgf and omr and amc:
            wb = omr.spreadsheet_repr(mgf, owner=username)
            buff = io.BytesIO()
            wb.save(buff)
            buff.seek(0)
            filename = 'market_report_%s_round_%d.xlsx' % (username, bidround)
            return cherrypy.lib.static.serve_fileobj(buff, 'application/x-download', 'attachment', filename)

        else:
            ml = MessageLog(e='Market report not present, cannot download')
            raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json='+ml.to_json())

    @cherrypy.expose
    def download_individual_generator_fleet_description(self, parent_url='/user/home/'):
        """url endpoint for downloading spreadsheet that describes all generators belonging to user
        """
        username = cherrypy.session.get(SESSION_KEY)
        mgf = get_object_from_db(MasterGeneratorFleet)

        if not mgf:
            ml = MessageLog(e='Market model not configured, cannot generate individual generator fleet description')
            raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json='+ml.to_json())
        else:
            wb = create_individual_fleet_description(mgf, username)
            buff = io.BytesIO()
            wb.save(buff)
            buff.seek(0)
            filename = '%s_generator_fleet_description.xlsx' % username
            return cherrypy.lib.static.serve_fileobj(buff, 'application/x-download', 'attachment', filename)

    @cherrypy.expose()
    def home(self, message_log_json=MessageLog().to_json()):
        pagename = 'Main Page'
        ms = get_misc_settings()
        userprofitability_allowed = ms.settings['allow_user_profitability_computation']
        txt = ''
        if market_configuration_complete():
            txt += '' #''<div> Electricity Market is configured, ready to accept bids </div>'
        else:
            txt += '<div> Electricity Market is not configured, not ready to accept bids </div>'

        # what kind of user are you?
        if is_admin():
            txt += '<div> You are an administrator, not a regular user, so there is nothing '
            txt += 'for you to do on this page. </div>'
        else:
            txt += '<div> Download Empty Bidsheet %s </div>' % self.download_empty_bidsheet_button()
            txt += ('<div> Individual Generator Fleet Description %s %s </div>'
                    ) % (self.download_individual_generator_fleet_description_button(),
                         self.view_individual_generator_fleet_description_button())

            txt += self.dashboard_table()
            if userprofitability_allowed:
                txt += '<br> <div> View or download cumulative profitability report: '
                txt += '<form method="post" action="/user/create_cumulative_profitability_report" '
                txt += 'style="display:inline" enctype="multipart/form-data">'
                txt += '<input type="submit" name="update" value="view"/>'
                txt += '<input type="submit" name="update" value="download"/>'
                txt += '</form> <div>'

        ml = MessageLog.from_json(message_log_json)
        return mage_html_wrap(pagename=pagename, body=txt, ml=ml)

    @cherrypy.expose
    def upload_bid(self, bidround, bid_spreadsheet_file, parent_url='/user/home/'):
        ml = MessageLog()
        if not hasattr(bid_spreadsheet_file, 'file'):
            ml.add_error('Input is not a file')
        else:
            username = cherrypy.session.get(SESSION_KEY)
            dtseg = get_object_from_db(DailyTimeSegments)
            mgf = get_object_from_db(MasterGeneratorFleet)
            if not (dtseg and mgf):
                ml.add_error('Market not configured, cannot accept bid')
            else:
                bid = Bid(marketsimulationname=get_current_marketsimulationname())
                ml.add_error(bid.initialize_from_spreadsheet_file(bid_spreadsheet_file.file,
                                                                  bidround=int(bidround),
                                                                  daily_time_segments=dtseg))
                e, m = bid.validate_against_master_generator_fleet(mgf)
                ml.add_error(e)
                ml.add_message(m)
                if (bid.username is not None) and bid.username != username: #2024/05/06
                    ml.add_error(('Bid spreadsheet has owner %s, but %s '
                                  'is logged in') % (bid.username, username))
                if not ml.e:
                    ml.add_message(bid.add_to_db(cherrypy.request.sa_session))
        raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json=' + ml.to_json())

    @cherrypy.expose
    def view_amc(self, bidround):
        amc = get_object_from_db(ActualMarketConditions, bidround=bidround)
        if amc:
            txt = '<div> Actual Market Conditions for bidround %d </div>' % int(bidround)
            txt += '<div> %s </div>' % amc.html_repr()
        else:
            txt = '<div> Actual Market Conditions for bidround %d not loaded </div>' % int(bidround)
        return mage_html_wrap(pagename='View Actual Market Conditions', body=txt)

    @cherrypy.expose
    def view_bid(self, bidround):
        bidround = int(bidround)
        username = cherrypy.session.get(SESSION_KEY)
        bid = get_object_from_db(Bid, username=username, bidround=bidround)
        if bid:
            txt = '<div> Bid for user %s, bidround %d</div>' % (username, bidround)
            txt += '<div> %s </div>' % bid.html_repr()
        else:
            txt = '<div> Bid for user %s, bidround %d not present </div>' % (username, bidround)
        return mage_html_wrap(pagename='View Bid', body=txt)

    @cherrypy.expose
    def view_bts(self):
        """url endpoint for viewing bid time schedule"""
        bts = get_object_from_db(BidTimeSchedule)
        if bts:
            txt = '<div> Bid time schedule  : Current time %s </div>' % datetime.datetime.now().ctime()
            txt += '<div> %s </div>' % bts.html_repr()
        else:
            txt = '<div> %s </div>' % 'Bid time schedule not loaded'
        return mage_html_wrap(body=txt, pagename='View Bid Time Schedule')

    @cherrypy.expose
    def view_dtseg(self):
        """url endpoint for viewing daily time segments"""
        dtseg = get_object_from_db(DailyTimeSegments)
        if dtseg:
            txt = '<div> Daily time segments  : Current time %s </div>' % datetime.datetime.now().ctime()
            txt += '<div> %s </div>' % dtseg.html_repr()
        else:
            txt = '<div> %s </div>' % 'Bid time schedule not loaded'
        return mage_html_wrap(pagename='View Daily Time Segments', body=txt)

    @cherrypy.expose
    def view_fmc(self, bidround):
        fmc = get_object_from_db(ForecastMarketConditions, bidround=int(bidround))
        if fmc:
            txt = '<div> Forecast Market Conditions for bidround %d </div>' % int(bidround)
            txt += '<div> %s </div>' % fmc.html_repr()
        else:
            txt = '<div> Forecast Market Conditions for bidround %d not loaded </div>' % int(bidround)
        return mage_html_wrap(pagename='View Forecast Market Conditions', body=txt)

    @cherrypy.expose
    def view_imr(self, bidround):
        """url endpoint for viewing individual market report.
        username is taken from session, not passed as an argument
        """
        username = cherrypy.session.get(SESSION_KEY)
        bidround = int(bidround)
        mgf = get_object_from_db(MasterGeneratorFleet)
        omr = get_object_from_db(OverallMarketReport, bidround=bidround)
        txt = '<div> Individual Market Report for %s, bidround %d </div>' % (username, bidround)

        if mgf and omr:
            wb = omr.spreadsheet_repr(mgf, owner=username)
            txt += '<div> %s </div>' % excel_worksheet_to_html_table(wb.active, tableclass='individual_market_report')
        else:
            txt += '<div> Market report not present, cannot display </div> '
        return mage_html_wrap(pagename='View Market Report', body=txt)

    @cherrypy.expose
    def view_individual_generator_fleet_description(self, parent_url='/user/home/'):
        username = cherrypy.session.get(SESSION_KEY)
        mgf = get_object_from_db(MasterGeneratorFleet)
        txt = ''
        if not mgf:
            ml = MessageLog(e='Market model not configured, cannot generate individual generator fleet description')
            raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json='+ml.to_json())
        else:
            wb = create_individual_fleet_description(mgf, username)
            txt += '<div> Individual generator fleet description for user %s </div>' % username
            txt += '<div> %s </div>' \
                   % excel_worksheet_to_html_table(wb.active, tableclass='individual_generator_fleet_description')
        return mage_html_wrap(pagename='View Generator Fleet', body=txt)

    ########################
    # non-exposed regular methods
    ########################

    def dashboard_table(self):
        if market_configuration_complete():
            username = cherrypy.session.get(SESSION_KEY)
            ms = get_misc_settings()
            userprofitability_allowed = ms.settings['allow_user_profitability_computation']
            # find students bid(s), if any, in db. 
            currbids = [b for b in cherrypy.request.sa_session.query(Bid).filter_by(username=username)]
            # allbids = [b for b in cherrypy.request.sa_session.query(Bid)]
            bid_by_round = {bid.bidround: bid for bid in currbids}

            # txt = ''#<div> Electricity Market is configured, ready to accept bids </div>'
            bts = get_object_from_db(BidTimeSchedule)
            dashtable = '<table class="dashboard">'

            if userprofitability_allowed:
                colheadings = ['Round', 'Bids Open', 'Bids Close', 'Forecast', 'Your Bid(s)', 'Market Report',
                               'Profitability Report']
            else:
                colheadings = ['Round', 'Bids Open', 'Bids Close', 'Forecast', 'Your Bid(s)', 'Market Report']
            dashtable += '<tr>'
            for x in colheadings:
                dashtable += '<th> %s </th>' % x
            dashtable += '</tr>'

            now = datetime.datetime.now()
            for (r, o, c) in bts:
                dashtable += '<tr>'
                dashtable += '<td> %d </td>' % r
                dashtable += '<td> %s </td>' % time_format(o)
                dashtable += '<td> %s </td>' % time_format(c)
                # find forecast, if any, in db
                fmc = get_object_from_db(ForecastMarketConditions, bidround=r)
                if fmc:
                    fmc_td_txt = 'Forecast present'
                    fmc_td_txt += form_button('get', '/user/view_fmc', 'View', bidround=r)
                else:
                    fmc_td_txt = 'Forecast not present'

                dashtable += '<td> %s </td>' % fmc_td_txt

                # find students market report(s), if any, in db
                if r in bid_by_round:
                    bid_td_txt = ' '.join(['Bid present.',
                                           self.view_bid_button_code(bidround=r)])
                    bid_td_txt += self.download_bid_button_code(bidround=r)
                    if o < now < c:
                        # only display delete button if bid window still open
                        bid_td_txt += self.delete_bid_button_code(bidround=r)
                else:
                    bid_td_txt = 'Bid not present.'
                    if o < now < c:
                        bid_td_txt += ' Accepting bids' + self.upload_bid_button_code(bidround=r)
                    # bid_td_txt +=
                    else:
                        bid_td_txt += ' Not accepting bids'

                dashtable += '<td> %s </td>' % bid_td_txt

                omr = get_object_from_db(OverallMarketReport, bidround=r)
                if omr:
                    mr_td_txt = 'Market report present.'
                    mr_td_txt += self.view_imr_button_code(r)
                    mr_td_txt += self.download_imr_button_code(r)
                else:
                    mr_td_txt = 'Market report not present.'

                dashtable += '<td> %s</td>' % mr_td_txt

                if userprofitability_allowed:
                    up_td_txt = """profitability report: 
<form method="post" action="/user/create_profitability_report" enctype="multipart/form-data">
<input type="hidden" name="bidround" value="{r}" />
<input type="submit" name="update" value="view"/>
<input type="submit" name="update" value="download"/> </form>""".format(r=r)
                    dashtable += '<td> %s</td>' % up_td_txt

                dashtable += '</tr>'
            dashtable += '</table>'
            return dashtable
        else:
            # txt = ''#<div> Electricity Market is not currently completely configured </div>'
            return ''

    ########################
    # static methods
    ########################

    @staticmethod
    def delete_bid_button_code(bidround, confirm=False):
        return form_button('post', '/user/delete_bid', 'delete', bidround=bidround, confirm=confirm)

    @staticmethod
    def upload_bid_button_code(bidround):
        txt = """
<form method="post" action="/user/upload_bid" style="display:inline" enctype="multipart/form-data">
<input type="file" name = "bid_spreadsheet_file" />
<input type="hidden" name="bidround" value="{bidround}"/>
<input type="submit" value = "upload"/></form>""".format(bidround=bidround)
        return txt

    @staticmethod
    def view_bid_button_code(bidround):
        txt = """<form method="get" action="/user/view_bid" style="display:inline">
        <input type="hidden" name="bidround" value="{bidround}"/>
        <input type="submit" value="view"/> </form>
        """.format(bidround=bidround)
        return txt

    @staticmethod
    def view_imr_button_code(bidround):
        return form_button('get', '/user/view_imr', 'View Market Report', bidround=bidround)

    @staticmethod
    def download_imr_button_code(bidround):
        return form_button('get', '/user/download_imr', 'Download Market Report', bidround=bidround)

    @staticmethod
    def download_bid_button_code(bidround):
        return form_button('get', '/user/download_bid', 'download', bidround=bidround)

    @staticmethod
    def download_empty_bidsheet_button():
        return form_button('get', '/user/download_empty_bidsheet', 'Download Empty Bidsheet')

    @staticmethod
    def download_individual_generator_fleet_description_button():
        return form_button('get', '/user/download_individual_generator_fleet_description',
                           'Download Generator Fleet Description')

    @staticmethod
    def view_individual_generator_fleet_description_button():
        return form_button('get', '/user/view_individual_generator_fleet_description',
                           'View Generator Fleet Description')

    @cherrypy.expose
    def _change_user_password(self, currentpassword, newpassword, confirmnewpassword, parent_url):
        username = cherrypy.session.get(SESSION_KEY)
        ml = MessageLog()
        ulist = get_matching_userlist(username)
        credentials = ulist[0].credentials
        if ulist[0].is_valid_password(currentpassword):
            if newpassword == confirmnewpassword:
                errstg = User.problems_with_username_password_credentials(username, newpassword, credentials)
                if errstg:
                    ml.add_error(errstg)
                else:
                    # actually change the password here
                    ulist[0].update_password(newpassword)
                    cherrypy.request.sa_session.commit()
                    ml.add_message('Password successfully changed for user %s' % username)
            else:
                ml.add_error("New password and confirm new password do not match")
                pass
        else:
            ml.add_error("Invalid current password")

        raise cherrypy.InternalRedirect(parent_url, query_string='message_log_json=' + ml.to_json())

    @staticmethod
    def _delete_bid(bidround):
        """Actually remove bid from database"""
        ml = MessageLog()
        username = cherrypy.session.get(SESSION_KEY)
        bid = get_object_from_db(Bid, username=username, bidround=bidround)
        if bid:
            cherrypy.request.sa_session.delete(bid)
            cherrypy.request.sa_session.commit()
            ml.add_message('Bid for bidround %d deleted' % bidround)
        else:
            ml.add_error('No bid present to delete')
        return ml


# pieces for cherrypy - sql alchemy integration
# mostly lifted from http://www.defuze.org/archives/222-integrating-sqlalchemy-into-a-cherrypy-application.html
class SAEnginePlugin(cherrypy.process.plugins.SimplePlugin):
    def __init__(self, bus):
        """
        The plugin is registered to the CherryPy engine and therefore
        is part of the bus (the engine *is* a bus) registery.

        We use this plugin to create the SA engine. At the same time,
        when the plugin starts we create the tables into the database
        using the mapped class of the global metadata.

        Finally we create a new 'bind' channel that the SA tool
        will use to map a session to the SA engine at request time.
        """
        cherrypy.process.plugins.SimplePlugin.__init__(self, bus)
        self.sa_engine = None
        self.bus.subscribe("bind", self.bind)

    def start(self):
        self.sa_engine = create_engine('sqlite:///' + database_file)
        Base.metadata.create_all(self.sa_engine)

    def stop(self):
        if self.sa_engine:
            self.sa_engine.dispose()
            self.sa_engine = None

    def bind(self, session):
        session.configure(bind=self.sa_engine)


# noinspection PyProtectedMember,PyArgumentList,PyUnresolvedReferences,PyMissingConstructor
class SATool(cherrypy.Tool):
    def __init__(self):
        """
        The SA tool is responsible for associating a SA session
        to the SA engine and attaching it to the current request.
        Since we are running in a multithreaded application,
        we use the scoped_session that will create a session
        on a per thread basis so that you don't worry about
        concurrency on the session object itself.

        This tools binds a session to the engine each time
        a requests starts and commits/rollbacks whenever
        the request terminates.
        """
        cherrypy.Tool.__init__(self, 'on_start_resource',
                               self.bind_session,
                               priority=20)

        self.session = scoped_session(sessionmaker(autoflush=True,
                                                   autocommit=False))

    def _setup(self):
        cherrypy.Tool._setup(self)
        cherrypy.request.hooks.attach('on_end_resource',
                                      self.commit_transaction,
                                      priority=80)

    def bind_session(self):
        cherrypy.engine.publish('bind', self.session)
        cherrypy.request.sa_session = self.session

    def commit_transaction(self):
        cherrypy.request.sa_session = None
        try:
            self.session.commit()
        except Exception:
            self.session.rollback()
            raise
        finally:
            self.session.remove()


# code below lifted from http://tools.cherrypy.org/wiki/AuthenticationAndAccessRestrictions
def check_auth():
    """A tool that looks in config for 'auth.require'. If found and it
    is not None, a login is required and the entry is evaluated as a list of
    conditions that the user must fulfill"""
    conditions = cherrypy.request.config.get('auth.require', None)
    if conditions is not None:
        username = cherrypy.session.get(SESSION_KEY)
        if username:
            cherrypy.request.login = username
            for condition in conditions:
                # A condition is just a callable that returns true or false
                if not condition():  # should give some indication of why user cannot use this page
                    raise cherrypy.HTTPRedirect("/auth/denied")
        else:
            raise cherrypy.HTTPRedirect("/auth/login")


class Root(object):
    admin = AdminAccess()
    auth = Authenticator()
    user = UserAccess()
    userselectsimulation = UserAccessSelectSimulation()
    adminsimulations = AdminAccessAdministerMarketSimulations()

    @cherrypy.expose
    def index(self):
        username = cherrypy.session.get(SESSION_KEY)
        if username is None:
            raise cherrypy.HTTPRedirect('/auth/login')

        else:
            raise cherrypy.HTTPRedirect('/user/home')


if __name__ == '__main__':
    cherrypy.tools.auth = cherrypy.Tool('before_handler', check_auth)
    SAEnginePlugin(cherrypy.engine).subscribe()
    cherrypy.tools.db = SATool()

    cherrypy.config.update({'server.socket_host': '0.0.0.0'})
    cherrypy.config.update({'server.socket_port': 80})

    if len(sys.argv) > 1:
        if sys.argv[1] == 'local':
            cherrypy.config.update({'server.socket_host': '127.0.0.1'})
            cherrypy.config.update({'server.socket_port': 8080})

    conf = {
        '/': {'tools.sessions.on': True, 'tools.auth.on': True, 'tools.db.on': True},
        global_css_file: {'tools.staticfile.on': True,
                          'tools.staticfile.filename': os.sep.join([basedir, global_css_file])},
        '/images': {'tools.staticdir.on': True, 'tools.staticdir.dir': os.sep.join([basedir, 'images'])},
        '/favicon.ico': {'tools.staticfile.on': True,
                        'tools.staticfile.filename': os.sep.join([basedir, 'images/favicon.ico'])}
    }
    cherrypy.quickstart(Root(), '/', conf)
