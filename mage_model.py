# beginnings of implementing the electricity market model in python
import os
import random
import glob
import string
import collections
import datetime
# from collections import namedtuple
import pdb
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import Numeric
from sqlalchemy import PickleType
from sqlalchemy import DateTime
from sqlalchemy import Float
from sqlalchemy import create_engine
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declared_attr
from openpyxl import load_workbook
from openpyxl import Workbook
import openpyxl
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

Base = declarative_base()
# globals
MAGIC_EXPENSIVE_ENERGY_BIDPRICE = 999
MAGIC_EXPENSIVE_ENERGY_CAPACITY = 1e10  # float('inf')#sys.float_info.max # not inf so 0*this=0.0
UNBID_UNITS_BIDPRICE = float('inf')


# exception classes
class MageDatabaseException(Exception):
    pass


class MageException(Exception):
    pass


class InternalMageError(Exception):
    pass


class MageDemandNotMetExactlyException(Exception):
    pass


# utility functions
def cumsum(x):
    """
    Returns the cumulative sum of x

    :param x:
    :type x: list
    :return: cumulative sum of input x
    :rtype: list
    """
    s = 0.0
    y = []
    for v in x:
        s = s+v
        y.append(s)
    return y


def time_format(dt):
    """
    Convert datetime.datetime object to string

    The format string is hardcoded here, so that it may be used uniformly

    :param dt:
    :type: datetime.datetime
    :return: string representation of time
    :rtype: str
    """
    return dt.strftime('%m/%d/%Y %H:%M')


def printlist(x):
    """
    Prints every element of the list x. Returns nothing.
    """
    for l in x:
        print(l)


def idx(x):
    return [i for (i, a) in enumerate(x) if a]


def is_string(s):
    return isinstance(s, str)


def is_string_list(x):
    return isinstance(x, (list, tuple)) and all([is_string(l) for l in x])


def eqs(s1, s2):
    """
    Tests if strings are equal, after stripping whitespace and
    converting to lowercase

    :param s1:
    :param s2:
    :rtype: bool
    """

    try:
        return s1.strip().lower() == s2.strip().lower()
    except AttributeError:
        return False


def is_strictly_increasing(x):
    return all([a < b for (a, b) in zip(x, x[1:])])


def is_nondecreasing(x):
    return all([a <= b for (a, b) in zip(x, x[1:])])


def is_constant(x):
    return all([a == x[0] for a in x])


def is_integer_range_from_one(x):
    return x == list(range(1, len(x) + 1))


def filter_out_none(x):
    return [l for l in x if l]


class GenericMessageLogger(dict):
    """Represents arbitrary collections of lists of messages
    """
    def __init__(self):
        super().__init__()

    def log(self, key, msg):
        if key not in self:
            self[key] = []
        if isinstance(msg, str):
            self[key].append(msg)
        elif isinstance(msg, (list, tuple)):
            self[key] += msg
        else:
            raise MageException('Unexpected type for msg, not string or list :' + repr(msg))

    def __add__(self, other):
        r = self.__class__()
        for k, v in self.items():
            r[k] = self[k].copy()
        for k, v in other.items():
            if k not in r:
                r[k] = []
            r[k] += other[k]
        return r

    def __getattr__(self, key):
        return self[key]


def enlist(x):
    """Given a tuple or list, return list. Otherwise, return list containing input element"""
    if isinstance(x, list):
        return x
    elif isinstance(x, tuple):
        return list(x)
    else:
        return [x]


def round_to_cents(x):
    return int(x*100)/100


def excel_worksheet_to_html_table(ws, tableclass=None):
    """
    Returns html table representation of excel worksheet

    :param ws: openpyxl.Worksheet
    :param tableclass: string for html table class
    :return: html table as string
    :rtype: str
    """
    if tableclass:
        txt = '<table class=%s>' % tableclass
    else:
        txt = '<table>'
    for row in ws.rows:
        txt += '<tr>'
        for cell in row:
            if cell.value is None:
                txt += '<td> </td>'
            else:
                txt += '<td> %s </td>' % str(cell.value)
        txt += '</tr>'
    txt += '</table>'
    return txt


def parse_username_password_spreadsheet(username_password_spreadsheet):
    """Extracts usernames and passwords from spreadsheet file

    :param username_password_spreadsheet: file-object for spreadsheet (not a file name)
    :type: file-like

    :return: tuple (usernames,passwords,e)
        usernames is list of usernames
        passwords is list of passwords
        e is list of error messages, if any
    :rtype: tuple
    """

    usernames = []
    passwords = []
    e = []
    try:
        wb = load_workbook(username_password_spreadsheet)

        sheets = [s for s in wb]
        if not sheets:
            e.append('No sheets present in spreadsheet')
            return usernames, passwords, e

        return parse_username_password_spreadsheet_sheet(sheets[0])
    except Exception as exc:
        e.append('Unable to load usernames and passwords from provided file : Details - ' + repr(exc))
        return usernames, passwords, e


# same as above but accepts a worksheet as argument
def parse_username_password_spreadsheet_sheet(sheet):

    def is_valid_username(usr):
        return all([c in string.ascii_lowercase for c in usr])

    usernames = []
    passwords = []
    e = []
    # verify first row
    if not (is_string_list([sheet.cell(row=1, column=1 + n).value for n in range(2)]) and
            sheet.cell(row=1, column=1).value.lower() == 'username' and
            sheet.cell(row=1, column=2).value.lower() == 'password'):
        e.append(('first row not as expected : should be username '
                  'in A1, password in B1'))
        return usernames, passwords, e
    for row in list(sheet.rows)[1:]:  # skip first row
        u = str(row[0].value).strip()
        p = str(row[1].value).strip()
        if u and p:  # silently ignore any rows where username or password are empty
            if not is_valid_username(u):
                e.append('Username %s invalid, must be all lowercase letters' % u)
            else:
                usernames.append(u)
                passwords.append(p)

    return usernames, passwords, e


class InitializesFromSpreadsheet:
    """
    Abstract base class for objects that can be loaded from spreadsheets
    """
    def initialize_from_spreadsheet_file(self, sfile, **kwargs):
        """
        Open spreadsheet file, verify that there is at least one sheet,
        and initialize from the open sheet object


        :param sfile: spreadsheet file name
        Additional arguments are passed to `InitializesFromSpreadsheet.initialize_from_spreadsheet`
        :rtype: list[stg]
        """
        e = []  # list of error messages
        sheets = None
        try:
            wb = load_workbook(sfile)
            sheets = [s for s in wb]
            if not sheets:
                e.append('No sheets')
        except Exception as exception_inst:
            e.append('Unable to open spreadsheet file : error message ' + repr(exception_inst))
        if e:
            return e
        else:
            return self.initialize_from_spreadsheet(sheets[0], **kwargs)

    def initialize_from_spreadsheet(self, sheet, **kwargs):
        raise NotImplementedError('abstract class must be subclassed to be used')


class StoredSingleInDB:
    """
    Abstract class for classes that can be stored in db, with only one instance allowed
    in db for each set of acceptable parameters (i.e. bidround, username)
    """

    @classmethod
    def get_from_db(cls, sa_session, **kwargs):
        """
        Returns object of type cls, with parameters as in kwargs, if in database

        :param sa_session:
        :type:

        :rtype: cls or None
        :returns instance of cls in db for given parameters, or none
        :raises: MageDatabaseException
        """
        objlist = [o for o in sa_session.query(cls).filter_by(**kwargs)]
        if len(objlist) > 1:
            errstg = 'Multiple %s in db' % cls.__name__
            if kwargs:
                errstg += ' for '
            for (k, v) in kwargs.items():
                errstg += '%s = %s ' % (k, v)
            raise MageDatabaseException(errstg)
        if len(objlist) == 1:
            return objlist[0]
        return None


class GeneratorConstraints(frozenset):
    """Represents constraints on generator dispatch

    Constraints must be strings that contain no commas, and have no leading or trailing whitespace

    Simple wrapper around frozenset, with __repr__ to convert to string nicely
    """

    def __repr__(self):
        return self.to_string()

    @classmethod
    def from_string(cls, stg):
        """
        :param stg: stg is string of constraints delimited by commas, or may also be None
        :return:
        """
        if stg:
            return cls([s.strip() for s in stg.split(',')])
        else:
            return cls()

    def to_string(self):
        return ', '.join(sorted(list(self)))


class MasterGeneratorFleet(Base, InitializesFromSpreadsheet, StoredSingleInDB):
    """Class representing properties of entire generator fleet
    Essentially this will consist of a list of MasterGeneratorUnit objects
    """
    __tablename__ = 'mastergeneratorfleets'
    id = Column(Integer, primary_key=True)
    marketsimulationname = Column(String, ForeignKey('marketsimulationinstances.name'))
    generatorunits = relationship('MasterGeneratorUnit',
                                  cascade='all,delete-orphan')

    max_num_segments = 3
    common_col_headings = ['Owner', 'Type', 'Unit', 'Total Capacity, MW', 'FOR',
                           'Fuel', 'Variable O&M, $/MWh', 'Fixed Costs', 'Constraints']
    cap_hr_col_headings = ['Capacity, MW', 'Heat Rate, Btu/kWh']

    allowed_constraints = GeneratorConstraints(('must_run', 'slow_ramp'))

    def __init__(self, marketsimulationname):
        self.marketsimulationname = marketsimulationname

    def __iter__(self):
        """ Return iter from generatorunits, allows writing
        for gu in mgf :
"""
        return self.generatorunits.__iter__()

    def verify_header_rows(self, sheet):
        """Check that first two rows of spreadsheet are as expected"""
        e = []
        # row1 = list(sheet.rows)[0]
        # if not ([row1[len(self.common_col_headings) + 2 * n].value for n in range(self.max_num_segments)] ==
        #             ['Segment %d' % (n + 1) for n in range(self.max_num_segments)]):
        #     e.append('First header row not as expected')
        #
        # row2 = [c.value for c in list(sheet.rows)[1]]
        # expected_row2 = (self.common_col_headings +
        #                  self.cap_hr_col_headings * self.max_num_segments)
        # if not all([(r2 == er2) for (r2, er2) in zip(row2, expected_row2)]):
        #     e.append('Second header row not as expected')
        row1 = [c.value for c in list(sheet.rows)[0]]
        expected_row1 = self.tableheadings()
        if len(row1) < len(expected_row1) or not all([r1 == er1 for (r1, er1) in zip(row1, expected_row1)]):
            e.append('First header row not as expected')
            #pdb.set_trace()
        return e

    @staticmethod
    def parse_to_string_list(cell_list, errlist, colname=''):
        r = [c.value for c in cell_list]
        if not is_string_list(r):
            err = '%s values not as expected, not all are strings' % colname
            errlist.append(err)
        return r

    def parse_to_constraint_set_list(self, cell_list, errlist):
        r = [c.value for c in cell_list]
        # values of r should be either None or parse to constraint list
        constraints_list = []
        for (n, stg) in enumerate(r):
            if stg is None:
                curr_constraints = GeneratorConstraints()
            else:
                try:
                    curr_constraints = GeneratorConstraints.from_string(stg)
                    if not curr_constraints <= self.allowed_constraints:
                        unexpected_constraints = curr_constraints-self.allowed_constraints
                        errlist.append('Unexpected constraints found %s' % unexpected_constraints)
                except AttributeError:
                    errlist.append('Unable to parse constraint')
                    curr_constraints = GeneratorConstraints()
            constraints_list.append(curr_constraints)
        return constraints_list

    @staticmethod
    def parse_to_float_or_none(cell_list, errlist, colname='',
                               additional_predicates=()):
        try:
            r = [None if c.value is None else float(c.value)
                 for c in cell_list]
        except (ValueError, TypeError):
            err = '%s values not as expected, not all are valid numbers' % colname
            errlist.append(err)
            return

        # filter out None types before applying predicates
        for p in additional_predicates:
            p([nr for nr in r if nr is not None], errlist, colname)
        return r

    @staticmethod
    def parse_to_float_list(cell_list, errlist, colname='',
                            additional_predicates=()):
        try:
            r = [float(c.value) for c in cell_list]
        except (ValueError, TypeError):
            err = '%s values not as expected, not all are valid numbers' % colname
            errlist.append(err)
            return

        for p in additional_predicates:
            p(r, errlist, colname)
        return r

    # noinspection PyUnusedLocal
    @staticmethod
    def ap_require_positive(floatlist, errlist, colname):
        if not all([f > 0 for f in floatlist]):
            errlist.append('%s values not as expected, should all be positive' % colname)

    @staticmethod
    def ap_require_nonnegative(floatlist, errlist, colname):
        if not all([f >= 0 for f in floatlist]):
            errlist.append('%s values not as expected, should all be non-negative' % colname)

    @staticmethod
    def ap_require_range(floatlist, errlist, colname, valrange=(0, 1)):
        if not all([valrange[0] <= f <= valrange[1] for f in floatlist]):
            errlist.append(('%s values not as expected, should all be in range'
                            ' %g to %g') % (colname, valrange[0], valrange[1]))

    @staticmethod
    def find_rmax(sheet):
        """Find maximum row containing valid generator units.
        This is based on the owner column, it finds the last
        row containing a valid owner"""
        n = 2
        values = [c.value for c in list(sheet.columns)[0]]
        while n < len(values) - 1 and values[n + 1] is not None:
            n += 1
        return n + 1

    def load_and_verify_common_columns(self, sheet):
        e = []
        rmax = self.find_rmax(sheet)
        owner_col = self.parse_to_string_list(list(sheet.columns)[0][1:rmax], e, 'Owner')
        type_col = self.parse_to_string_list(list(sheet.columns)[1][1:rmax], e, 'Type')
        unit_col = self.parse_to_string_list(list(sheet.columns)[2][1:rmax], e, 'Unit')
        total_capacity_col = self.parse_to_float_list(list(sheet.columns)[3][1:rmax], e,
                                                      'Total Capacity',
                                                      [self.ap_require_positive])
        forced_outage_rate_col = self.parse_to_float_list(list(sheet.columns)[4][1:rmax], e,
                                                          'forced_outage_rate', [self.ap_require_range])
        fuel_col = self.parse_to_string_list(list(sheet.columns)[5][1:rmax], e, 'Fuel')
        variable_o_and_m_col = self.parse_to_float_list(list(sheet.columns)[6][1:rmax],
                                                        e, 'Variable O&M',
                                                        [self.ap_require_positive])
        fixed_costs_col = self.parse_to_float_list(list(sheet.columns)[7][1:rmax], e,
                                                   'Fixed Costs',
                                                   [self.ap_require_nonnegative])
        constraints_col = self.parse_to_constraint_set_list(list(sheet.columns)[8][1:rmax], e)

        segment_capacities = [
            self.parse_to_float_or_none(list(sheet.columns)[9 + 2 * n][1:rmax], e,
                                        'Segment Capacity',
                                        [self.ap_require_positive])
            for n in range(self.max_num_segments)]
        segment_heat_rates = [
            self.parse_to_float_or_none(list(sheet.columns)[9 + 1 + 2 * n][1:rmax], e,
                                        'Segment Heat Rate',
                                        [self.ap_require_positive])
            for n in range(self.max_num_segments)]

        if e:
            return e

        # construct master generator unit objects and add to generatorunits
        for n in range(len(unit_col)):
            snchr_triples = [(m+1, segment_capacities[m][n], segment_heat_rates[m][n])
                             for m in range(self.max_num_segments)
                             if (segment_capacities[m][n] is not None and
                                 segment_heat_rates[m][n] is not None)]
            mgu_curr = MasterGeneratorUnit(self.marketsimulationname,
                                           owner_col[n],
                                           type_col[n],
                                           unit_col[n],
                                           total_capacity_col[n],
                                           forced_outage_rate_col[n],
                                           fuel_col[n],
                                           variable_o_and_m_col[n],
                                           fixed_costs_col[n],
                                           snchr_triples,
                                           constraints=constraints_col[n])
            self.generatorunits.append(mgu_curr)
        unitnamelist = [gu.unit for gu in self.generatorunits]
        if len(unitnamelist) != len(set(unitnamelist)):
            e.append('Duplicate unitnames present in spreadsheet')
            # it should be helpful to list the duplicates
            d = {}
            for u in unitnamelist:
                if u in d:
                    d[u] += 1
                else:
                    d[u] = 1
            duplicates = [u for u in unitnamelist if d[u] > 1]
            for u in duplicates:
                e.append('unit %s appears %d times' % (u, d[u]))
        return e

    def verify_segment_capacities_sum_to_unit_capacities(self):
        e = []
        for u in self:
            if not u.capacity == sum([s.capacity for s in u]):
                err = ('Capacity for unit %s not equal to sum '
                       'of segment capacities') % u.unit
                e.append(err)
        return e

    def initialize_from_spreadsheet(self, sheet, **kwargs):
        try:
            e = self.verify_header_rows(sheet)
            if e:
                return e
            e = self.load_and_verify_common_columns(sheet)
            if e:
                return e
            e = self.verify_segment_capacities_sum_to_unit_capacities()
            return e
        except Exception as exc:
            e = ['Error loading master generator fleet from spreadsheet : Details - ' + repr(exc)]
            return e

    @staticmethod
    def tableheadings():
        single_tableheadings = ['Owner', 'Type', 'Unit', 'Capacity (MW)', 'Forced outage rate', 'Fuel',
                                'Variable O and M', 'Fixed costs', 'Constraints']
        one_per_segment_tableheadings = []
        for s in range(1, 1 + Bid.max_num_segments):
            one_per_segment_tableheadings += ['Segment %d capacity' % s, 'Segment %d heat rate' % s]
        tableheadings = single_tableheadings + one_per_segment_tableheadings
        return tableheadings

    def spreadsheet_repr(self):
        wb = Workbook()
        sheet = wb.active
        # single_tableheadings = ['Owner', 'Type', 'Unit', 'Capacity (MW)', 'Forced outage rate', 'Fuel',
        #                         'Variable O and M', 'Fixed costs', 'Constraints']
        # one_per_segment_tableheadings = []
        # for s in range(1, 1 + Bid.max_num_segments):
        #     one_per_segment_tableheadings += ['Segment %d Capacity' % s, 'Segment %d Heat Rate' % s]
        # tableheadings = single_tableheadings + one_per_segment_tableheadings
        tableheadings = self.tableheadings()
        endpt = WSpoint(row=1, col=1)
        endpt = write_list_to_sheet(sheet, endpt, tableheadings, openpyxl.styles.Font(bold=True))
        endpt = WSpoint(row=endpt.row+1, col=1)

        def tablerow(gu):
            row_d = {'Owner': gu.owner,
                     'Type': gu.generator_type,
                     'Unit': gu.unit,
                     'Capacity (MW)': gu.capacity,
                     'Forced outage rate': '%.5g' % gu.forced_outage_rate,
                     'Fuel': gu.fuel,
                     'Variable O and M': '%.5g' % gu.variable_O_and_M,
                     'Fixed costs': '%.5g' % gu.fixed_costs,
                     'Constraints': '%s' % gu.constraints}
            for gs in gu:
                row_d['Segment %d capacity' % gs.segmentnumber] = '%.5g' % gs.capacity
                row_d['Segment %d heat rate' % gs.segmentnumber] = '%.5g' % gs.heat_rate
            return [row_d.get(k, '') for k in tableheadings]

        sorted_generatorunits = sorted(self.generatorunits, key=lambda gu: (gu.owner, gu.unit))
        textlists = [tablerow(gu) for gu in sorted_generatorunits]
        write_list_to_sheet(sheet, endpt, textlists)
        return wb

    def html_repr(self):
        wb = self.spreadsheet_repr()
        return excel_worksheet_to_html_table(wb.active, tableclass='mgf')

    def html_reprz(self):
        """Return representation as html table"""
        sorted_generatorunits = sorted(self.generatorunits,
                                       key=lambda gu: (gu.owner, gu.unit))
        txt = '<table class="mgf">'
        # table headings ...
        txt += '<tr>'
        txt += '\n'.join(['<th> %s </th>' % x for x in
                          ['Owner', 'Type', 'Unit', 'Capacity (MW)', 'forced_outage_rate', 'Fuel',
                           'Variable O&amp;M', 'Fixed Costs', 'Constraints']])
        for s in range(1, 1 + Bid.max_num_segments):
            txt += '<th> %s </th>' % ('Segment %d Capacity' % s)
            txt += '<th> %s </th>' % ('Segment %d Heat Rate' % s)
        txt += '</tr>'

        def tablerow(gu):
            r = '<tr>'
            for x in ['owner', 'generator_type', 'unit', 'capacity', 'forced_outage_rate',
                      'fuel', 'variable_O_and_M', 'fixed_costs', 'constraints']:
                r += '<td> %s </td>' % getattr(gu, x, '')
            capacities = {gs.segmentnumber: gs.capacity for gs in gu}
            heat_rates = {gs.segmentnumber: gs.heat_rate for gs in gu}
            for seg in range(1, 1 + Bid.max_num_segments):
                r += '<td> %s </td>' % capacities.get(seg, '')
                r += '<td> %s </td>' % heat_rates.get(seg, '')
            r += '</tr>'
            return r

        # table rows
        txt += '\n'.join([tablerow(gu) for gu in sorted_generatorunits])
        txt += '</table>'
        return txt

    def maximum_total_generation_capacity(self):
        """ returns total capacity (not considering forced outage rates)"""
        return sum(([float(us.capacity) for u in self for us in u]))

    def expected_total_generation_capacity(self):
        """ returns total expected capacity (considering forced outage rates)
        This is a quick and dirty estimate, and does not properly account for propagation
        of forced outages from lower to higher segments. This means that this will be an overestimate of
        expected capacity
        """
        return sum(([float(us.capacity)*(1.0-u.forced_outage_rate) for u in self for us in u]))


# class to enable storing master generator fleets in database, for purposes of enabling market generation
# tool to create, store, and manage multiple master generator fleets
class MarketgenMasterGeneratorFleet(Base):
    __tablename__ = 'marketgenmastergeneratorfleets'
    id = Column(Integer, primary_key=True)
    marketsimulationname = Column(String, ForeignKey('marketsimulationinstances.name'))
    mastergeneratorfleetname = Column(String)
    mgf = Column(PickleType)

    def __init__(self, mgf, marketsimulationname, mastergeneratorfleetname):
        self.mgf = mgf
        self.marketsimulationname = marketsimulationname
        self.mastergeneratorfleetname = mastergeneratorfleetname


# class to enable storing multiple daily time segments in database, for purposes of enabling
# default daily time segments to be selected
#
# These will be placed into database by bootstrap program
class DefaultDailyTimeSegments(Base):
    __tablename__ = 'defaultdailytimesegments'
    id = Column(Integer, primary_key=True)
    dailytimesegmentsname = Column(String)
    dts = Column(PickleType)

    def __init__(self, dts, dailytimesegmentsname):
        self.dts = dts
        self.dailytimesegmentsname = dailytimesegmentsname


class MageMiscSettings(Base, StoredSingleInDB):
    __tablename__ = 'magemiscsettings'
    id = Column(Integer, primary_key=True)
    marketsimulationname = Column(String, ForeignKey('marketsimulationinstances.name'))
    settings = Column(PickleType)

    def __init__(self, marketsimulationname, **kwargs):
        self.marketsimulationname = marketsimulationname
        self.set_defaults()
        for key in kwargs:
            self.settings[key] = kwargs[key]

    def set_defaults(self):
        self.settings = {'allow_user_profitability_computation': True}


class MasterGeneratorUnit(Base):
    """Class representing single unit of generator fleet"""
    __tablename__ = 'mastergeneratorunits'
    id = Column(Integer, primary_key=True)
    marketsimulationname = Column(String, ForeignKey('marketsimulationinstances.name'))
    mastergeneratorfleet_id = Column(Integer,
                                     ForeignKey('mastergeneratorfleets.id'))

    owner = Column(String)
    generator_type = Column(String)
    unit = Column(String)
    capacity = Column(Integer)
    forced_outage_rate = Column(Float)
    fuel = Column(String)
    variable_O_and_M = Column(Numeric)
    fixed_costs = Column(Numeric)
    constraints = Column(PickleType)
    segments = relationship('MasterGeneratorSegment', cascade='all,delete-orphan')

    def __iter__(self):
        return self.segments.__iter__()

    def __init__(self, marketsimulationname, owner, generator_type, unit, capacity, forced_outage_rate,
                 fuel, variable_o_and_m, fixed_costs, snchr_triples, constraints=frozenset()):
        self.marketsimulationname = marketsimulationname
        self.owner = owner
        self.generator_type = generator_type
        self.unit = unit
        self.capacity = capacity
        self.forced_outage_rate = forced_outage_rate
        self.fuel = fuel
        self.variable_O_and_M = variable_o_and_m
        self.fixed_costs = fixed_costs
        self.segments = [MasterGeneratorSegment(self.marketsimulationname, sn, c, hr)
                         for (sn, c, hr) in snchr_triples]
        self.constraints = constraints


class MasterGeneratorSegment(Base):
    """Class representing data unique to each individual segment
    of a generator"""
    __tablename__ = 'mastergeneratorsegments'
    marketsimulationname = Column(String, ForeignKey('marketsimulationinstances.name'))
    id = Column(Integer, primary_key=True)
    mastergeneratorunit_id = Column(Integer,
                                    ForeignKey('mastergeneratorunits.id'))

    segmentnumber = Column(Integer)
    capacity = Column(Numeric)
    heat_rate = Column(Numeric)

    def __init__(self, marketsimulationname, segmentnumber, capacity, heat_rate):
        self.marketsimulationname = marketsimulationname
        self.segmentnumber = segmentnumber
        self.capacity = capacity
        self.heat_rate = heat_rate


class Bid(Base, InitializesFromSpreadsheet, StoredSingleInDB):
    """Class representing single students bid, at one bidround.
    Storable by sqlalchemy"""
    __tablename__ = 'bids'
    id = Column(Integer, primary_key=True)
    marketsimulationname = Column(String, ForeignKey('marketsimulationinstances.name'))
    username = Column(String)
    bidround = Column(Integer)
    bidcomponents = relationship('BidComponent', cascade='all,delete-orphan')
    daily_time_segments_id = Column(Integer, ForeignKey('dailytimesegments.id'))
    daily_time_segments = relationship('DailyTimeSegments')
    common_col_headings = ['Owner', 'Unit', 'Segment']
    max_num_segments = 3

    def __init__(self, marketsimulationname):
        self.marketsimulationname = marketsimulationname

    def __iter__(self):
        """ allows using
for bc in bid :"""
        return self.bidcomponents.__iter__()

    def __repr__(self):
        owner_stg = "Owner : %s \n" % self.username
        bcp_stg = '\n'.join([bcp.__repr__() for bcp in self.bidcomponents])
        return owner_stg + bcp_stg

    def spreadsheet_repr(self):
        """ Return representation as spreadsheet
        :return:
        """

        wb = Workbook()
        sheet = wb.active
        single_tableheadings = ['Owner', 'Unit', 'Segment']
        one_per_ts_tableheadings = ['Bid Price', 'Capacity']

        endpt = WSpoint(row=1, col=1)
        tableheading1 = ['' for _ in single_tableheadings]
        for ts in self.daily_time_segments.names():
            tableheading1 += [ts, '']
        endpt = write_list_to_sheet(sheet, endpt, tableheading1, openpyxl.styles.Font(bold=True))
        endpt = WSpoint(row=endpt.row+1, col=1)
        tableheading2 = single_tableheadings + len(self.daily_time_segments.names())*one_per_ts_tableheadings
        endpt = write_list_to_sheet(sheet, endpt, tableheading2, openpyxl.styles.Font(bold=True))
        endpt = WSpoint(row=endpt.row+1, col=1)

        sorted_bidcomponents = sorted(self.bidcomponents, key=lambda x: (x.unit, x.segment))
        textlists = []
        for bc in sorted_bidcomponents:
            textlist = [self.username, bc.unit, bc.segment]
            for ts in self.daily_time_segments.names():
                textlist += [bc.bidprices[ts], bc.capacities[ts]]
            textlists.append(textlist)
        write_list_to_sheet(sheet, endpt, textlists)
        return wb

    def html_repr(self):
        wb = self.spreadsheet_repr()
        return excel_worksheet_to_html_table(wb.active, tableclass='bid')

    def html_reprz(self):
        txt = '<table class="bid">'
        # headings
        txt += '<tr>'
        txt += ''.join(['<th> %s </th>' % x
                        for x in ['Owner', 'Unit', 'Segment']])
        for ts in self.daily_time_segments.names():
            txt += '<th> %s </th>' % (ts + ' Bid Price')
            txt += '<th> %s </th>' % (ts + ' Capacity')
        txt += '</tr>'

        # rows
        def tablerow(bc):
            r = '<tr>'
            r += '<td> %s </td>' % self.username
#            r += '<td> %g </td>' % bc.forced_outage_rate
            r += '<td> %s </td>' % bc.unit
            r += '<td> %d </td>' % bc.segment
            for cts in self.daily_time_segments.names():
                r += '<td> %g </td>' % bc.bidprices[cts]
                r += '<td> %g </td>' % bc.capacities[cts]
            r += '</tr>'
            return r

        sorted_bidcomponents = sorted(self.bidcomponents,
                                      key=lambda bc: (bc.unit, bc.segment))
        txt += '\n'.join([tablerow(bc) for bc in sorted_bidcomponents])
        txt += '</table>'
        return txt

    def verify_bids_nondecreasing_with_segment_number(self, unit_col,
                                                      segment_col,
                                                      bidprices_col):
        """Return list of errors unless bid value for segment n+1 >=
        bid value for segment n, for all units in bid"""
        e = []
        for ts in self.daily_time_segments.names():
            d = {}
            # u : unit name, s : segment number, bp : bid price
            # build dictionary of lists of tuples (segment number, bid price)
            for (u, s, bp) in zip(unit_col, segment_col, bidprices_col[ts]):
                if u not in d:
                    d[u] = [(s, bp)]
                else:
                    d[u].append((s, bp))
            # now verify that, if segment_number,bid_price lists are sorted
            # by segment number, the bid prices are nondecreasing
            for u in unit_col:
                d[u].sort()  # will sort by segment number, first
                if not is_nondecreasing([r[1] for r in d[u]]):
                    err = ('Bid price values not as expected : must be '
                           ' nondecreasing with segment number. Unit=%s, '
                           ' time segment=%s') % (u, ts)
                    e.append(err)
                if not is_integer_range_from_one([r[0] for r in d[u]]):
                    err = ('Segment numbers not as expected : must be '
                           'continuous range starting from 1. Unit=%s') % u
                    e.append(err)
        return e

    def verify_header_rows(self, sheet):
        """Return list of errors unless first two rows are as expected"""
        e = []
        # verify first row is as expected
        row1 = list(sheet.rows)[0]
        time_segments_sheet = [row1[len(self.common_col_headings) + 2 * n].value
                               for n in range(len(row1[len(self.common_col_headings):])//2)]
        time_segments_sheet = [c.strip() for c in time_segments_sheet]

        if not all([ts == ts_sheet for (ts, ts_sheet) in
                    zip(self.daily_time_segments.names(), time_segments_sheet)]):
            e.append('First row of input spreadsheet is not as expected')
        # verify second row is as expected
        row2 = list(sheet.rows)[1]
        common_col_headings_sheet = [r.value for (r, _)
                                     in zip(row2, self.common_col_headings)]

        if not all([eqs(cch, cch_sheet) for (cch, cch_sheet) in
                    zip(self.common_col_headings, common_col_headings_sheet)]):
            e.append('Second row of input spreadsheet is not as expected')
        num_common_col_headings = len(self.common_col_headings)
        bidprice_headings_sheet = [row2[num_common_col_headings + 2 * n].value
                                   for n in range(len(self.daily_time_segments.names()))]
        capacity_headings_sheet = [row2[num_common_col_headings + 1 + 2 * n].value
                                   for n in range(len(self.daily_time_segments.names()))]
        if not all([eqs(bp, 'Bid Price') for bp in bidprice_headings_sheet]):
            err = ('Bid Price headings in second row of input spreadsheet '
                   'are not as expected')
            e.append(err)
        if not all([eqs(c, 'Capacity') for c in capacity_headings_sheet]):
            err = ('Capacity headings in second row of input spreadsheet '
                   'are not as expected')
            e.append(err)
        return e

    def load_and_verify_column_data(self, sheet):
        """Scan over columns : owner, unit, segment, bid price,
capacity, bid price, capacity, bid price, capacity
(where # of bid price, capacity pairs is equal to length
of time_segments). Ensure that value formatting is as expected (i.e. numbers
or strings, ranges of numbers as expected).  Return tuple
(owner_col,unit_col,segment_col,bidprices_col,
capacities_col,e) where owner_col, forced_outage_rate_col, unit_col, segment_col are lists
of parsed values bidprices_col, capacities_col are dictionaries, where
bidprices_col[ts] is list of bid prices for specific time_segment,
capacities_col[ts] is list of capacities for specific time_segment.
e is list of errors if anything is not as expected"""

        e = []
        # load and validate data for columns
        # first column - owner
        owner_col = [c.value for c in list(sheet.columns)[0][2:]]
        if not (all([isinstance(c, str) for c in owner_col]) and
                is_constant(owner_col)):
            err = ('Owner values not as expected '
                   '(should be all equal, all character strings)')
            e.append(err)
        # second column - forced_outage_rate (forced outage rate)
        # try:
        #     forced_outage_rate_col = [float(c.value) for c in list(sheet.columns)[1][2:]]
        #     if not all([0.0 <= c <= 1.0 for c in forced_outage_rate_col]):
        #         e.append('forced_outage_rate values must be between 0.0 and 1.0')
        # except ValueError:
        #     e.append('forced_outage_rate values not valid numbers')
        #     forced_outage_rate_col = None
        #
        # second column : unit names
        unit_col = [c.value for c in list(sheet.columns)[1][2:]]
        if not all([isinstance(c, str) for c in unit_col]):
            e.append('Unit values not as expected : should all be strings')
        unit_col = [u.strip() for u in unit_col]
        # 3rd column : segment numbers
        try:
            segment_col = [int(c.value) for c in list(sheet.columns)[2][2:]]
            if not all([1 <= c <= self.max_num_segments for c in segment_col]):
                err = ('Segment numbers not as expected : should be between 1'
                       'and %d') % self.max_num_segments
                e.append(err)
        except ValueError:
            err = 'Segment numbers not as expected : should be valid integers'
            e.append(err)
            segment_col = None

        # verify maximum capacities. Well ... maybe just ignore this? The max capacity
        # len(Bid.common_col_headings)
        # bid price and capacity columns
        # Place into into dictionaries, as below
        # bidprices_col[time_segment]
        # capacities_col[time_segment]
        bidprices_col = {}
        capacities_col = {}
        for (n, ts) in enumerate(self.daily_time_segments.names()):
            try:
                bidprices_col[ts] = [float(c.value)
                                     for c in list(sheet.columns)[len(Bid.common_col_headings) + 2 * n][2:]]
                if not all([0 <= c <= 999 for c in bidprices_col[ts]]):
                    err = ('Bid price values not as expected : '
                           'should be between 0 and 999')
                    e.append(err)
            except ValueError:
                err = ('Bid price values not as expected : '
                       'should be valid floating point numbers')
                e.append(err)
            try:
                capacities_col[ts] = [float(c.value)
                                      for c in list(sheet.columns)[len(Bid.common_col_headings) + 1 + 2 * n][2:]]
            except ValueError:
                err = ('Capacity values not as expected : '
                       'should be valid floating point numbers')
                e.append(err)
        return (owner_col,
                unit_col,
                segment_col,
                bidprices_col,
                capacities_col,
                e)

#    def initialize_from_spreadsheet(self, sheet, bidround):
    def initialize_from_spreadsheet(self, sheet, **kwargs):
        """
        Called as Bid.initialize_from_spreadsheet(sheet,bidround)

        Attempt to parse input spreadsheet (sheet should be a openpyxl
        sheet object, from an opened excel spreadsheet).
        Return list of error messages from parsing"""

        bidround = kwargs['bidround']
        self.daily_time_segments = kwargs['daily_time_segments']
        try:
            e = self.verify_header_rows(sheet)
            if e:
                return e

            (owner_col,
             unit_col,
             segment_col,
             bidprices_col,
             capacities_col,
             e) = self.load_and_verify_column_data(sheet)

            if e:
                return e

            e = self.verify_bids_nondecreasing_with_segment_number(unit_col,
                                                                   segment_col,
                                                                   bidprices_col)
            if e:
                return e
            self.username = owner_col[0].lower()  # note conversion to lowercase
            for (n, (unit, segment)) in enumerate(zip(unit_col, segment_col)):
                bidprices = {ts: bidprices_col[ts][n] for ts in self.daily_time_segments.names()}
                capacities = {ts: capacities_col[ts][n] for ts in self.daily_time_segments.names()}
                self.bidcomponents += [BidComponent(self.marketsimulationname,  unit, segment, bidprices, capacities)]
            self.bidround = bidround
            return e
        except Exception as exc:
            e = ['Error loading bid from spreadsheet : Details - ' + repr(exc)]
            return e

    def validate_against_master_generator_fleet(self, mgf):
        """

        :param mgf:
        :return: (e,m) tuple of lists of error and non-error messages
        """
        def namematch(n1, n2):
            try:
                return n1.lower() == n2.lower()
            except AttributeError:
                return False
        e = []
        m = []
        my_generatorunits = [gu for gu in mgf if
                             namematch(gu.owner, self.username)]
        owned_unitnames = [gu.unit for gu in my_generatorunits]
        bid_unitnames = set([bc.unit for bc in self])

        # Does user own all units that were bid?        
        for u in bid_unitnames:
            if u not in owned_unitnames:
                err = 'Unit %s in bid, but not owned by %s' % (u, self.username)
                e.append(err)
        # Did user bid all the units that he/she owns?
        # Per conversation with hjc this should create a message but not be an error.
        for u in owned_unitnames:
            if u not in bid_unitnames:
                msg = ('Warning : Unit %s owned by %s, but not in bid. '
                       'When market clears, it will be treated as if bid with capacity 0') % (u, self.username)
                m.append(msg)
        if e:
            return e, m

        # build dictionary to index by unitname
        my_generatorunits_d = {}
        for gu in my_generatorunits:
            my_generatorunits_d[gu.unit] = gu
        # build dictionary to index bidcomponents by unitname
        bidcomponents_d = {}
        for bc in self.bidcomponents:
            if bc.unit in bidcomponents_d:
                bidcomponents_d[bc.unit].append(bc)
            else:
                bidcomponents_d[bc.unit] = [bc]

        # verify segment numbers
        for bc in self:
            if not bc.segment <= len(my_generatorunits_d[bc.unit].segments):
                err = ('Unit %s, segment %d not in master record'
                       ) % (bc.unit, bc.segment)
                e.append(err)

        def verify_bid_capacities(mstr_cap, bd_cap):
            """ confirm that bid capacities do not exceed master capacities,
            and that bid capacities are well-formed : i.e. only 'top' segment
            can be less than full (master) capacity.

            master_cap and bid_cap are N-length lists of capacities,
            ordered by segment number, where N is number of segments.
            """
            eqind = idx([bcp == mcp for (bcp, mcp) in zip(bd_cap, mstr_cap)])
            ltind = idx([0 < bcp < mcp for (bcp, mcp) in zip(bd_cap, mstr_cap)])
            zind = idx([bcp == 0 for bcp in bd_cap])

            return ((eqind + ltind + zind == list(range(len(bd_cap)))) and
                    len(ltind) <= 1 and
                    len(mstr_cap) == len(bd_cap))

        # verify that bid capacity less than segment capacity
        # for u in owned_unitnames:
        for u in bid_unitnames:

            bidcomponents_d[u].sort(key=lambda bc: bc.segment)
            my_generatorunits_d[u].segments.sort(key=lambda gs: gs.segmentnumber)
            master_cap = [gs.capacity for gs in my_generatorunits_d[u].segments]
            for ts in self.daily_time_segments.names():
                bid_cap = [bc.capacities[ts] for bc in bidcomponents_d[u]]
                if len(master_cap) == len(bid_cap):
                    if not verify_bid_capacities(master_cap, bid_cap):
                        err = ('Invalid bid capacities for unit %s, time_segment %s. Bid capacities '
                               '(by segment) : %s, Unit capacities (by segment) : %s. Bid capacities '
                               'must be less than unit capacities, only one segment may have a positive bid '
                               'under capacity, and successive segments (if any) must be bid zero'
                               ) % (u, ts, str(bid_cap), str(master_cap))
                        e.append(err)
                else:
                    err = ('Bid for unit %s has different number of segments '
                           '(%d) than are actually present (%d)'
                           ) % (u, len(bid_cap), len(master_cap))
                    e.append(err)
        return e, m

    def add_to_db(self, sa_session):
        """add bid to database unless  bid with same username and bidround is
        already present. Return string indicating what happened"""
        bidlist = [b for b in
                   sa_session.query(Bid).filter_by(username=self.username,
                                                   bidround=self.bidround)]
        if bidlist:
            txt = ('bid with user %s and bidround %d already present, '
                   'not adding') % (self.username, self.bidround)
        else:
            sa_session.add(self)
            txt = 'bid with user %s and bidround %d added' % (self.username,
                                                              self.bidround)
        return txt


class BidComponent(Base):
    """Class to represent components of bid : i.e. bid values for single
    segment of a single unit.  This corresponds to a single row of input
    bid spreadsheet.
    """
    __tablename__ = 'bidcomponents'
    id = Column(Integer, primary_key=True)
    marketsimulationname = Column(String, ForeignKey('marketsimulationinstances.name'))
    # forced_outage_rate = Column(Float)
    unit = Column(String)
    segment = Column(Integer)
    bidprices = Column(PickleType)  # will be dictionary, indexed by time segment
    capacities = Column(PickleType)  # will be dictionary, indexed by time segment

    bid_id = Column(Integer, ForeignKey('bids.id'))

    def __init__(self, marketsimulationname, unit, segment, bidprices, capacities):
        self.marketsimulationname = marketsimulationname
        self.unit = unit
        self.segment = segment
        self.bidprices = bidprices
        self.capacities = capacities

    def __repr__(self):
        return ':'.join([self.unit, str(self.segment),
                         str(self.bidprices), str(self.capacities)])


BidTimeSegmentComponent = collections.namedtuple('BidTimeSegmentComponent', ['name', 'start_time', 'end_time'])


class DailyTimeSegments(Base, InitializesFromSpreadsheet, StoredSingleInDB):
    """ Represents time segments within each day that there is a market for

    time_segment_component_list will be a list of BidTimeSegmentComponent which
    have fields name, start_time, end_time

    """
    __tablename__ = 'dailytimesegments'
    id = Column(Integer, primary_key=True)
    marketsimulationname = Column(String, ForeignKey('marketsimulationinstances.name'))
    time_segment_component_list = Column(PickleType)

    def __init__(self, marketsimulationname):
        self.marketsimulationname = marketsimulationname

    def __iter__(self):
        return self.time_segment_component_list.__iter__()

    def __repr__(self):
        return '\n'.join('%s : %g hours long' % (n, d) for (n, d) in zip(self.names(), self.durations()))

    def html_repr(self):
        """Return representation as html table"""
        def mytf(dt):
            return '%02d:%02d' % (dt.hour, dt.minute)

        start_time_strings = [mytf(tsc.start_time) for tsc in self.time_segment_component_list]
        end_time_strings = [mytf(tsc.end_time) for tsc in self.time_segment_component_list]

        txt = '<table class = "dtseg">'
        # header row
        txt += '<tr>'
        txt += '\n'.join(['<th> %s </th>' % s
                          for s in ['Name', 'Start time', 'End time', 'Duration (hours)']])
        txt += '</tr>'

        for (n, s, e, d) in zip(self.names(), start_time_strings, end_time_strings, self.durations()):
            txt += '\n <tr> <td> %s </td> <td> %s </td> <td> %s </td> <td> %s </td> </tr>' % (n, s, e, d)
        txt += '</table>'
        return txt

    def spreadsheet_repr(self):
        wb = Workbook()
        sheet = wb.active
        sheet.cell(row=1, column=1).value = 'name'
        sheet.cell(row=1, column=2).value = 'start_time'
        sheet.cell(row=1, column=3).value = 'end_time'
        for (n, ts) in enumerate(self.time_segment_component_list):
            sheet.cell(row=2 + n, column=1).value = ts.name
            if ts.start_time == datetime.time(0, 0):
                sheet.cell(row=2 + n, column=2).value = 'midnight'
            else:
                sheet.cell(row=2 + n, column=2).value = ts.start_time
            if ts.end_time == datetime.time(0, 0):
                sheet.cell(row=2 + n, column=3).value = 'midnight'
            else:
                sheet.cell(row=2 + n, column=3).value = ts.end_time

        return wb

    def names(self):
        """
        Returns list of names of time segments
        :return:
        """
        return [x.name for x in self.time_segment_component_list]

    def durations(self):
        """
        Returns list of lengths of time (as number of hours) of each time segment

        :return: list of floats
        """
        start_times = [x.start_time for x in self.time_segment_component_list]

        day_start_times = [datetime.datetime(1, 1, 1, x.hour, x.minute, x.second, x.microsecond) for x in start_times]
        day_start_times.append(datetime.datetime(1, 1, 2, 0, 0, 0, 0))  # midnight the 'day after'
        return [(y-x).total_seconds()/3600.0 for (x, y) in zip(day_start_times, day_start_times[1:])]

    def initialize_from_spreadsheet(self, sheet, **kwargs):
        e = []
        try:
            # verify first row

            if not (is_string_list([c.value for c in list(sheet.rows)[0][0:3]]) and
                    [c.value.lower().strip() for c in list(sheet.rows)[0][0:3]] ==
                    ['name', 'start_time', 'end_time']):
                e.append('First row not as expected, should be name in A1, start_time in B1, end_time in C1')

            names = [c.value for c in list(sheet.columns)[0][1:]]
            start_times = [c.value for c in list(sheet.columns)[1][1:]]
            end_times = [c.value for c in list(sheet.columns)[2][1:]]
            if not start_times[0] == 'midnight':
                e.append("First start time must be 'midnight' (as character string)")
            if not end_times[-1] == 'midnight':
                e.append("Last end time must be 'midnight' (as character string)")
            # modify midnight to datetime.time(0,0)
            start_times[0] = datetime.time(0, 0)
            end_times[-1] = datetime.time(0, 0)

            if not all([isinstance(c, datetime.time) for c in start_times]):
                e.append("All start times must be a valid 24 hour time, or 'midnight'")
            if not all([isinstance(c, datetime.time) for c in end_times]):
                e.append("All end times must be a valid 24 hour time, or 'midnight'")

            if not is_string_list(names):
                e.append('Names of daily time segments must all be strings')
            if not len(names) == len(set(names)):
                e.append('Names of daily time segments must all be unique')
            if not start_times[1:] == end_times[0:-1]:
                e.append('All start times must match previous end time')
            if not is_strictly_increasing(start_times[1:]):
                e.append('Start times must be increasing')
            if e:
                return e
            tmp = [BidTimeSegmentComponent(name=name, start_time=start_time, end_time=end_time)
                   for (name, start_time, end_time) in zip(names, start_times, end_times)]
            self.time_segment_component_list = tmp
            eps = 1e-5
            if abs(sum(self.durations()) - 24.0) > eps:
                e.append('Sum of time durations should equal 24 hours')

        except Exception as exc:
            e.append('Unable to load daily time segments : Details - ' + repr(exc))
            return e

        return e


class BidTimeSchedule(Base, InitializesFromSpreadsheet, StoredSingleInDB):
    """ Represents what date / times bids are open or closed """
    __tablename__ = 'bidtimeschedules'
    id = Column(Integer, primary_key=True)
    marketsimulationname = Column(String, ForeignKey('marketsimulationinstances.name'))
    bidtimes = relationship('BidTimeScheduleRound', cascade='all,delete-orphan')

    def __init__(self, marketsimulationname):
        self.marketsimulationname = marketsimulationname

    def __iter__(self):
        return self.bidtimes.__iter__()

    def initialize_from_spreadsheet(self, sheet, **kwargs):
        # kwargs not used
        # check first row
        e = []
        try:
            if not (is_string_list([c.value for c in list(sheet.rows)[0][0:3]]) and
                    [c.value.lower().strip() for c in list(sheet.rows)[0][0:3]] ==
                    ['round', 'bid open time', 'bid close time']):
                e.append('First row not as expected, should be Round in A1, Bid open time in B1, Bid close time in C1')

            roundlist = [int(c.value) for c in list(sheet.columns)[0][1:]]
            opentimes = [c.value for c in list(sheet.columns)[1][1:]]
            closetimes = [c.value for c in list(sheet.columns)[2][1:]]
        except Exception as exc:
            e.append('Unable to load bid time schedule : Details - ' + repr(exc))
            return e

        if not is_integer_range_from_one(roundlist):
            e.append('Round numbers must be continuous range starting from 1')
        if not all([o < c for (o, c) in zip(opentimes, closetimes)]):
            e.append('Bid close times must all be later than bid open times')
        if e:
            return e

        self.bidtimes = [BidTimeScheduleRound(self.marketsimulationname, r, o, c) for (r, o, c)
                         in zip(roundlist, opentimes, closetimes)]

        return e

    def html_repr(self):
        """Return representation as html table"""

        tf = time_format
        txt = '<table class = "bts">'
        # header row
        txt += '<tr>'
        txt += '\n'.join(['<th> %s </th>' % s
                          for s in ['Round', 'Bid open time', 'Bid close time']])
        txt += '</tr>'

        txt += '\n'.join([('<tr> <td> %d </td> <td> %s </td> <td> %s </td> </tr>'
                           '') % (bt.bidround, tf(bt.opentime), tf(bt.closetime))
                          for bt in self.bidtimes])
        txt += '</table>'
        return txt

    def spreadsheet_repr(self):
        wb = Workbook()
        sheet = wb.active
        sheet.cell(row=1, column=1).value = 'Round'
        sheet.cell(row=1, column=2).value = 'Bid open time'
        sheet.cell(row=1, column=3).value = 'Bid close time'
        for (r, o, c) in self.bidtimes:
            sheet.cell(row=1+r, column=1).value = r
            sheet.cell(row=1+r, column=2).value = o.strftime('%m/%d/%Y %H:%M')
            sheet.cell(row=1+r, column=3).value = c.strftime('%m/%d/%Y %H:%M')
        return wb


class BidTimeScheduleRound(Base):
    __tablename__ = 'bidtimeschedulerounds'
    id = Column(Integer, primary_key=True)
    marketsimulationname = Column(String, ForeignKey('marketsimulationinstances.name'))
    bidtimeschedule_id = Column(Integer,
                                ForeignKey('bidtimeschedules.id'))
    bidround = Column(Integer)
    opentime = Column(DateTime)
    closetime = Column(DateTime)

    def __init__(self, marketsimulationname, bidround, opentime, closetime):
        self.marketsimulationname = marketsimulationname
        self.bidround = bidround
        self.opentime = opentime
        self.closetime = closetime

    def __getitem__(self, index):
        if index < 0 or index > 2:
            raise IndexError
        if index == 0:
            return self.bidround
        if index == 1:
            return self.opentime
        if index == 2:
            return self.closetime


class OverallProfitabilityReport(Base, StoredSingleInDB):
    __tablename__ = 'overallprifitabilityreports'
    id = Column(Integer, primary_key=True)
    marketsimulationname = Column(String, ForeignKey('marketsimulationinstances.name'))
    bidround = Column(Integer)
    omr = Column(PickleType)
    mgf = Column(PickleType)
    total_fuel_cost_by_owner = Column(PickleType)
    total_fixed_cost_by_owner = Column(PickleType)
    total_o_and_m_by_owner = Column(PickleType)
    total_revenue_by_owner = Column(PickleType)
    total_profit_by_owner = Column(PickleType)
    entries_by_ts_us = Column(PickleType)

    OPRentry = collections.namedtuple('OPRentry', ['dispatched_capacity', 'heat_rate', 'fuel_used', 'fuel_type',
                                                   'fuel_cost', 'fixed_cost', 'o_and_m', 'revenue', 'profit'])

    def __init__(self, bidround, mgf, omr):
        self.bidround = bidround
        self.mgf = mgf
        self.omr = omr
        self.total_fuel_cost_by_owner = {}
        self.total_fixed_cost_by_owner = {}
        self.total_o_and_m_by_owner = {}
        self.total_revenue_by_owner = {}
        self.total_profit_by_owner = {}
        self.entries_by_ts_us = {}

    def compute(self):
        dtsnames = self.omr.daily_time_segments.names()
        dtsdurations = self.omr.daily_time_segments.durations()
        heat_rate_by_us = {(u.unit, s.segmentnumber): float(s.heat_rate) for u in self.mgf for s in u}
        fueltype_by_us = {(u.unit, s.segmentnumber): u.fuel for u in self.mgf for s in u}
        fixed_cost_by_us = {(u.unit, s.segmentnumber): float(u.fixed_costs) * 1000 for u in self.mgf for s in u}
        variable_o_and_m_by_us = {(u.unit, s.segmentnumber): float(u.variable_O_and_M) for u in self.mgf for s in u}
        owners = sorted(list(set([u.owner for u in self.mgf])))
        us_by_owner = {owner: [] for owner in owners}
        for u in self.mgf:
            for s in u:
                us_by_owner[u.owner].append((u.unit, s.segmentnumber))

        for (ts, dT) in zip(dtsnames, dtsdurations):
            for us in self.omr.market_unit_segment_list_by_ts[ts]:
                if (us.u, us.s) in heat_rate_by_us:
                    dc = us.dispatched_capacity  # dispatched capacity in MW
                    hr = heat_rate_by_us[(us.u, us.s)]  # heat rate in BTU/KWh
                    ft = fueltype_by_us[(us.u, us.s)]  # fuel type
                    # fp = fuelprice_by_us[(us.u,us.s)]
                    fp = self.omr.actual_market_conditions.fuelprices[ft][0]  # fuel price in $/MMBTU
                    fuel_used = dc*dT*hr/1000.0  # fuel used, in MMBTU
                    # note dc*dT is energy dispatched over entire time segment, in MWh
                    #  dc*dT*1000 is energy dispatched over entire time segment, in kWh
                    #  dc*dT*1000*hr is fuel used in BTU
                    #  (dc*dT*1000*hr)/1e6 =  (dc*dT*hr/1000.0 is fuel used in MMBTU
                    mcp = self.omr.clearing_price_by_ts[ts]

                    fuel_cost = round_to_cents(fuel_used*fp)
                    if us.s == 1:
                        # fixed cost for entire generator, assign it only to segment 1
                        fixed_cost = fixed_cost_by_us[(us.u, us.s)]
                    else:
                        fixed_cost = 0
                    o_and_m = round_to_cents(dT * dc * variable_o_and_m_by_us[(us.u, us.s)])
                    revenue = round_to_cents(mcp * dc*dT)
                    profit = round_to_cents(revenue - (fuel_cost+fixed_cost + o_and_m))
                    self.entries_by_ts_us[(ts, us.u, us.s)] = self.OPRentry(dispatched_capacity=dc,
                                                                            heat_rate=hr,
                                                                            fuel_used=fuel_used,
                                                                            fuel_type=ft,
                                                                            fuel_cost=fuel_cost,
                                                                            fixed_cost=fixed_cost,
                                                                            o_and_m=o_and_m,
                                                                            revenue=revenue,
                                                                            profit=profit)

        for owner in owners:
            us_list = us_by_owner[owner]
            fuelcosts = [self.entries_by_ts_us[(ts, u, s)].fuel_cost for (u, s) in us_list for ts in dtsnames]
            self.total_fuel_cost_by_owner[owner] = round_to_cents(sum(fuelcosts))
            fixedcosts = [self.entries_by_ts_us[(ts, u, s)].fixed_cost for (u, s) in us_list for ts in dtsnames]
            self.total_fixed_cost_by_owner[owner] = round_to_cents(sum(fixedcosts))
            o_and_m_s = [self.entries_by_ts_us[(ts, u, s)].o_and_m for (u, s) in us_list for ts in dtsnames]
            self.total_o_and_m_by_owner[owner] = round_to_cents(sum(o_and_m_s))
            revenues = [self.entries_by_ts_us[(ts, u, s)].revenue for (u, s) in us_list for ts in dtsnames]
            self.total_revenue_by_owner[owner] = round_to_cents(sum(revenues))
            profits = [self.entries_by_ts_us[(ts, u, s)].profit for (u, s) in us_list for ts in dtsnames]
            self.total_profit_by_owner[owner] = round_to_cents(sum(profits))

    def spreadsheet_repr(self, owner=None):
        """If owner is none, return spreadsheet for profitability report for all users. If owner is specified,
        return report only for that user"""
        wb = Workbook()
        sheet = wb.active
        sheet.title = 'Summary'
        sheet.cell(row=1, column=1).value = 'Overall profit/loss report'
        col_headings = ['Owner', 'Total fuel cost', 'Total fixed costs', 'Total operating and maintenance',
                        'Total revenue', 'Total profit']
        pt = WSpoint(row=3, col=1)
        write_list_to_sheet(sheet, pt, [col_headings])
        pt = WSpoint(row=pt.row+1, col=1)

        if owner is None:
            output_owners = sorted(list(set([u.owner for u in self.mgf])))
        else:
            output_owners = [owner]

        us_by_owner = {o: [] for o in output_owners}
        for u in self.mgf:
            for s in u:
                if u.owner in output_owners:
                    us_by_owner[u.owner].append((u.unit, s.segmentnumber))

        for o in output_owners:
            tl = [[o,
                   self.total_fuel_cost_by_owner[o],
                   self.total_fixed_cost_by_owner[o],
                   self.total_o_and_m_by_owner[o],
                   self.total_revenue_by_owner[o],
                   self.total_profit_by_owner[o]]]
            write_list_to_sheet(sheet, pt, textlists=tl)
            pt = WSpoint(row=pt.row+1, col=1)

        for ts, duration in zip(self.omr.daily_time_segments.names(), self.omr.daily_time_segments.durations()):
            newtitle = ts[0:31]
            wb.create_sheet(newtitle)  # There is a 31 character limit on sheet names
            sheet = wb[newtitle]
            pt = WSpoint(row=1, col=1)
            tl = [['Time segment', ts],
                  ['Duration', '%s h' % duration],
                  ['Clearing price', self.omr.clearing_price_by_ts[ts], '$/MWh']]
            pt = write_list_to_sheet(sheet, pt, tl)
            pt = WSpoint(row=pt.row + 1, col=1)
            pt = WSpoint(row=pt.row + 1, col=1)
            # write out fuel prices
            tl = [['Fuel', 'Price', 'Units']]
            for ft in sorted(self.omr.actual_market_conditions.fuelprices.keys()):
                tl.append([ft,
                           self.omr.actual_market_conditions.fuelprices[ft][0],
                           self.omr.actual_market_conditions.fuelprices[ft][1]])
            pt = write_list_to_sheet(sheet, pt, tl)
            pt = WSpoint(row=pt.row + 1, col=1)
            pt = WSpoint(row=pt.row + 1, col=1)

            col_headings = ['Owner', 'Unit', 'Segment', 'Dispatched Capacity (MW)', 'Heat Rate (BTU/KWh)',
                            'Fuel used (MMBTU)', 'Fuel Type', 'Fuel cost ($)', 'Fixed costs ($)',
                            'Operating and maintenance ($)', 'Revenue ($)', 'Profit ($)']
            write_list_to_sheet(sheet, pt, [col_headings])
            pt = WSpoint(row=pt.row+1, col=1)
            for o in output_owners:
                uslist = us_by_owner[o]
                uslist.sort(key=lambda x: (x[0], x[1]))
                for us in uslist:
                    en = self.entries_by_ts_us[(ts, us[0], us[1])]
                    tl = [[o, us[0], int(us[1]), en.dispatched_capacity, en.heat_rate, en.fuel_used, en.fuel_type,
                           en.fuel_cost, en.fixed_cost, en.o_and_m, en.revenue, en.profit]]
                    write_list_to_sheet(sheet, pt, textlists=tl)
                    pt = WSpoint(row=pt.row + 1, col=1)
        return wb


class OverallMarketReport(Base, StoredSingleInDB):
    __tablename__ = 'overallmarketreports'
    id = Column(Integer, primary_key=True)
    marketsimulationname = Column(String, ForeignKey('marketsimulationinstances.name'))
    bidround = Column(Integer)
    clearing_price_by_ts = Column(PickleType)
    # dispatched_capacity_d_by_ts = Column(PickleType)
    # forced_out_d_by_ts = Column(PickleType)
    market_unit_segment_list_by_ts = Column(PickleType)
    actual_market_conditions = Column(PickleType)
    daily_time_segments_id = Column(Integer, ForeignKey('dailytimesegments.id'))
    daily_time_segments = relationship('DailyTimeSegments')

    def __init__(self, marketsimulationname, bidround,
                 clearing_price_by_ts,
                 market_unit_segment_list_by_ts,
                 actual_market_conditions,
                 daily_time_segments
                 ):
        self.marketsimulationname = marketsimulationname
        self.bidround = bidround
        self.clearing_price_by_ts = clearing_price_by_ts
        # self.dispatched_capacity_d_by_ts = dispatched_capacity_d_by_ts
        # self.forced_out_d_by_ts = forced_out_d_by_ts
        self.market_unit_segment_list_by_ts = market_unit_segment_list_by_ts
        self.actual_market_conditions = actual_market_conditions
        self.daily_time_segments = daily_time_segments

    def html_repr(self, mgf):
        wb = self.spreadsheet_repr(mgf)
        return excel_worksheet_to_html_table(wb.active, tableclass='overall_market_report')

    # def html_repr_z(self, mgf):
    #
    #     # separate tables for separate time_periods (formerly known as time segments)
    #
    #     # headings : owner, unit, segment, forced_outage_rate, Forced Out, bidprice, capacity with forced_outage_rate,
    #     # cumulative capacity, called capacity
    #     txt = ''
    #
    #     # create dictionary of owners indexed by (u,s) tuple
    #     owner_d = {(gu.unit, gs.segmentnumber): gu.owner for gu in mgf for gs in gu}
    #     forced_outage_rate_d = {(gu.unit, gs.segmentnumber): gu.forced_outage_rate for gu in mgf for gs in gu}
    #     for ts in self.daily_time_segments.names():
    #         # headings
    #
    #         tableheadings = ['Owner', 'Unit', 'Segment', 'forced_outage_rate', 'Forced Out',
    #                          'Bidprice', 'Capacity w/ outage', 'Cumulative Capacity', 'Called Capacity']
    #         txt += '<div> Time Period : %s. Market clearing price : %g </div>' % (ts, self.clearing_price_by_ts[ts])
    #         txt += '<table class="overall_market_report">'
    #         txt += '<tr>'
    #         txt += ' '.join(['<th> %s </th>' % x for x in tableheadings])
    #         txt += '</tr>'
    #
    #         # rows
    #         # List of market unit segments gets turned into plain list when pickled,unpickled
    #         # using json pickler. Put it back into MarketUnitSegment named tuples.
    #         # this is a bit hacky ...
    #         market_unit_segment_list = [MarketUnitSegment(*x) for x in self.L_by_ts[ts]]
    #         cumulative_capacity_list = list(cumsum([mu.capacity for mu in market_unit_segment_list]))
    #         for (n, mu) in enumerate(market_unit_segment_list):  # these are already in proper order
    #             # owner
    #             owner = owner_d.get((mu.u, mu.s), '')
    #             # unit
    #             unit = mu.u
    #             # segment
    #             segment = mu.s
    #             # forced_outage_rate
    #             forced_outage_rate = forced_outage_rate_d.get((mu.u, mu.s), 0)  # dummy for now
    #             #
    #             forced_out = {True: 'Yes', False: 'No'}[self.forced_out_d_by_ts[ts][(mu.u, mu.s)]]
    #             bidprice = mu.bidprice
    #             capacity_with_outage = mu.capacity
    #             cumulative_capacity = cumulative_capacity_list[n]
    #             called_capacity = self.dispatched_capacity_d_by_ts[ts][(mu.u, mu.s)]
    #             txt += '<tr>'
    #             txt += ' '.join(['<td> %s </td>' % str(x) for x in
    #                              [owner, unit, segment, forced_outage_rate, forced_out, bidprice,
    #                               capacity_with_outage, cumulative_capacity, called_capacity]])
    #             txt += '</tr>'
    #         txt += '</table>'
    #     return txt

    def spreadsheet_repr(self, mgf, owner=None):
        """If owner specified, display only generators belonging to specified owner

        If owner specified then output will be an "individual market report"
        In this case the columns will not include cumulative capacity, and rows will be sorted
        by unit,segment

        Otherwise, rows will be sorted by as returned by market clearing mechanism, i.e. forced on units first,
        then other units in order of increasing bidprice.

        """

        wb = Workbook()
        sheet = wb.active

        if owner is None:
            tableheadings = ['Owner', 'Unit', 'Segment', 'forced_outage_rate', 'Forced Out', 'Forced On',
                             'Bidprice', 'Bid capacity w/ outage', 'Cumulative bid capacity', 'Dispatched capacity',
                             'Cumulative dispatched capacity']
        else:
            tableheadings = ['Owner', 'Unit', 'Segment', 'forced_outage_rate', 'Forced Out', 'Forced On',
                             'Bidprice', 'Bid capacity w/ outage', 'Dispatched capacity']

        # create dictionary of owners indexed by (u,s) tuple
        owner_d = {(gu.unit, gs.segmentnumber): gu.owner for gu in mgf for gs in gu}
        for specialunit in [MarketUnitSegment.magic_expensive_energy(), MarketUnitSegment.magic_cheap_energy()]:
            owner_d[(specialunit.u, specialunit.s)] = None

        forced_outage_rate_d = {(gu.unit, gs.segmentnumber): gu.forced_outage_rate for gu in mgf for gs in gu}

        endpt = WSpoint(row=1, col=1)

        for ts in self.daily_time_segments.names():
            sectionhead = [['Time Segment', str(ts)],
                           ['Market Clearing Price', str(self.clearing_price_by_ts[ts])],
                           ['Actual demand : '] + list(self.actual_market_conditions.demand_by_ts[ts]),
                           ['Fuelprices : ']
                           ]
            sectionhead += [[''] + [k] + list(v) for (k, v) in self.actual_market_conditions.fuelprices.items()]
            endpt = write_list_to_sheet(sheet, endpt, sectionhead, openpyxl.styles.Font(bold=True, color='FF0000FF'))
            endpt = WSpoint(row=endpt.row+1, col=1)
            write_list_to_sheet(sheet, endpt, tableheadings, openpyxl.styles.Font(bold=True))
            endpt = WSpoint(row=endpt.row+1, col=1)

            market_unit_segment_list = self.market_unit_segment_list_by_ts[ts]

            def ownermatch_p(us):
                try:
                    # return owner_d[(us.u, us.s)] is None or owner_d[(us.u, us.s)].lower() == owner.lower()

                    # special units will not appear in individual market report
                    return owner_d[(us.u, us.s)].lower() == owner.lower()
                except AttributeError:
                    return False

            if owner is not None:
                market_unit_segment_list = [us for us in market_unit_segment_list if ownermatch_p(us)]
                market_unit_segment_list.sort(key=lambda x: (x.u, x.s))

            cumulative_capacity_list = list(cumsum([us.effective_capacity() for us in market_unit_segment_list]))
            cumulative_dispatched_capacity = list(cumsum([us.dispatched_capacity for us in market_unit_segment_list]))
            txtlists = []
            for (n, us) in enumerate(market_unit_segment_list):  # these are already in proper order
                row_d = {'Owner': owner_d.get((us.u, us.s), ''),
                         'Unit': us.u,
                         'Segment': us.s,
                         'forced_outage_rate': '%.5g' % forced_outage_rate_d.get((us.u, us.s), 0),
                         'Forced Out': {True: 'Yes', False: 'No'}[us.forced_out],
                         'Forced On': {True: 'Yes', False: 'No'}[us.forced_on],
                         'Bidprice': us.bidprice,
                         'Bid capacity w/ outage': us.effective_capacity(),
                         'Cumulative bid capacity': cumulative_capacity_list[n],
                         'Dispatched capacity': us.dispatched_capacity,
                         'Cumulative dispatched capacity': cumulative_dispatched_capacity[n]
                         }
                txtlists.append([str(row_d[key]) for key in tableheadings])
            endpt = write_list_to_sheet(sheet, endpt, txtlists)
            endpt = WSpoint(row=endpt.row+2, col=1)  # skip another row
        return wb

    # def create_individual_market_report_spreadsheet(self, mgf, owner):
    #     pass
    #     wb = Workbook()
    #     sheet = wb.active
    #
    #     endpt = WSpoint(1, 1)
    #     if owner:
    #         endpt = write_list_to_sheet(sheet, WSpoint(row=endpt.row + 2, col=1),
    #                                     'Market Report for ' + owner)
    #     else:
    #         endpt = write_list_to_sheet(sheet, WSpoint(endpt.row + 2, 1),
    #                                     'Overall Market Report')
    #
    #     endpt = write_list_to_sheet(sheet, WSpoint(endpt.row + 2, 1), 'Actual Fuel Price')
    #     endpt = write_list_to_sheet(sheet, origin=WSpoint(endpt.row + 1, 1),
    #                                 textlists=[[x] + enlist(y) for (x, y) in sorted(fuelprices.items())])
    #     endpt = write_list_to_sheet(sheet, WSpoint(endpt.row + 2, 1), 'Actual demand')
    #     endpt = write_list_to_sheet(sheet, origin=WSpoint(endpt.row + 1, 1),
    #                                 textlists=[[ts] + enlist(d) for (ts, d) in list(actual_demand_by_ts.items())])
    #
    #     endpt = write_list_to_sheet(sheet, WSpoint(endpt.row + 2, 1),
    #                                 'Market clearing prices')
    #     endpt = write_list_to_sheet(sheet, origin=WSpoint(endpt.row + 1, 1),
    #                                 textlists=[[ts] + [str(p), '$/MWh']
    #                                            for (ts, p) in list(clearing_price_by_ts.items())])
    #
    #     dpc_row = endpt.row + 2
    #     endpt = WSpoint(dpc_row, -1)  # dummy ... so that adding 2 to col gives 1
    #     truefalse_d = {True: 'Forced out', False: ''}
    #
    #     # create dictionary of owners indexed by (u,s) tuple
    #     owner_d = {(gu.unit, gs.segmentnumber): gu.owner for gu in mgf for gs in gu}
    #
    #     if owner:
    #         # write out market report for individual owner
    #         for ts in daily_time_segments.names():
    #             dpc_d = dispatched_capacity_d_by_ts[ts]
    #             forced_out_d = forced_out_d_by_ts[ts]
    #
    #             # us : should be list of (u,s) tuples for units that are
    #             # owned by owner
    #             us = sorted([us for us in list(dpc_d.keys()) if owner_d.get(us, '').lower() == owner])
    #
    #             textlists = [[ts],
    #                          ['unit', 'segment', 'dispatched capacity', 'forced outage']
    #                          ]
    #             textlists += [[u, str(s), str(dpc_d[(u, s)]),
    #                            truefalse_d[forced_out_d[(u, s)]]]
    #                           for (u, s) in us]
    #             endpt = write_list_to_sheet(sheet,
    #                                         origin=WSpoint(dpc_row, endpt.col + 2),
    #                                         textlists=textlists)
    #     else:
    #         # write out overall market report
    #
    #         for ts in daily_time_segments.names():
    #             # reconstruct cumulative capacity
    #             dpc_d = dispatched_capacity_d_by_ts[ts]
    #             forced_out_d = forced_out_d_by_ts[ts]
    #             market_unit_segment_list = market_unit_segment_list_by_ts[ts]
    #             cumulative = list(cumsum([mu.capacity for mu in market_unit_segment_list]))
    #             cumulative_d = {(mu.u, mu.s): c for (mu, c) in zip(market_unit_segment_list, cumulative)}
    #             bidprice_d = {(mu.u, mu.s): mu.bidprice for mu in market_unit_segment_list}
    #             capacity_with_outages_d = {(mu.u, mu.s): mu.capacity for mu in market_unit_segment_list}
    #             us = sorted(list(dpc_d.keys()),
    #                         key=lambda usr: (bidprice_d[usr], usr[0], usr[1]))
    #
    #             # not filtering by owner
    #             textlists = [[ts],
    #                          ['unit', 'segment', 'owner', 'bidprice', 'capacity with outages', 'cumulative capacity',
    #                           'dispatched capacity', 'forced outage']]
    #             textlists += [[u, str(s), owner_d.get((u, s), ''), str(bidprice_d[(u, s)]),
    #                            capacity_with_outages_d[(u, s)], cumulative_d[(u, s)],
    #                            str(dpc_d[(u, s)]),
    #                            truefalse_d[forced_out_d.get((u, s), False)]]
    #                           for (u, s) in us]
    #             endpt = write_list_to_sheet(sheet,
    #                                         origin=WSpoint(dpc_row, endpt.col + 2),
    #                                         textlists=textlists)
    #     return wb
    #

    def add_to_db(self, sa_session):
        if self.get_from_db(sa_session, bidround=self.bidround, marketsimulationname=self.marketsimulationname):
            txt = ('Overall market report for bidround %d already present'
                   ', not adding') % self.bidround
        else:
            sa_session.add(self)
            txt = 'Overall market report for bidround %d added' % self.bidround
        return txt

        # @classmethod
        # def get_from_db(cls, sa_session, bidround):
        #     """
        #     Returns overall market report for given bidround, if in database
        #
        #     :param bidround : int
        #     :rtype: OverallMarketReport or None
        #     :returns overall market report
        #     :raises: MageDatabaseException
        #     """
        #     if bidround == 'all':
        #         bts = get_bid_time_schedule()
        #         if bts:
        #             return filter_out_none([cls.get_from_db(sa_session, r) for (r, o, c) in bts])
        #         else:
        #             return []
        #     else:
        #         omrlist = [omr for omr in sa_session.query(OverallMarketReport).filter_by(bidround=bidround)]
        #         if len(omrlist) > 1:
        #             raise MageDatabaseException('Multiple overall market reports for bidround %d in db' % bidround)
        #         if len(omrlist) == 1:
        #             return omrlist[0]


class AbstractMarketConditions(InitializesFromSpreadsheet, StoredSingleInDB):
    id = Column(Integer, primary_key=True)
    bidround = Column(Integer)
    demand_by_ts = Column(PickleType)  # dictionary, indexed by ts
    fuelprices = Column(PickleType)  # dictionary, indexed by fuel type

    # dkh : I need to define these as callables, due to how sqlAlchemy interacts with inheiritance.
    @declared_attr
    def daily_time_segments_id(self):
        return Column(Integer, ForeignKey('dailytimesegments.id'))

    @declared_attr
    def daily_time_segments(self):
        return relationship('DailyTimeSegments')

    forecast_or_actual = ''

    # noinspection PyAttributeOutsideInit
    def initialize_from_spreadsheet(self, sheet, **kwargs):
        """
        call as initialize_from_spreadsheet(sheet,bidround=bidround,daily_time_segments=daily_time_segments)
        """
        bidround = kwargs['bidround']
        self.daily_time_segments = kwargs['daily_time_segments']
        e = []
        frow = [str(c.value) for c in list(sheet.rows)[0][0:3]]
        if [s.strip().lower() for s in frow] != ['fuel', 'forecast price', 'units']:
            e.append('First should be Fuel, Forecast Price, Units')

        # find blank row
        def blankvalue(v):
            return (v is None) or str(v).strip() == ''
        n = None
        for n, c in enumerate(list(sheet.columns)[0]):
            if blankvalue(c.value):
                break
        blankrowidx = n
        try:
            drow = [str(c.value) for c in list(sheet.rows)[blankrowidx + 1][0:3]]
            if [s.strip().lower() for s in drow] != ['time segment', 'forecast demand', 'units']:
                e.append('Row after blank row should be Time Segment, Forecast Demand, Units')
        except IndexError:
            e.append('There should be a blank row separating fuel prices from demand')
        if e:
            return e

        fuelprices = {str(c.value).strip(): (float(d.value), str(u.value)) for (c, d, u)
                      in zip(list(sheet.columns)[0][1:blankrowidx],
                             list(sheet.columns)[1][1:blankrowidx],
                             list(sheet.columns)[2][1:blankrowidx])}
        demand_by_ts = {str(c.value).strip(): (float(d.value), str(u.value)) for (c, d, u) in
                        zip(list(sheet.columns)[0][blankrowidx + 2:],
                            list(sheet.columns)[1][blankrowidx + 2:],
                            list(sheet.columns)[2][blankrowidx + 2:])
                        if not blankvalue(c.value)}

        if set(demand_by_ts.keys()) != set(self.daily_time_segments.names()):
            e.append(('Names for time periods do not match those expected'
                      ': ') + ','.join(self.daily_time_segments.names()))
        if e:
            return e
        self.fuelprices = fuelprices
        self.demand_by_ts = demand_by_ts
        self.bidround = int(bidround)
        return e

    def html_repr(self):
        txt = '<table class="market_conditions"> <tr>'
        txt += ' '.join(['<th> %s </tf>' % x
                         for x in ['Fuel', '%s Price' % self.forecast_or_actual, 'Units']])
        txt += '</tr>'
        for fuel in sorted(self.fuelprices.keys()):
            txt += '<tr> <td> %s </td> <td> %g </td> <td>%s </td> </tr>' % (fuel, self.fuelprices[fuel][0],
                                                                            self.fuelprices[fuel][1])
        txt += '<tr></tr>'
        txt += ' '.join(['<th> %s </tf>' % x
                         for x in ['Time Segment', '%s Demand' % self.forecast_or_actual, 'Units']])

        for ts in self.daily_time_segments.names():
            txt += '<tr> <td> %s </td> <td> %g </td> <td> %s </td> </tr>' % (ts,
                                                                             self.demand_by_ts[ts][0],
                                                                             self.demand_by_ts[ts][1])
        txt += '</table>'
        return txt

    # @classmethod
    # def get_from_db(cls, sa_session, bidround):
    #     if bidround == 'all':
    #         bts = BidTimeSchedule.get_from_db(sa_session)
    #         if bts:
    #             return filter_out_none([cls.get_from_db(sa_session, r) for (r, o, c) in bts])
    #         else:
    #             return []
    #     else:
    #         amclist = [amc for amc in sa_session.query(cls).filter_by(bidround=bidround)]
    #         if len(amclist) > 1:
    #             raise MageDatabaseException(
    #                 'Multiple %s market conditions for bidround %d in db' % (self.forecast_or_actual,
    #                                                                          bidround))
    #         if len(amclist) == 1:
    #             return amclist[0]

    def add_to_db(self, sa_session):
        raise NotImplementedError('abstract method must be overridden')


# noinspection PyAbstractClass,PyAbstractClass,PyAbstractClass,PyAbstractClass
class ActualMarketConditions(Base, AbstractMarketConditions):
    __tablename__ = 'actualmarketconditions'
    marketsimulationname = Column(String, ForeignKey('marketsimulationinstances.name'))
    forecast_or_actual = 'actual'

    def __init__(self, marketsimulationname):
        self.marketsimulationname = marketsimulationname

    def add_to_db(self, sa_session):
        if ActualMarketConditions.get_from_db(sa_session, marketsimulationname=self.marketsimulationname,
                                              bidround=self.bidround):
            txt = ('Actual market conditions for bidround %d already present'
                   ', not adding') % self.bidround
        else:
            sa_session.add(self)
            txt = 'Actual market conditions for bidround %d added' % self.bidround
        return txt


# noinspection PyAbstractClass,PyAbstractClass,PyAbstractClass
class ForecastMarketConditions(Base, AbstractMarketConditions):
    __tablename__ = 'forecastmarketconditions'
    marketsimulationname = Column(String, ForeignKey('marketsimulationinstances.name'))
    forecast_or_actual = 'forecast'

    def __init__(self, marketsimulationname):
        self.marketsimulationname = marketsimulationname

    def add_to_db(self, sa_session):
        if ForecastMarketConditions.get_from_db(sa_session, marketsimulationname=self.marketsimulationname,
                                                bidround=self.bidround):
            txt = ('Forecast market conditions for bidround %d already present'
                   ', not adding') % self.bidround
        else:
            sa_session.add(self)
            txt = 'Forecast market conditions for bidround %d added' % self.bidround
        return txt


def load_mcp_from_spreadsheet_file(sfile, ml, marketsimulationname):
    """ Load, validate, and extract all objects from a market configuration package

    market configuration package is a single spreadsheet with sheets laid out as

    sheet # : component
1 : username/password list
2 : master generator fleet
3 : bid time schedule
4 : daily time segments
5 : forecast market conditions for bidround 1
6 : actual market conditions for bidround 1
7 : forecast market conditions for bidround 2
8 : actual market conditions for bidround 2
... etc

    This function will return username/pw lists, mgf, bts, dtseg and then lists of fmc and amc objects.
    The fmc and amc lists will be the same length as the number of bidrounds in bts. If the amc and fmc are not
    present or cannot be loaded from the mpc spreadsheet then a None object will be returned in that list position


    :param sfile: spreadsheet file
    :param ml: message list for error messages
    :param marketsimulationname: market simulation name
    :return: dictionary containing loaded usernames,passwords,mgf, bts, dtseg, fmc_list, amc_list
    """

    wb = load_workbook(sfile)
    sheets = [s for s in wb]
    if len(sheets) < 4:
        ml.add_error('Not enough sheets in workbook to be a valid market configuration package')
        return None

    # parse username/pw list
    usernames, passwords, e1 = parse_username_password_spreadsheet_sheet(sheets[0])
    mgf = MasterGeneratorFleet(marketsimulationname=marketsimulationname)
    e2 = mgf.initialize_from_spreadsheet(sheets[1])

    bts = BidTimeSchedule(marketsimulationname=marketsimulationname)
    e3 = bts.initialize_from_spreadsheet(sheets[2])

    dtseg = DailyTimeSegments(marketsimulationname=marketsimulationname)
    e4 = dtseg.initialize_from_spreadsheet(sheets[3])

    e_tot = e1+e2+e3+e4
    if e_tot:
        ml.add_error(e_tot)
        return None

    nrounds = len(bts.bidtimes)
    amc_list = []
    fmc_list = []
    for n in range(nrounds):
        fmc = ForecastMarketConditions(marketsimulationname=marketsimulationname)
        amc = ActualMarketConditions(marketsimulationname=marketsimulationname)
        try:
            e = fmc.initialize_from_spreadsheet(sheets[4 + 2 * n], bidround=n + 1, daily_time_segments=dtseg)
            if e:
                fmc = None
        except IndexError:
            fmc = None
        try:
            e = amc.initialize_from_spreadsheet(sheets[4 + 2 * n + 1], bidround=n + 1, daily_time_segments=dtseg)
            if e:
                amc = None
        except IndexError:
            amc = None

        amc_list.append(amc)
        fmc_list.append(fmc)

    d = {'usernames': usernames,
         'passwords': passwords,
         'mgf': mgf,
         'bts': bts,
         'dtseg': dtseg,
         'amc_list': amc_list,
         'fmc_list': fmc_list}
    return d


def generate_forced_outages(mgf):
    """return dictionary forced_out_d, with bool all values, so that
    forced_out_d[(u,s)] is true if unit u, segment s was forced out.
    This code ensures propagation of outages, so that if segment n is
    out, then segments n+k for k>=0 will also be forced out

    Inputs :
    mgf - master generator fleet object

    Output :
    forced_out_d - as above
    """
    forced_out_d = {}
    for gu in mgf:
        for gs in gu:
            u = gu.unit
            s = gs.segmentnumber
            forced_out_d[(u, s)] = False

    for gu in mgf:
        for gs in gu:
            u = gu.unit
            s = gs.segmentnumber
            if gu.forced_outage_rate > random.random():
                for t in range(s, len(gu.segments) + 1):
                    # propagate outages. if segment s is out, then
                    # ensure segments s+k for s>0 are also out.
                    forced_out_d[(u, t)] = True
    return forced_out_d


def heuristic_dispatch_capacity(market_unit_segment_list, idx_margin, demand):
    dispatched_capacity = [us.effective_capacity() if (n < idx_margin) else 0
                           for (n, us) in enumerate(market_unit_segment_list)]
    capacity_diff = demand - sum(dispatched_capacity)
    if partial_activation_allowed(market_unit_segment_list[idx_margin].u, market_unit_segment_list[idx_margin].s):
        dispatched_capacity[idx_margin] = capacity_diff
    else:
        # bring marginal unit fully online, then reduce generation for
        # cheaper generators until supply matches demand.
        dispatched_capacity[idx_margin] = market_unit_segment_list[idx_margin].effective_capacity()
        capacity_diff = demand - sum(dispatched_capacity)
        # we now have excess called generation ... (so capacity_diff is negative)
        # work backwards, reducing called generation as possible
        idx_curr = idx_margin
        while capacity_diff < 0:
            idx_curr -= 1
            if idx_curr < 0:
                msg = ('Unable to meet exact demand due to constraint that'
                       'all first segments must be fully active')
                raise MageDemandNotMetExactlyException(msg)
            if partial_activation_allowed(market_unit_segment_list[idx_curr].u, market_unit_segment_list[idx_curr].s):
                curr_reduce = min(-capacity_diff, dispatched_capacity[idx_curr])
                dispatched_capacity[idx_curr] -= curr_reduce
                capacity_diff = demand - sum(dispatched_capacity)

    clearing_price = market_unit_segment_list[idx_margin].effective_bidprice()
    for dc, us in zip(dispatched_capacity, market_unit_segment_list):
        us.dispatched_capacity = dc
    return clearing_price


def partial_activation_allowed(unitname, segmentnumber):
    return unitname.startswith('magic') or segmentnumber >= 2


def sanity_check_forced_out_forced_on(forced_out_by_ts, forced_on_by_ts):
    """Raise exception if forced_out_by_ts and forced_on_d has same unit both forced out and forced on"""
    if not forced_out_by_ts.keys() == forced_on_by_ts.keys():
        raise Exception('forced_out_by_ts and forced_on_by_ts dont have same ts keys')

    for ts in forced_out_by_ts:
        for us in forced_out_by_ts[ts]:
            if forced_out_by_ts[ts][us] and forced_on_by_ts[ts][us]:
                raise MageException('Inconsistent forced_out_by_ts and forced_on_by_ts')


def sanity_check_market_unit_segment_list_by_ts(market_unit_segment_list_by_ts):
    for ts in market_unit_segment_list_by_ts:
        if any([us.forced_on and us.forced_out for us in market_unit_segment_list_by_ts[ts]]):
            raise MageException('there are market_unit_segments that are both forced on and forced out')


def run_market_model(mgf, bids, demand_by_ts, daily_time_segments):
    """
    Clear the electricity market for a single bid round

    This involves generating forced outages, ensuring that the forced outages are consistent with constraints
    (all higher segments forced out if lower segment is forced out, slow_ramp and must_run units forced out for
     all times if their segment 1 forced out any time during the day).

    Market is then cleared provisionally, then any units that are slow_ramp and are called at any time in day are
    forced on for entire day.

    :param mgf: master generator fleet
    :param bids: list of bids
    :param demand_by_ts: dictionary of demand for each time segment
    :param daily_time_segments: daily time segments
    :return: (clearing_price_by_ts, market_unit_segment_list_by_ts,logger)

            clearing_price_by_ts is dictionary of clearing prices for each time segment

            market_unit_segment_list_by_ts is dictionary of lists of market unit segments,
               for each time segment, in dispatch stack order

            logger is GenericMesageLogger, with message lists indexed by unit,segment tuples
            These messages are notes about market clearing mechanism that are relevant to each unit,segment
    """
    logger = GenericMessageLogger()

    # check bids against mgf
    if any([bid.validate_against_master_generator_fleet(mgf)[0] for bid in bids]):
        raise MageException('not all bids are valid against mgf')

    # compute forced outages
    forced_out_by_ts = {ts: generate_forced_outages(mgf) for ts in daily_time_segments.names()}

    # propagate forced outages according to constraints
    mgf_us = [(gu.unit, gs.segmentnumber) for gu in mgf for gs in gu]
    constraints_d = {gu.unit: gu.constraints for gu in mgf}

    slow_ramp_must_run_units = [gu for gu in mgf if
                                constraints_d[gu.unit].intersection(GeneratorConstraints(['slow_ramp', 'must_run']))]
    for gu in slow_ramp_must_run_units:
        if any([forced_out_by_ts[ts][(gu.unit, 1)] for ts in daily_time_segments.names()]):
            for ts in daily_time_segments.names():
                for gs in gu:
                    if not forced_out_by_ts[ts][(gu.unit, gs.segmentnumber)]:
                        forced_out_by_ts[ts][(gu.unit, gs.segmentnumber)] = True
                        msg = ('Unit %s segment %s forced out for time segment %s '
                               'due to slow_ramp or must_run constraint') % (gu.unit, gs.segmentnumber, ts)
                        logger.log((gu.unit, gs.segmentnumber), msg)
    ts1 = daily_time_segments.names()[0]
    # forced_on_d are units that, independent of bid price, must run
    # For provisional market clearing, only segment 1 of must_run units, that are not forced out, are forced on.
    forced_on_d = {}
    for (u, s) in mgf_us:
        forced_on_d[(u, s)] = (bool(constraints_d[u].intersection(GeneratorConstraints(['must_run']))) and
                               s == 1 and not forced_out_by_ts[ts1][(u, s)])

    forced_on_by_ts = {ts: forced_on_d for ts in daily_time_segments.names()}
    sanity_check_forced_out_forced_on(forced_out_by_ts, forced_on_by_ts)
    capacity_frombids_by_ts = {ts: {(bc.unit, bc.segment): bc.capacities[ts]
                                    for bid in bids for bc in bid.bidcomponents}
                               for ts in daily_time_segments.names()}
    bidprice_frombids_by_ts = {ts: {(bc.unit, bc.segment): bc.bidprices[ts]
                                    for bid in bids for bc in bid.bidcomponents}
                               for ts in daily_time_segments.names()}

    # any units that are forced on should take their capacity values from mgf, not from bid.
    capacity_from_mgf = {(gu.unit, gs.segmentnumber): gs.capacity for gu in mgf for gs in gu}
    for ts in daily_time_segments.names():
        for us in mgf_us:
            if forced_on_by_ts[ts][us]:
                capacity_frombids_by_ts[ts][us] = capacity_from_mgf[us]

    market_unit_segment_list_by_ts = {}
    for ts in daily_time_segments.names():
        market_unit_segment_list_by_ts[ts] = [MarketUnitSegment(
            u=u,
            s=s,
            bidprice=bidprice_frombids_by_ts[ts].get((u, s), UNBID_UNITS_BIDPRICE),
            bidcapacity=capacity_frombids_by_ts[ts].get((u, s), 0),
            forced_on=forced_on_by_ts[ts][(u, s)],
            forced_out=forced_out_by_ts[ts][(u, s)])
            for (u, s) in mgf_us]
        # add "magic expensive energy" units, to be activated only if total supply is insufficient to meet demand
        market_unit_segment_list_by_ts[ts].append(MarketUnitSegment.magic_expensive_energy())

    sanity_check_market_unit_segment_list_by_ts(market_unit_segment_list_by_ts)
    # clear market provisionally
    #print('\nmarket unit segment list just created')
    for ts in daily_time_segments.names():
        #print(ts)
        for x in market_unit_segment_list_by_ts[ts]:
            pass #print(x)
    # noinspection PyUnusedLocal
    clearing_price_by_ts = clear_market(market_unit_segment_list_by_ts, demand_by_ts)

    #print('\nNow market has cleared 1st time')
    for ts in daily_time_segments.names():
        #print(ts)
        for x in market_unit_segment_list_by_ts[ts]:
            pass #print(x)

    # idx_d_by_ts[ts][(u,s)] will give index of unit,segment (u,s) in market_unit_segment_list_by_ts[ts]
    idx_d_by_ts = {ts: {(us.u, us.s): n for (n, us) in enumerate(market_unit_segment_list_by_ts[ts])}
                   for ts in daily_time_segments.names()}

    # force on any slow_ramp units that were dispatched at any time
    slow_ramp_units = [u for u in constraints_d if 'slow_ramp' in constraints_d[u]]
    for u in slow_ramp_units:
        dispatched_list = [market_unit_segment_list_by_ts[ts][idx_d_by_ts[ts][(u, 1)]].dispatched_capacity > 0.0
                           for ts in daily_time_segments.names()]
        if any(dispatched_list):
            # force segment 1 on for all time segments
            for ts in daily_time_segments.names():
                market_unit_segment_list_by_ts[ts][idx_d_by_ts[ts][(u, 1)]].forced_on = True
                market_unit_segment_list_by_ts[ts][idx_d_by_ts[ts][(u, 1)]].bidcapacity = capacity_from_mgf[(u, 1)]
                if not all(dispatched_list):
                    msg = ('Unit %s segment 1 for time segment %s was forced on due to slow_ramp constraint, '
                           'dispatched capacity before forcing on was %f'
                           ) % (u, ts, market_unit_segment_list_by_ts[ts][idx_d_by_ts[ts][(u, 1)]].dispatched_capacity)
                    logger.log((u, 1), msg)
    # clear market again, with new set of forced on units.
    clearing_price_by_ts = clear_market(market_unit_segment_list_by_ts, demand_by_ts)
    #print('\nNow market has cleared 2nd time')
    for ts in daily_time_segments.names():
        #print(ts)
        for x in market_unit_segment_list_by_ts[ts]:
            pass #print(x)
    return clearing_price_by_ts, market_unit_segment_list_by_ts, logger


class MarketUnitSegment:
    def __init__(self, u, s, bidprice, bidcapacity, forced_on=False, forced_out=False, dispatched_capacity=0.0):
        self.u = u
        self.s = s
        self.bidprice = bidprice
        self.bidcapacity = bidcapacity
        self.forced_on = forced_on
        self.forced_out = forced_out
        self.dispatched_capacity = dispatched_capacity

    @classmethod
    def magic_expensive_energy(cls):
        return cls(u='magic_expensive_energy', s=1,
                   bidprice=MAGIC_EXPENSIVE_ENERGY_BIDPRICE, bidcapacity=MAGIC_EXPENSIVE_ENERGY_CAPACITY)

    @classmethod
    def magic_cheap_energy(cls, capacity=0.0):
        return cls(u='magic_cheap_energy', s=1, bidprice=0, bidcapacity=capacity)

    def effective_capacity(self):
        if self.forced_out:
            return 0.0
        else:
            return float(self.bidcapacity)

    def effective_bidprice(self):
        if self.forced_on:
            return 0.0
        else:
            return self.bidprice

    def __repr__(self):
        s = ''
        for attr in ['u', 's', 'bidprice', 'bidcapacity', 'forced_on', 'forced_out', 'dispatched_capacity']:
            s += '%s=%s ' % (attr, str(getattr(self, attr)))
        return s


def clear_market_single_ts(market_unit_segment_list, demand):
    """
    Compute clearing price, and unit dispatch for single time segment.

    :param market_unit_segment_list:  list of market unit segments. This function changes this list by reordering
    in economic order (forced on units first, followed by other units in order of increasing bidprice), and also
    by entering the dispatched_capacity for each market unit segment
    :param demand: total electricity demand for this time segment

    :return: clearing_price: the computed market clearing price
    """
    # remove magic cheap energy
    mce = MarketUnitSegment.magic_cheap_energy()
    mce_idx_list = [n for (n, x) in enumerate(market_unit_segment_list) if (x.u, x.s) == (mce.u, mce.s)]
    for n in mce_idx_list:
        del market_unit_segment_list[n]

    # sort marketunitsegments, forced_on units are first
    market_unit_segment_list.sort(key=lambda x: (not x.forced_on, x.bidprice, x.u, x.s))

    forced_on_capacity = sum([x.effective_capacity() for x in market_unit_segment_list if x.forced_on])
    if forced_on_capacity > demand:
        raise MageException('forced on capacity exceeds demand ... figure out later how to handle this case')

    #  Find marginal generator idx_margin. This is index of generator unit that, if fully activated would bring total
    #  supply above demand
    cumulative = list(cumsum([us.effective_capacity() for us in market_unit_segment_list]))
    is_called = [(c < demand) for c in cumulative]
    if any(is_called):
        idx_margin = max(idx(is_called)) + 1
    else:
        idx_margin = 0

    # sanity check
    if idx_margin > 0:
        if not (cumulative[idx_margin - 1] <= demand <= cumulative[idx_margin]):
            raise InternalMageError('cumulative[idx_margin] incorrect')

    # compute dispatch
    try:
        clearing_price = heuristic_dispatch_capacity(market_unit_segment_list, idx_margin, demand)
    except MageDemandNotMetExactlyException:
        # This code will be reached if simple heuristic method for calling capacity cannot exactly meet demand.
        # This occurs if, due to the requirement that all segment 1's are fully on, it isn't possible to exactly
        # meet the demand with the generators up to and including idx_margin.
        # In this case, add "magic cheap energy" to make up difference.
        for (n, us) in enumerate(market_unit_segment_list):
            if n < idx_margin:
                us.dispatched_capacity = us.bidcapacity
            else:
                us.dispatched_capacity = 0.0
        capacity_diff = demand - sum([us.dispatched_capacity for us in market_unit_segment_list])

        mce = MarketUnitSegment.magic_cheap_energy(capacity=capacity_diff)
        mce.dispatched_capacity = capacity_diff
        market_unit_segment_list.insert(0, mce)
        clearing_price = market_unit_segment_list[idx_margin].effective_bidprice()
        # cherrypy.log('%d units of magic cheap energy needed' % capacity_diff)

    return clearing_price


def clear_market(market_unit_segment_list_by_ts, demand_by_ts):
    """ Compute clearing price, and unit dispatch for all time segments.

    :param market_unit_segment_list_by_ts: dictionary of market_unit_segment_list for each time segment
    :param demand_by_ts: dictionary containing demand at each time segment
    :return: clearing_price_by_ts : dictionary containing clearing prices at each time segment
    """
    if market_unit_segment_list_by_ts.keys() != demand_by_ts.keys():
        raise MageException('keys do not match')
    clearing_price_by_ts = {}
    for ts in demand_by_ts:
        clearing_price_by_ts[ts] = clear_market_single_ts(market_unit_segment_list_by_ts[ts], demand_by_ts[ts])
    return clearing_price_by_ts


# related functions for writing spreadsheet files
WSpoint = collections.namedtuple('Worksheetpoint', ['row', 'col'])


def write_list_to_sheet(sheet, origin, textlists, font=None):
    """Write list of list of text items into sheet.
     Each sublist of input textlists will be written into a separate row of spreadsheet, beginning at specified origin.

     Return coordinates of last cell written (typically bottom right)
     font is openpyxl.styles.Font class, if specified all cells to be written will be assigned this font property
    """
    # munge to ensure textlist is list of list of strings,
    # if given string or list of strings as input
    if is_string(textlists):
        textlists = [[textlists]]
    if is_string_list(textlists):
        textlists = [textlists]
    trow = 0
    tcol = 0
    for (trow, tlist) in enumerate(textlists):
        for (tcol, tval) in enumerate(tlist):
            if isinstance(tval, int):
                tmp = tval
            else:
                try:
                    tmp = float(tval)
                except ValueError:
                    tmp = tval

            sheet.cell(row=origin.row + trow, column=origin.col + tcol).value = tmp

            if font:
                sheet.cell(row=origin.row + trow, column=origin.col + tcol).font = font

    return WSpoint(origin.row + trow, origin.col + tcol)


def create_empty_bidsheet(mgf, username, daily_time_segments):
    wb = Workbook()
    sheet = wb.active
    for (n, s) in enumerate(Bid.common_col_headings):
        sheet.cell(row=2, column=1+n).value = s
    for (n, s) in enumerate(daily_time_segments.names()):
        sheet.cell(row=1, column=len(Bid.common_col_headings)+1+2*n).value = s
        sheet.cell(row=2, column=len(Bid.common_col_headings)+1+2*n).value = 'Bid Price'
        sheet.cell(row=2, column=len(Bid.common_col_headings)+1+2*n+1).value = 'Capacity'

    # get list of units
    us = [(gu.owner, gu.unit, gs.segmentnumber) for gu in mgf for gs in gu]
    us = [x for x in us if x[0].lower() == username.lower()]
    us.sort(key=lambda x: (x[1].lower(), x[2]))
    write_list_to_sheet(sheet, WSpoint(3, 1), us)
    return wb


def create_individual_fleet_description(mgf, username):
    wb = Workbook()
    sheet = wb.active

    tableheadings = ['Owner', 'Type', 'Unit', 'Segment', 'Capacity', 'Forced outage rate',
                     'Fuel', 'Variable O and M $/MWh', 'Fixed costs', 'Constraints', 'Heat rate Btu/kWh']
    endpt = WSpoint(row=1, col=1)
    write_list_to_sheet(sheet, endpt, tableheadings, openpyxl.styles.Font(bold=True))
    us = [(gu.owner, gu.generator_type, gu.unit, gs.segmentnumber, int(gs.capacity), '%.5g' % gu.forced_outage_rate,
           gu.fuel, gu.variable_O_and_M, gu.fixed_costs, str(gu.constraints), gs.heat_rate)
          for gu in mgf for gs in gu]
    us = [x for x in us if x[0].lower() == username.lower()]
    us.sort(key=lambda x: (x[2].lower(), x[3]))
    write_list_to_sheet(sheet, WSpoint(2, 1), us)
    return wb


def test_master_generator_fleet():
    print('testing masterGeneratorFleet')
    mr = MasterGeneratorFleet(marketsimulationname='')
    sfname = 'spreadsheets/master_generator_fleet_description.xlsx'
    e = mr.initialize_from_spreadsheet_file(sfname)

    if e:
        for m in e:
            print(m)

    return mr


# def test_bid():
#     sfname = 'spreadsheets/bids/clark_reconstructed_bidsheet.xlsx'
#     bid = Bid()
#     e = bid.initialize_from_spreadsheet_file(sfname, bidround=2)
#     if e:
#         for m in e:
#             print(m)
#     print(bid)


# def test_bid_db():
#     sfname = 'spreadsheets/bids/clark_reconstructed_bidsheet.xlsx'
#     db_file = '/Users/david.hammond/emmpy/proto_emm_model.sqlite'
#     sa_engine = create_engine('sqlite:///' + db_file)
#     Base.metadata.create_all(sa_engine)
#     sa_session = sessionmaker(bind=sa_engine)()
#     bid = Bid()
#     e = bid.initialize_from_spreadsheet_file(sfname, bidround=2)
#     if e:
#         printlist(e)
#         raise Exception
#
#     bid.add_to_db(sa_session)
#     sa_session.commit()
#     bids = [b for b in sa_session.query(Bid)]
#     print(bids[0])


# def test_load_many_bids():
#     import glob
#     # bid_dir = 'spreadsheets/bids'
#     flist = glob.glob('spreadsheets/correctedbids/*.xlsx')
#     bids = []
#     for f in flist:
#         curr_bid = Bid()
#         e = curr_bid.initialize_from_spreadsheet_file(f, bidround=1)
#
#         if e:
#             print('Errors for bid from file ' + f)
#             for m in e:
#                 print(m)
#         else:
#             print('Bid loaded without errors from from file ' + f)
#         bids.append(curr_bid)
#     return bids

# def test_test_load_many_bids():
#     bids = test_load_many_bids()
#     bids = [b for b in bids if b.username is not None]
#     mr = test_master_generator_fleet()
#
#     for bid in bids:
#         e = bid.validate_against_master_generator_fleet(mr)
#
#         if e:
#             print('Errors for bid from ' + bid.username)
#             for m in e:
#                 print(m)
#         else:
#             print('No errors for bid from ' + bid.username)
#

# def integration_test(demandlist=[2500]):
#     # load master fleet from spreadsheet, store
#     # load bids from spreadsheets, store
#     mgf_spreadsheet_file = 'spreadsheets/master_generator_fleet_description.xlsx'
#     mr = MasterGeneratorFleet()
#     e = mr.initialize_from_spreadsheet_file(mgf_spreadsheet_file)
#     if e:
#         printlist(e)
#         return
#     else:
#         print ('Master generator fleet loaded without errors from file '
#                + mgf_spreadsheet_file)
#     sa_session.add(mr)
#
#     import glob
#     bid_dir = 'spreadsheets/bids'
#     flist = glob.glob('spreadsheets/correctedbids/*.xlsx')
#     bids = []
#     for f in flist:
#         curr_bid = Bid()
#         e = curr_bid.initialize_from_spreadsheet_file(f, bidround=1)
#         if e:
#             print 'Errors for bid from file ' + f
#             printlist(e)
#         else:
#             print 'Bid loaded without errors from from file ' + f
#         bids.append(curr_bid)
#     for bid in bids:
#         bid.add_to_db(sa_session)
#
#     sa_session.commit()
#
#     del mr
#     del bids
#
#     # load mgf from db
#     mgf = sa_session.query(MasterGeneratorFleet)[0]
#     # load bids from db
#     bids = [b for b in sa_session.query(Bid).filter_by(bidround=1)]
#
#     elist = []
#     for bid in bids:
#         e = bid.validate_against_master_generator_fleet(mgf)
#         if e:
#             printlist(e)
#             return
#         elist.append(e)
#
#     if any(elist):
#         return

def integration_test2():
    import shutil

    print("Running integration test 2")
    example_spreadsheet_dir = 'example_spreadsheets/set3/'

    dtseg = DailyTimeSegments(marketsimulationname='')
    e = dtseg.initialize_from_spreadsheet_file(os.sep.join([example_spreadsheet_dir, 'daily_time_segments.xlsx']))
    if e:
        for er in e:
            print(er)
    else:
        print('Daily time segments loaded without error')
        print(dtseg.__repr__())

    amc_spreadsheet_file = os.sep.join([example_spreadsheet_dir, 'actual_market_conditions.xlsx'])
    amc = ActualMarketConditions(marketsimulationname='')
    e = amc.initialize_from_spreadsheet_file(amc_spreadsheet_file, bidround=1, daily_time_segments=dtseg)
    if e:
        printlist(e)
        raise Exception

    mgf_spreadsheet_file = os.sep.join([example_spreadsheet_dir, 'master_generator_fleet_description.xlsx'])

    bid_dir = os.sep.join([example_spreadsheet_dir, 'bids'])
    db_file = 'emm_model_integration_test2.sqlite'

    # load master generator fleet
    mgf = MasterGeneratorFleet(marketsimulationname='')
    e = mgf.initialize_from_spreadsheet_file(mgf_spreadsheet_file)
    if e:
        printlist(e)
        raise Exception
    else:
        print(('Master generator fleet loaded without errors from file ' + mgf_spreadsheet_file))

    # load bids
    flist = glob.glob('%s/*.xlsx' % bid_dir)
    bids = []
    for f in flist:
        curr_bid = Bid(marketsimulationname='')
        e = curr_bid.initialize_from_spreadsheet_file(f, bidround=1, daily_time_segments=dtseg)
        e1, m1 = curr_bid.validate_against_master_generator_fleet(mgf)
        e = e + e1
        if e:
            print('Errors for bid from file ' + f)
            for m in e:
                print(m)
        else:
            print('Bid loaded without errors from from file ' + f)
        bids.append(curr_bid)

    # load bid time schedule
    bts_spreadsheet_file = os.sep.join([example_spreadsheet_dir, 'bid_time_schedule.xlsx'])
    bts = BidTimeSchedule(marketsimulationname='')
    e = bts.initialize_from_spreadsheet_file(bts_spreadsheet_file)
    if e:
        printlist(e)
        raise Exception
    else:
        print(('Bid time schedule loaded without errors from file ' + bts_spreadsheet_file))

    # load daily time segments
    dtseg_spreadsheet_file = os.sep.join([example_spreadsheet_dir, 'daily_time_segments.xlsx'])
    dtseg = DailyTimeSegments(marketsimulationname='')
    e = dtseg.initialize_from_spreadsheet_file(dtseg_spreadsheet_file)
    if e:
        printlist(e)
        raise Exception
    else:
        print(('Daily time segments loaded without errors from file ' + dtseg_spreadsheet_file))

    if os.path.exists(db_file):
        print("Deleting "+db_file)
        os.remove(db_file)

    print('saving mgf,bids,amc,dtseg to db')
    sa_engine = create_engine('sqlite:///' + db_file)
    Base.metadata.create_all(sa_engine)
    sa_session = sessionmaker(bind=sa_engine)()
    for bid in bids:
        bid.add_to_db(sa_session)
    sa_session.add(mgf)
    sa_session.add(amc)
    sa_session.add(bts)
    sa_session.add(dtseg)
    sa_session.commit()

    del mgf
    del bids
    del amc
    del bts
    del dtseg

    print('loading mgf,bids from db')
    mgf = sa_session.query(MasterGeneratorFleet)[0]
    bids = [b for b in sa_session.query(Bid).filter_by(bidround=1)]
    amc = sa_session.query(ActualMarketConditions)[0]
    # noinspection PyUnusedLocal
    bts = sa_session.query(BidTimeSchedule)[0]

    print('loading dtseg from db')
    dtseg = sa_session.query(DailyTimeSegments)[0]
    print(dtseg.__repr__())

    print('running model')
    # clearing_price_by_ts = {}
    # dispatched_capacity_d_by_ts = {}
    # forced_out_d_by_ts = {}
    # market_unit_segment_list_by_ts = {}

    demand_by_ts = {ts: amc.demand_by_ts[ts][0] for ts in amc.demand_by_ts}
    (clearing_price_by_ts, market_unit_segment_list_by_ts, logger
     ) = run_market_model(mgf, bids, demand_by_ts, daily_time_segments=dtseg)

    omr = OverallMarketReport(marketsimulationname='', bidround=1, clearing_price_by_ts=clearing_price_by_ts,
                              market_unit_segment_list_by_ts=market_unit_segment_list_by_ts,
                              actual_market_conditions=amc,
                              daily_time_segments=dtseg)
    demand_by_ts = {ts: amc.demand_by_ts[ts][0] for ts in amc.demand_by_ts}
    run_market_model(mgf, bids, demand_by_ts, dtseg)
    ownerlist = [bid.username for bid in bids]

    outdir = 'outputspreadsheets_integrationtest2'

    if os.path.exists(outdir):
        print('\nDirectory ' + outdir + ' already exists, deleting.')
        shutil.rmtree(outdir)
        os.mkdir(outdir)
    else:
        os.mkdir(outdir)

    for owner in ownerlist:
        # wb = create_market_report(mgf, owner, amc, omr)
        wb = omr.spreadsheet_repr(mgf, owner)
        outfname = os.path.sep.join([outdir, '%s.xlsx' % owner])
        print('Writing output spreadsheet %s' % outfname)
        wb.save(outfname)
    # create overall market report
    wb = omr.spreadsheet_repr(mgf, owner=None)
    outfname = os.path.sep.join([outdir, 'overall_market_report.xlsx'])
    print('Writing output spreadsheet %s' % outfname)
    wb.save(outfname)


# can I open a spreadsheet from a "file-like" object
if __name__ == '__main__':
    integration_test2()
    # (u,p,e)=parse_username_password_spreadsheet('spreadsheets/initial_username_password.xlsx')
    # mgf=test_master_generator_fleet()
    #    integration_test(range(500,60000,200))

    # dtseg = DailyTimeSegments()
    # e = dtseg.initialize_from_spreadsheet_file('example_spreadsheets3/daily_time_segments.xlsx')
    # if e:
    #     for er in e:
    #         print(er)
    # else:
    #     print('Daily time segments loaded without error')
    #     print(dtseg.__repr__())
    #
    # bid=Bid()
    # e = bid.initialize_from_spreadsheet_file('example_spreadsheets3/bids/davis_bidsheet.xlsx',
    #                                          bidround=1,daily_time_segments=dtseg)
    # for er in e:
    #      print(er)
    # # test putting bid in db, deleting, pulling out
    # db_file = 'emm_model_integration_test2.sqlite'
    # if os.path.exists(db_file):
    #     print("Deleting "+db_file)
    #     os.remove(db_file)
    #
    # sa_engine = create_engine('sqlite:///' + db_file)
    # Base.metadata.create_all(sa_engine)
    # sa_session = sessionmaker(bind=sa_engine)()
    # # dtseg.add_to_db(sa_session)
    # bid.add_to_db(sa_session)
    # sa_session.add(dtseg)
    # sa_session.commit()
    #
    # del bid
    # del dtseg
    #
    # dtseg=sa_session.query(DailyTimeSegments)[0]
    # bid=sa_session.query(Bid)[0]
